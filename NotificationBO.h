//
//  NotificationBO.h
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface NotificationBO : BaseBO

@property (nonatomic,strong) NSString *notificationId;
@property (nonatomic,strong) NSString *notificationTime;
@property (nonatomic,strong) NSString *notificationTittle;
@property (nonatomic,strong) NSString *notificationMessage;



-(NotificationBO *)init;
-(NotificationBO *)initWithResponse:(id)response;

@end
