//
//  NSJSONSerialization+UAAdditions.h
//  Shoneys
//
//  Created by Sanjay Kumar Yadav on 27/01/14.
//  Copyright (c) 2014 Esecforte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSJSONSerialization (UAAdditions)
+ (id)objectWithString:(NSString *)jsonString;
+ (NSString *)stringWithObject:(id)jsonObject options:(NSJSONWritingOptions)opt;
+ (NSString *)stringWithObject:(id)jsonObject;
@end
