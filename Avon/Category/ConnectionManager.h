


////Connection Manger Class
#import <Foundation/Foundation.h>
typedef enum
{
    TASK_REGISTER = 100,
   TASK_VIDEOALL,
  TASK_FEEDBACK,
  TASK_OFFERDETAILS,
  TASK_CATEGORYONE,
  TASK_PUSH_REGISTRATION,
  TASK_COUNTRY_LIST,
   TASK_GET_PRODUCT_CATEGORY,
  TASK_GET_PRODUCT,
  TASK_GET_PERCENTAGE,
  TASK_CURRENT_OFFER,
  TASK_GET_STATE,
  TASK_GET_BOUTIQUES,
  TASK_GET_ALLPRODUCT,
}CURRENT_TASK;
@protocol ConnectionManager_Delegate <NSObject>
@end

@interface ConnectionManager : NSObject
{
}

@property (nonatomic, retain) NSMutableDictionary *responseDictionary;
@property (nonatomic, retain) NSError *responseError;
@property (nonatomic, assign) CURRENT_TASK currentTask;
@property (nonatomic, assign) id<ConnectionManager_Delegate> delegate;
+ (id)sharedManager;
-(void)sendRequestToserver:(NSMutableDictionary *)dictRoot withCurrentTask:(CURRENT_TASK )task withURL:(NSString *)url with:(NSString *)method;
@end
