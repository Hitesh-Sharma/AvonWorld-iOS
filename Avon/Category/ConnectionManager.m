

#import "ConnectionManager.h"
#import "ASIFormDataRequest.h"
@interface ConnectionManager()
@end

static ConnectionManager *conectionManger = nil;

@implementation ConnectionManager
@synthesize delegate;
@synthesize responseDictionary,responseError;
@synthesize currentTask;

+ (id)sharedManager {
    static ConnectionManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)sendRequestToserver:(NSMutableDictionary *)dictRoot withCurrentTask:(CURRENT_TASK )task withURL:(NSString *)url with:(NSString *)method;
{
    conectionManger.currentTask = task;

   ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
   [request setDelegate:self];
   if ([[dictRoot allKeys] count]!=0)
   {
       for (NSString *keyValue in [dictRoot allKeys])
       {
           [request setPostValue:[dictRoot valueForKey:keyValue] forKey:keyValue];
       }
   }
    request.timeOutSeconds=180;
   [request setRequestMethod:method];
   [request setTag:task];
   [request setDidFailSelector:@selector(requestFailed:)];
   [request setDidFinishSelector:@selector(requestFinished:)];
 [request setValidatesSecureCertificate:NO];
   [request startAsynchronous];
}


#pragma mark ASI Delegates
-(void)requestFinished:(ASIHTTPRequest *)request
{
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(requestFinished:)])
        {
            [self.delegate performSelector:@selector(requestFinished:) withObject:request];
        }
}
-(void) requestFailed:(ASIHTTPRequest *)request
{
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(requestFailed:)])
    {
        [self.delegate performSelector:@selector(requestFailed:) withObject:request];
    }

}

@end
