//
//  NSDate+timeAgo.h
//  ANI
//
//  Created by esecfote on 7/14/15.
//  Copyright (c) 2015 esecfote. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (timeAgo)
- (NSString *) timeAgo;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit;
- (NSString *) timeAgoWithLimit:(NSTimeInterval)limit dateFormat:(NSDateFormatterStyle)dFormatter andTimeFormat:(NSDateFormatterStyle)tFormatter;
@end
