//
//  NSDictionary+BVJSONString.h
//  MoneyFinder
//
//  Created by sanni kumar on 7/17/14.
//  Copyright (c) 2014 eSecForte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BVJSONString)
-(NSString*) bv_jsonStringWithPrettyPrint:(BOOL) prettyPrint;
@end
