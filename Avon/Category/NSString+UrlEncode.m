//
//  NSString+UrlEncode.m
//  Burnerr
//
//  Created by Esecforte on 09/12/14.
//  Copyright (c) 2014 Cartoon. All rights reserved.
//

#import "NSString+UrlEncode.h"

@implementation NSString (UrlEncode)

-(NSString *) urlEncode{
    NSString *string = self;
    CFStringRef urlString = CFURLCreateStringByAddingPercentEscapes(
                                                                    NULL,
                                                                    (CFStringRef) string,
                                                                    NULL,
                                                                    (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                    kCFStringEncodingUTF8 );
    
    return (__bridge NSString *) urlString ;
}

@end
