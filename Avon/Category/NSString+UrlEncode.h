//
//  NSString+UrlEncode.h
//  Burnerr
//
//  Created by Esecforte on 09/12/14.
//  Copyright (c) 2014 Cartoon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UrlEncode)
-(NSString *) urlEncode;
@end
