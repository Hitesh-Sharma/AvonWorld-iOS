//
//  NSJSONSerialization+UAAdditions.m
//  Shoneys
//
//  Created by Sanjay Kumar Yadav on 27/01/14.
//  Copyright (c) 2014 Esecforte. All rights reserved.
//

#import "NSJSONSerialization+UAAdditions.h"

@implementation NSJSONSerialization (UAAdditions)


+ (NSString *)stringWithObject:(id)jsonObject {
    return [NSJSONSerialization stringWithObject:jsonObject options:0];
}

+ (NSString *)stringWithObject:(id)jsonObject options:(NSJSONWritingOptions)opt {
    if (!jsonObject) {
        return nil;
        
    }
    NSData *data = [NSJSONSerialization dataWithJSONObject:jsonObject
                                                   options:opt
                                                     error:nil];
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (id)objectWithString:(NSString *)jsonString {
    if (!jsonString) {
        return nil;
    }
    return [NSJSONSerialization JSONObjectWithData: [jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                           options: NSJSONReadingMutableContainers
                                             error: nil];
}


@end
