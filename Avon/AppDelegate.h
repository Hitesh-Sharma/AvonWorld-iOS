//
//  AppDelegate.h
//  Avon
//
//  Created by Shipra Singh on 27/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,MKMapViewDelegate>
{
   CLLocationManager *locationManager;
   }
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString* eastOrWestMalaysia;

- (void)setUser;

@end

