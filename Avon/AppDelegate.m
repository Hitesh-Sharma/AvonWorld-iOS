//
//  AppDelegate.m
//  Avon
//
//  Created by Shipra Singh on 27/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "LeftSlideViewController.h"
#import "Database.h"
#import <GoogleMaps/GoogleMaps.h>
#import "NotificationViewController.h"
#import "LeftSlideViewController.h"
#import "NotificationBO.h"
#import "LeftViewChangeViewController.h"


@interface AppDelegate ()<UINavigationControllerDelegate>
{
  SlideNavigationController *slideNavController;
  int a;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  a = 0;
   
  for (NSString* family in [UIFont familyNames])
  {
    NSLog(@"%@", family);
    
    for (NSString* name in [UIFont fontNamesForFamilyName: family])
    {
      NSLog(@"  %@", name);
    }
  }
  [GMSServices provideAPIKey:@"AIzaSyAO6pmWPFnmYBGYmx8sJlMvxBv2yOBkFdg"];
 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   [self accessingMap];
  
   // [NSThread sleepForTimeInterval:3.0];
     
   
  if ( ![[NSUserDefaults standardUserDefaults]boolForKey:kFirstLogin]  ) {
    
     [[NSUserDefaults standardUserDefaults]setBool: YES forKey:kFirstLogin];
  }
  [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsEastMalaysia];
  
  [[NSUserDefaults standardUserDefaults]synchronize];
  
  self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor clearColor];
 
    
  //ViewController class used to display twitter and top news
  HomeViewController *mainVC=[[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
  
  slideNavController = [[SlideNavigationController alloc]initWithRootViewController:mainVC];
  slideNavController.navigationBarHidden=YES;
  LeftSlideViewController *leftVC = [[LeftSlideViewController alloc]initWithNibName:@"LeftSlideViewController" bundle:nil];
  slideNavController.portraitSlideOffset = [[UIScreen mainScreen]bounds].size.width - ([[UIScreen mainScreen]bounds].size.width-[[UIScreen mainScreen]bounds].size.width/4);
  slideNavController.leftMenu = leftVC;
  [slideNavController setNavigationBarHidden:YES];
  self.window.rootViewController = slideNavController;
  [self.window makeKeyAndVisible];
  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
  [self.window setBackgroundColor:[UIColor whiteColor]];
  
  [self appRegisterForRemoteNotification];
 
  // Override point for customization after application launch.
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

-(void)accessingMap{
  
  
  locationManager = [[CLLocationManager alloc]init];
  //[locationManager requestAlwaysAuthorization];
  //[locationManager requestWhenInUseAuthorization];
  
  //[locationManager startUpdatingLocation];
//  [locationManager startUpdatingLocation];
  
  if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    [locationManager requestWhenInUseAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;

  } else {
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;

    [locationManager startUpdatingLocation];
    
  }
    if ([CLLocationManager locationServicesEnabled]) {
         switch ([CLLocationManager authorizationStatus]) {
           case kCLAuthorizationStatusAuthorized:
    
             break;
           case kCLAuthorizationStatusDenied:
           {
                   }
             break;
           case kCLAuthorizationStatusNotDetermined:
           {
    
                  }
             break;
           case kCLAuthorizationStatusRestricted:
           {
                   }
             break;
             
           default:
             break;
         }
       }
  
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
////  if (self.isLocationSelected == NO) {
////    self.isLocationSelected = YES;
//  NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
//  NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
//  
//  CLLocationCoordinate2D zoomLocation;
//  zoomLocation.latitude = 5.25;
//  zoomLocation.longitude= 117;
//    [locationManager stopUpdatingLocation];
//  CLLocation *previousLocation = [[CLLocation alloc] initWithLatitude:zoomLocation.latitude longitude:zoomLocation.longitude];
//
//    
////      CLGeocoder *ceo = [[CLGeocoder alloc]init];
////      [ceo reverseGeocodeLocation:previousLocation
////                completionHandler:^(NSArray *placemarks, NSError *error) {
////                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
////    //              labelLocation.text = [[placemark.subLocality stringByAppendingString:@",\n"] stringByAppendingString:placemark.locality];
////                   NSLog(@"%@",placemark.region );
////                  NSLog(@"%@",placemark.addressDictionary );
////                  NSLog(@"%@",placemark.name );
////                  NSLog(@"%@",placemark.thoroughfare );
////                   NSLog(@"%@",placemark.subThoroughfare );
////                   NSLog(@"%@",placemark.locality );
////                   NSLog(@"%@",placemark.subLocality );
////                   NSLog(@"%@",placemark.administrativeArea );
////                  NSLog(@"%@",placemark.subAdministrativeArea );
////                  NSLog(@"%@",placemark.postalCode );
////                  NSLog(@"%@",placemark.ISOcountryCode );
////                  NSLog(@"%@",placemark.country );
////                  NSLog(@"%@",placemark.inlandWater );
////                  NSLog(@"%@",placemark.ocean );
////                  NSLog(@"%@",placemark.areasOfInterest );
////                NSString*  placeName = [[placemark.subLocality stringByAppendingString:@", "] stringByAppendingString:placemark.locality];
////            }
////          ];
//  
//    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%@", zoomLocation.latitude, zoomLocation.longitude, @"AIzaSyAO6pmWPFnmYBGYmx8sJlMvxBv2yOBkFdg"];
//  url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//  
//  NSURL *googleRequestURL=[NSURL URLWithString:url];
//  
//  dispatch_async(kBgQueue, ^{
//    NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
//    [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
//    
//  });
//
//
//  //}
//}





//-(void)fetchedData:(NSData *)responseData {
//  //parse out the json data
//  NSError* error;
//  NSDictionary* json = [NSJSONSerialization
//                        JSONObjectWithData:responseData
//
//                        options:kNilOptions
//                        error:&error];
//
//  NSArray* data = [json objectForKey:@"results"];
//
//  for (NSDictionary* reportData in data) {
//    
//  }
//}

/*This method is used for enabling the APNS*/
-(void)appRegisterForRemoteNotification{
  
  if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
  {
    // iOS 8 Notifications
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
  }
  else
  {
    // iOS < 8 Notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
  }

  
//  if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]){
//    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
//    [[UIApplication sharedApplication]registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert)];
//  }
//  
//  else{
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//     (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
//  }
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
  NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
  token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
  
  [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"DevToken"];
  [[NSUserDefaults standardUserDefaults]synchronize];
  [[NSNotificationCenter defaultCenter]
   postNotificationName:@"CallWebServiceForPush"
   object:self];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
   application.applicationIconBadgeNumber = 0;
  NSString *message = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"body"];
  NotificationBO *obj = [[NotificationBO alloc]init];
  obj.notificationTittle = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"title"];
  obj.notificationMessage = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"body"];
  obj.notificationTime = [[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]objectForKey:@"time"];
  [[Database getSharedInstance]storeMessage:obj];
  if (application.applicationState == UIApplicationStateInactive)
  {
    NotificationViewController *mainVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];

    slideNavController = [[SlideNavigationController alloc]initWithRootViewController:mainVC];
    slideNavController.navigationBarHidden=YES;
    LeftSlideViewController *leftVC = [[LeftSlideViewController alloc]initWithNibName:@"LeftSlideViewController" bundle:nil];
    slideNavController.portraitSlideOffset = [[UIScreen mainScreen]bounds].size.width - ([[UIScreen mainScreen]bounds].size.width-[[UIScreen mainScreen]bounds].size.width/4);
    slideNavController.leftMenu = leftVC;
    [slideNavController setNavigationBarHidden:YES];
    self.window.rootViewController = slideNavController;
    [self.window makeKeyAndVisible];
    
    application.applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
  }else {
    ALERT(@"", message);
  }
  
  [[NSNotificationCenter defaultCenter]
   postNotificationName:@"NotificationMessage"
   object:message];
}

- (void)setUser
{
  a = a+1;
  
    //vendor
  if (a % 2 == 0) {
    
  
HomeViewController *mainVC=[[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];

slideNavController = [[SlideNavigationController alloc]initWithRootViewController:mainVC];
slideNavController.navigationBarHidden=YES;
LeftViewChangeViewController *leftVC = [[LeftViewChangeViewController alloc]initWithNibName:@"LeftViewChangeViewController" bundle:nil];
slideNavController.portraitSlideOffset = [[UIScreen mainScreen]bounds].size.width - ([[UIScreen mainScreen]bounds].size.width-[[UIScreen mainScreen]bounds].size.width/4);
slideNavController.leftMenu = leftVC;
    [slideNavController setNavigationBarHidden:YES];
    self.window.rootViewController = slideNavController;
    [self.window makeKeyAndVisible];
  }else{
    HomeViewController *mainVC=[[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    
    slideNavController = [[SlideNavigationController alloc]initWithRootViewController:mainVC];
    slideNavController.navigationBarHidden=YES;
    LeftSlideViewController *leftVC = [[LeftSlideViewController alloc]initWithNibName:@"LeftSlideViewController" bundle:nil];
   slideNavController.portraitSlideOffset = [[UIScreen mainScreen]bounds].size.width - ([[UIScreen mainScreen]bounds].size.width-[[UIScreen mainScreen]bounds].size.width/4); 
    slideNavController.leftMenu = leftVC;
    [slideNavController setNavigationBarHidden:YES];
    self.window.rootViewController = slideNavController;
    [self.window makeKeyAndVisible];

  }
//    VendorMenuViewController *vendorMenu = [[VendorMenuViewController alloc]initWithNibName:@"VendorMenuViewController" bundle:nil];
//    
//    self.navigationController.leftMenu = vendorMenu;


  
}


@end
