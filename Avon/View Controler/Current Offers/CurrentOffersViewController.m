//
//  CurrentOffersViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "CurrentOffersViewController.h"
#import "CurrentOfferBO.h"
#import "UIImageView+WebCache.h"


@interface CurrentOffersViewController ()<ConnectionManager_Delegate,UIScrollViewDelegate>
{
  
  IBOutlet UILabel *sepraterLine;
  IBOutlet UILabel *labelOfferDetails;
  IBOutlet UILabel *labelOffer;
  IBOutlet UIView *viewForOffers;
  IBOutlet UIView *viewForImage;
  IBOutlet UIScrollView *scrollForImage;
 
  NSMutableArray *offerDetails;
  CGFloat offsetRadio;
  UIImageView* previewImage;
  UIScrollView* viewForPreviewImage;
  ConnectionManager *requestManager;
}
@end

@implementation CurrentOffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  //Hide All Views
  viewForImage.hidden = YES;
  viewForOffers.hidden = YES;
  sepraterLine.hidden = YES;

  //connection Manager
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  
  [self designHeaderOfTheScreen:@"menu_icon.png": LocalizedString(@"Current Offers") :@"":@"":@""];
  
  offerDetails = [[NSMutableArray alloc]init];
  
  [self customView];
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
    ALERT(@"", LocalizedString(@"Offer Connection"));
  }else{
  [self sendDataToServerWithTag:TASK_CURRENT_OFFER];
  }
 
}


#pragma mark - Customise View

-(void)customView{
  
  int lowerHeight;
  int upperHeight;
 
  int i = 0;
  if (IS_IPAD) {
     i = 30;
  }
  
  upperHeight = (80 * (kScreenHeight - 60))/100;
  lowerHeight = (20 * (kScreenHeight - 60))/100;
 
  //customise view for inlarge Image
  
  viewForImage.frame = CGRectMake(5 + i/3, 65 + i/3, kScreenWidth -10 - 2*i/3,  upperHeight - 10 - 2*i/3);
  scrollForImage.frame = CGRectMake(0,0,viewForImage.frame.size.width , viewForImage.frame.size.height);
  scrollForImage.pagingEnabled = YES;
  scrollForImage.delegate = self;
  [viewForImage addSubview:scrollForImage];
  
  viewForOffers.frame = CGRectMake(5 + i/3, kScreenHeight - lowerHeight + 5 + i/3, kScreenWidth -10 - 2*i/3, lowerHeight - 10 - 2*i/3);
  labelOffer.frame = CGRectMake(10, 5,viewForOffers.frame.size.width - 20 , 20 + i/2);
  labelOfferDetails.frame = CGRectMake(10, 30 + i/2, viewForOffers.frame.size.width - 20 , viewForOffers.frame.size.height - 35);
  sepraterLine.frame = CGRectMake(0, viewForOffers.frame.origin.y - 6 - i/3, kScreenWidth, 1);

  //set tap gesture
  UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImageOfScrollView:)];
  
  [scrollForImage setUserInteractionEnabled:YES];
  [scrollForImage addGestureRecognizer:singleTap];
  scrollForImage.tag = 1000;
  
  
  [self roundCornersOfView:viewForOffers radius:lowerHeight/10 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0f] borderWidth:1];
  
  //set font size
  labelOfferDetails.font = [UIFont fontWithName:kFontHelvaticaLight size:16 + i/3];
  labelOffer.font = [UIFont fontWithName:kFontHelavaticaBold size:16 + i/3];
  
  labelOffer.text = LocalizedString(@"Offers");
 
 }


#pragma mark - Created Views For Image

-(void)createdForImage{
  [self setOffer:0];

  scrollForImage.contentSize = CGSizeMake(CGRectGetWidth(scrollForImage.frame) * [offerDetails count], 1);

  for (UIView *v  in scrollForImage.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
  }
  
  for (int i = 0 ; i<[offerDetails count]; i++) {
    
  CurrentOfferBO* obj = [offerDetails objectAtIndex:i];
    
  UIView *viewForProductImage = [[UIView alloc]init];
  UIImageView *imageOffer = [[UIImageView alloc]init];
  
  viewForProductImage.frame = CGRectMake(scrollForImage.frame.size.width * i,0,scrollForImage.frame.size.width , scrollForImage.frame.size.height);
  imageOffer.frame = CGRectMake(0, 0,viewForProductImage.frame.size.width  , viewForProductImage.frame.size.height);
  imageOffer.contentMode = UIViewContentModeScaleAspectFit;
  
  [imageOffer sd_setImageWithURL:[NSURL URLWithString:obj.offerImage]
                     placeholderImage:nil
                              options:SDWebImageProgressiveDownload
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            }];

    //[imageOffer setImage:[UIImage imageNamed:@"play_icon_ipad.png"]];
  viewForProductImage.backgroundColor = [UIColor whiteColor];
  imageOffer.backgroundColor = [UIColor clearColor];
    
  [scrollForImage addSubview:viewForProductImage];
  [viewForProductImage addSubview:imageOffer];
       }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews.tag == 1) {
    
 
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    
    self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
  }
  
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  
      CGFloat pageWidth = scrollViews.contentOffset.x;
      int i = pageWidth/scrollForImage.frame.size.width;
  
      [self setOffer:i];
  
}


- (void)setOffsetRadio:(CGFloat)offsetRadios
{
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    
    [scrollForImage setContentOffset:CGPointMake(scrollForImage.frame.size.width + scrollForImage.frame.size.width * offsetRadios, 0)];
  }
}


-(void)setOffer:(int)offertAtindex{
  CurrentOfferBO* obj = [offerDetails objectAtIndex:offertAtindex];
  
  
  int i = 0;
  if (IS_IPAD) {
    i = 30;
  }

  NSAttributedString *attributed = [[NSAttributedString alloc]initWithString:obj.offerDescreption attributes:@
                                        {
                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:16 + i/3]
                                        }];
  CGRect rect = [attributed boundingRectWithSize:(CGSize){viewForOffers.frame.size.width - 20, CGFLOAT_MAX}
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                 context:nil];
  CGSize size = rect.size;
  
  CGFloat heights = size.height;
  
  
  if (heights > (viewForOffers.frame.size.height - 35)) {
    labelOfferDetails.frame = CGRectMake(10, 30 + i/2, viewForOffers.frame.size.width - 20 , viewForOffers.frame.size.height - 35);
    labelOfferDetails.adjustsFontSizeToFitWidth = YES;
  }else{
    
    labelOfferDetails.frame = CGRectMake(10, 30 + i/2, viewForOffers.frame.size.width - 20 , heights);
  }
 
   labelOffer.text = obj.offerTittle;
   labelOfferDetails.text = obj.offerDescreption;
  //[labelOfferDetails sizeToFit];
}


#pragma setting NEA image for zooming

- (void)previewNEAImage:(NSString *)image
{
  previewImage = [[UIImageView alloc]init];
  previewImage.frame = CGRectMake(10, 80 , kScreenWidth - 20, kScreenHeight - 160);
  [previewImage sd_setImageWithURL:[NSURL URLWithString:image]
           placeholderImage:nil
                    options:SDWebImageProgressiveDownload
                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                   }
                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                  }];
  previewImage.contentMode = UIViewContentModeScaleAspectFit;
  viewForPreviewImage =[[UIScrollView alloc]initWithFrame: CGRectMake(0, 0,kScreenWidth, kScreenHeight)];
  viewForPreviewImage.contentSize = previewImage.bounds.size;
  viewForPreviewImage.contentMode = UIViewContentModeCenter;
  viewForPreviewImage.indicatorStyle = UIScrollViewIndicatorStyleWhite;
  viewForPreviewImage.minimumZoomScale = 0.1f;
  viewForPreviewImage.maximumZoomScale = 3.0f;
  viewForPreviewImage.delegate = self;
  [viewForPreviewImage setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.7]];
  [self.view addSubview:viewForPreviewImage];
  [viewForPreviewImage addSubview:previewImage];
  
  [[UIApplication sharedApplication].keyWindow addSubview:viewForPreviewImage];
  
  UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
  
  [viewForPreviewImage setUserInteractionEnabled:YES];
  [viewForPreviewImage addGestureRecognizer:singleTap];
  
}

- (void)tapOnImageOfScrollView:(UITapGestureRecognizer *)gesture

{
  NSInteger section = (gesture.view.tag)/1000;
  NSInteger row = (gesture.view.tag)%1000;
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
  CurrentOfferBO* obj = [offerDetails objectAtIndex:indexPath.row];
  [self previewNEAImage:obj.offerImage];
  
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture

{
  [viewForPreviewImage removeFromSuperview];
  
}



- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
  
  return previewImage;
  
}



-(void)scrollViewDidZoom:(UIScrollView *)scrollView

{
  
  previewImage.frame = [self centeredFrameForScrollView:viewForPreviewImage andUIView:previewImage];
  
  
}



- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
  
  CGSize boundsSize = scroll.bounds.size;
  
  CGRect frameToCenter = rView.frame;
  
  // center horizontally
  
  if (frameToCenter.size.width < boundsSize.width) {
    
    frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    
  }
  
  else {
    
    frameToCenter.origin.x = 0;
    frameToCenter.origin.y = 80;
    
  }
  return frameToCenter;
}


#pragma mark - Accessing Data From Server


-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_CURRENT_OFFER:
    {
      NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
      [dateFormatter setDateFormat:@"YYYY-MM-dd"];
      NSDate *now = [NSDate date];
      NSString *dateString = [dateFormatter stringFromDate:now];
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryId = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      [dictRoot setObject:countryId forKey:kCountryId];
      [dictRoot setObject:dateString forKey:kOfferStartDate];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_CURRENT_OFFER withURL:kUrlForCurrentOffer with:@"POST"];
      break;
    }
       default:
      break;
  }
}



-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_CURRENT_OFFER:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllOffer = [[NSMutableArray alloc]init];
          arrayAllOffer = [recievedDictionary objectForKey:kResponse];
          if ([arrayAllOffer count] > 0) {
            
          for (NSDictionary *obj in arrayAllOffer) {

          CurrentOfferBO* offer = [[CurrentOfferBO alloc]initWithResponse:obj];
          [offerDetails addObject:offer];
          }
          viewForImage.hidden = NO;
          viewForOffers.hidden = NO;
          sepraterLine.hidden = NO;
          [self createdForImage];
          }else{
            ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
            viewForImage.hidden = YES;
            viewForOffers.hidden = YES;
            sepraterLine.hidden = YES;
          }


        }else{
          ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
          viewForImage.hidden = YES;
          viewForOffers.hidden = YES;
          sepraterLine.hidden = YES;

        }
        break;
      }
        
      default:
        break;
    }
  }
  else
  {
    ALERT(nil, @"");
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  NSString *response = [request responseString];
  [self remove_Loader];
}












@end
