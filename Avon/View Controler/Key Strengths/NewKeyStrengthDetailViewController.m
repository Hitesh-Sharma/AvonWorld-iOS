//
//  NewKeyStrengthDetailViewController.m
//  Avon
//
//  Created by Shipra Singh on 06/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NewKeyStrengthDetailViewController.h"

@interface NewKeyStrengthDetailViewController ()<UIScrollViewDelegate>
{
  IBOutlet UIScrollView *scrollImage;
  NSMutableArray *arraykeyStrength;
}
@end

@implementation NewKeyStrengthDetailViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  arraykeyStrength = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"key1"),LocalizedString(@"key2"),LocalizedString(@"key3"),LocalizedString(@"key4"),LocalizedString(@"key5"),LocalizedString(@"key6"),LocalizedString(@"key7"),LocalizedString(@"key8"),LocalizedString(@"key9"),LocalizedString(@"key10"), nil];
  
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Our Strengths") :@"":@"":@""];
  [self createingSlider];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)createingSlider{
  
  NSMutableArray* questions = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Question1"),LocalizedString(@"Question2"),LocalizedString(@"Question3"),LocalizedString(@"Question4"),LocalizedString(@"Question5"),LocalizedString(@"Question6"),LocalizedString(@"Question7"),LocalizedString(@"Question8"),LocalizedString(@"Question9"),LocalizedString(@"Question10"), nil];
  
  NSMutableArray* answers = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Ans1"),LocalizedString(@"Ans2"),LocalizedString(@"Ans3"),LocalizedString(@"Ans4"),LocalizedString(@"Ans5"),LocalizedString(@"Ans6"),LocalizedString(@"Ans7"),LocalizedString(@"Ans8"),LocalizedString(@"Ans9"),LocalizedString(@"Ans10"), nil];
  
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
  NSString* imageforOneX;
  NSString* imageForThreeX;
  NSString* imageForIpadX;
  
  if ([countryName isEqualToString:@"Philippines"]) {
    imageforOneX = @"key_10_Ph@2x.png";
    imageForThreeX = @"key_10_Ph@3x.png";
    imageForIpadX = @"key_10_ipad_Ph@2x.png";

  }else{
    imageforOneX = @"key_10@2x.png";
    imageForThreeX = @"key_10@3x.png";
    imageForIpadX = @"key_10_ipad@2x.png";

  }
  
  NSMutableArray* colorForR = [[NSMutableArray alloc]initWithObjects:@"15",@"211",@"128",@"204",@"69",@"0",@"240",@"231",@"43",@"235", nil];
  NSMutableArray* colorForG = [[NSMutableArray alloc]initWithObjects:@"89",@"25",@"29",@"83",@"107",@"167",@"100",@"22",@"54",@"17", nil];
  NSMutableArray* colorForB = [[NSMutableArray alloc]initWithObjects:@"113",@"32",@"128",@"156",@"77",@"161",@"34",@"126",@"136",@"100", nil];
  
  NSMutableArray* imageFor1x = [[NSMutableArray alloc]initWithObjects:@"key_1@2x.png",@"key_2@2x.png",@"key_3@2x.png",@"key_4@2x.png",@"key_5@2x.png",@"key_6@2x.png",@"key_7@2x.png",@"key_8@2x.png",@"key_9@2x.png",imageforOneX, nil];
  NSMutableArray* imageFor3x = [[NSMutableArray alloc]initWithObjects:@"key_1@3x.png",@"key_2@3x.png",@"key_3@3x.png",@"key_4@3x.png",@"key_5@3x.png",@"key_6@3x.png",@"key_7@3x.png",@"key_8@3x.png",@"key_9@3x.png",imageForThreeX, nil];
  NSMutableArray* imageForIpad = [[NSMutableArray alloc]initWithObjects:@"key_1_ipad@2x.png",@"key_2_ipad@2x.png",@"key_3_ipad@2x.png",@"key_4_ipad@2x.png",@"key_5_ipad@2x.png",@"key_6_ipad@2x.png",@"key_7_ipad@2x.png",@"key_8_ipad@2x.png",@"key_9_ipad@2x.png",imageForIpadX, nil];
  
  
  scrollImage.frame = CGRectMake(0,60,kScreenWidth , kScreenHeight - 60);
  scrollImage.contentSize = CGSizeMake(CGRectGetWidth(scrollImage.frame) * 10,0);
  
  int height;
  int imageHeight;
  int x = 0;
  
  int size = 25;
  if (kScreenHeight > 667) {
    size = 35;
  }else if(kScreenHeight == 568){
    size = 30;
  }else if(kScreenHeight == 667){
    size = 30;
  }
  
  if (IS_IPAD) {
    height = (70 * 780/100);
    imageHeight = (70 * height)/100;
    x = 30;
  }else{
    height = (70 * kScreenWidth/100);
    imageHeight = (70 * height)/100;
  }
  
  for (UIView *v  in scrollImage.subviews) {
    if ([v isKindOfClass:[UIScrollView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
  }
  
  for (int i = 0 ; i<10; i++) {
    UIScrollView *viewBackgroundForKey = [[UIScrollView alloc]init];
    UILabel* labelText = [[UILabel alloc]init];
    UIImageView* imageAvon = [[UIImageView alloc]init];
    UIImageView* imageTaiwan = [[UIImageView alloc]init];
    UIView *viewBackgroundForScroll = [[UIView alloc]init];
    UILabel *labelForTittle = [[UILabel alloc]init];
    UILabel *labelForQ = [[UILabel alloc]init];
    UILabel *labelForA = [[UILabel alloc]init];
    UIImageView *imageMiddle = [[UIImageView alloc]init];
    UIImageView *imageLower = [[UIImageView alloc]init];
    
    viewBackgroundForScroll.frame = CGRectMake(scrollImage.frame.size.width * i,0,scrollImage.frame.size.width , scrollImage.frame.size.height );
    viewBackgroundForKey.frame = CGRectMake(0, 0, kScreenWidth, viewBackgroundForScroll.frame.size.height);
    
    viewBackgroundForKey.delegate = self;
    viewBackgroundForKey.backgroundColor = [UIColor colorWithRed:([[colorForR objectAtIndex:i] floatValue]/255.0f) green:([[colorForG objectAtIndex:i] floatValue]/255.0f) blue:([[colorForB objectAtIndex:i] floatValue]/255.0f) alpha:1.0f];
    
    
    NSAttributedString *attributedForTittle = [[NSAttributedString alloc]initWithString:[arraykeyStrength objectAtIndex:i] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelavaticaBold size:15 + x/6]}];
    CGRect rectForTittle = [attributedForTittle boundingRectWithSize:(CGSize){kScreenWidth - 40 - (2*x), CGFLOAT_MAX}
                                                             options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize sizeForTittle = rectForTittle.size;
    CGFloat heightsForTittle = sizeForTittle.height;
    
    
    NSAttributedString *attributedForQ = [[NSAttributedString alloc]initWithString:[questions objectAtIndex:i] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaBoldItalic size:15 + x/6]}];
    CGRect rectForQ = [attributedForQ boundingRectWithSize:(CGSize){kScreenWidth - 40 - (2*x), CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize sizeForQ = rectForQ.size;
    
    CGFloat heightsForQ = sizeForQ.height;
    NSAttributedString *attributedForA = [[NSAttributedString alloc]initWithString:[answers objectAtIndex:i] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelavaticaBold size:15 + x/6]}];
    CGRect rectForA = [attributedForA boundingRectWithSize:(CGSize){kScreenWidth - 40 - (2*x), CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize sizeForA = rectForA.size;
    
    CGFloat heightsForA = sizeForA.height;
    
    labelForTittle.frame = CGRectMake(20+x, size + x/2 - 5, kScreenWidth - 40 - (2*x), heightsForTittle);
    
    labelForQ.frame = CGRectMake(20 + x, labelForTittle.frame.origin.y + heightsForTittle + size + x/8, kScreenWidth - 40 - (2*x), heightsForQ);
    
    imageMiddle.frame = CGRectMake(20 + x,  labelForQ.frame.origin.y + heightsForQ + size + x/8, kScreenWidth - 40 - (2*x), imageHeight);
    
    labelForA.frame = CGRectMake(20 + x, imageMiddle.frame.origin.y + size +  imageHeight + x/5 , kScreenWidth - 40 - (2*x), heightsForA);
    
    imageLower.frame = CGRectMake(20 + x, labelForA.frame.origin.y + 25 +  heightsForA, kScreenWidth - 40 - (2*x), imageHeight);
    
    labelText.frame = CGRectMake(20 + x, labelForA.frame.origin.y + 25 +  heightsForA, kScreenWidth - 40 - (2*x), imageHeight);
    imageAvon.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/2 - (imageHeight/3)/2 + 10,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
    imageTaiwan.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/6 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
    NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
    NSString* logoText;
    if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
      logoText = @"Beauty for a purpose \n\n\n\n my.avon.com";
    }else  if ([countryName isEqualToString:@"Taiwan"]) {
      logoText = @"\n\n\n\n\n avon.com.tw";
    }else if ([countryName isEqualToString:@"India"]){
      logoText = @"Beauty for a purpose \n\n\n\n NEW YORK";
      
    }else if ([countryName isEqualToString:@"Philippines"]){
      logoText = @"Beauty for a purpose \n\n\n\n avon.com.ph";
      
    }
    
    imageAvon.backgroundColor = [UIColor clearColor];
    labelText.backgroundColor = [UIColor clearColor];
    labelText.textAlignment = NSTextAlignmentCenter;
    labelText.text = logoText;
    
    imageAvon.contentMode = UIViewContentModeCenter;
    imageTaiwan.backgroundColor = [UIColor clearColor];
    imageTaiwan.contentMode = UIViewContentModeCenter;
    NSString* compareText;
    if ([countryName isEqualToString:@"Taiwan"]) {
      compareText = @"雅芳美麗好給力";
      
    }else{
      compareText = @"Beauty for a purpose";
      
    }
    
    NSString* compareText1 = @"\n AVON";
    labelText.numberOfLines = 0;
    
    NSMutableAttributedString *attributedString1;
    attributedString1 = [[NSMutableAttributedString alloc] initWithString:labelText.text attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaLight size:14 + x/2]}];
    
    NSError *error4 = nil;
    NSRegularExpression *    regex4 = [NSRegularExpression regularExpressionWithPattern:compareText options:0 error:&error4];
    
    NSArray *matches2 = [regex4 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    NSError *error3 = nil;
    NSRegularExpression *    regex3 = [NSRegularExpression regularExpressionWithPattern:@"AVON" options:0 error:&error3];
    
    NSArray *matches1 = [regex3 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    
    for (NSTextCheckingResult *match in matches2)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontCoalhandLukeTRIAL size:30 + x]
                                range:matchRange];
      
    }
    
    for (NSTextCheckingResult *match in matches1)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontHelvaticaLight size:30 + x]
                                range:matchRange];
      NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
      [paragraphStyle setLineSpacing:15];
      paragraphStyle.alignment = NSTextAlignmentCenter;
      [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [compareText length])];
      
      
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange([compareText length], [compareText1 length])];
    
    
    labelText.attributedText = attributedString1;
    labelText.textColor = [UIColor whiteColor];

    
    
    
    labelForA.font = [UIFont fontWithName:kFontHelavaticaBold size:15 + x/6];
    labelForTittle.font = [UIFont fontWithName:kFontHelavaticaBold size:15 + x/6];
    labelForQ.numberOfLines = 0;
    labelForA.numberOfLines = 0;
    labelForTittle.numberOfLines = 0;
    labelForQ.textAlignment = NSTextAlignmentLeft;
    labelForA.textAlignment = NSTextAlignmentLeft;
    labelForTittle.textAlignment = NSTextAlignmentLeft;
    
    [scrollImage addSubview:viewBackgroundForScroll];
    [viewBackgroundForScroll addSubview:viewBackgroundForKey];
    [viewBackgroundForKey addSubview:labelForTittle];
    [viewBackgroundForKey addSubview:labelForQ];
    [viewBackgroundForKey addSubview:imageMiddle];
    [viewBackgroundForKey addSubview:labelForA];
    [viewBackgroundForKey addSubview:labelText];
     [viewBackgroundForKey addSubview:imageAvon];
    if ([countryName isEqualToString:@"Taiwan"]) {
      [viewBackgroundForKey addSubview:imageTaiwan];
      
    }
    
    labelForA.text =  [answers objectAtIndex:i];
    labelForQ.text =  [questions objectAtIndex:i];
    labelForTittle.text = [arraykeyStrength objectAtIndex:i];
    if (IS_IPAD){
      imageMiddle.image = [UIImage imageNamed:[imageForIpad objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo_ipad.png"];
       imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purposeipad@1x.png"];
//      imageLower.image = [UIImage imageNamed:@"logo_ipad@2x.png"];
    }else if (kScreenHeight > 600) {
      imageMiddle.image = [UIImage imageNamed:[imageFor3x objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo@3x.png"];
      imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@3x.png"];
//      imageLower.image = [UIImage imageNamed:@"logo@3x.png"];
    }else{
      imageMiddle.image = [UIImage imageNamed:[imageFor1x objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo@2x.png"];
      imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@2x.png"];
//      imageLower.image = [UIImage imageNamed:@"logo@2x.png"];
    }
    labelForA.textColor = [UIColor whiteColor];
    labelForQ.textColor = [UIColor whiteColor];
    labelForTittle.textColor = [UIColor whiteColor];
    imageMiddle.contentMode = UIViewContentModeScaleAspectFill;
    imageLower.contentMode = UIViewContentModeCenter;
    viewBackgroundForKey.contentSize = CGSizeMake(0, labelText.frame.origin.y + imageHeight - 23 - 2*x);
    NSMutableAttributedString *attributedString;
    attributedString = [[NSMutableAttributedString alloc] initWithString:[questions objectAtIndex:i] attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaItalic size:15 + x/6]}];
    
    NSError *error2 = nil;
    NSRegularExpression *    regex2 = [NSRegularExpression regularExpressionWithPattern:LocalizedString(@"boldKey") options:0 error:&error2];
    
    NSArray *matches = [regex2 matchesInString:[questions objectAtIndex:i]
                                       options:0
                                         range:NSMakeRange(0, labelForQ.text.length)];
    
    for (NSTextCheckingResult *match in matches)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:kFontHelvaticaBoldItalic size:15 + x/6]
                               range:matchRange];
      
    }
    
    if ([countryName isEqualToString:@"Philippines"]) {
      NSError *error3 = nil;
      NSRegularExpression *    regex3 = [NSRegularExpression regularExpressionWithPattern:@"development and beauty and a platform" options:0 error:&error3];
      
      NSArray *matches3 = [regex3 matchesInString:[questions objectAtIndex:i]
                                         options:0
                                           range:NSMakeRange(0, labelForQ.text.length)];
      
      for (NSTextCheckingResult *match in matches3)
      {
        NSRange matchRange = [match rangeAtIndex:0];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:kFontHelvaticaBoldItalic size:15 + x/6]
                                 range:matchRange];
        
      }

      
    }
    labelForQ.attributedText = attributedString;
  }
  
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollImage) {
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    if (scrollViews.contentOffset.x <= -1) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(0, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }
    else if (scrollViews.contentOffset.x >= (kScreenWidth * 9)+1 ) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(kScreenWidth * 9, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }
    else{
      self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
    }
  }
}

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
//   if (scrollViews == scrollImage) {
//  CGFloat pageWidth = scrollViews.contentOffset.x;
//  int i = pageWidth/kScreenWidth;
//  NSString* tittle = [NSString stringWithFormat:@"%d. %@",i + 1,[arraykeyStrength objectAtIndex:i]];
//  [self designHeaderOfTheScreen:@"menu_icon.png" :tittle :@"":@"":@""];
//   }
//}

- (void)setOffsetRadio:(CGFloat)offsetRadios{
  CGFloat offsetRadio;
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    [scrollImage setContentOffset:CGPointMake(scrollImage.frame.size.width + scrollImage.frame.size.width * offsetRadios, 0)];
  }
}

-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

@end
