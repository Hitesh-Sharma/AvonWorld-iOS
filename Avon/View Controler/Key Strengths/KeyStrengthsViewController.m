//
//  KeyStrengthsViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "KeyStrengthsViewController.h"
#import "KeyStrengthTableViewCell.h"
#import "GlobalPresenceViewController.h"
#import "NewKeyStrengthDetailViewController.h"

@interface KeyStrengthsViewController ()
{
  IBOutlet UITableView *tableForKeyStrength;
  NSMutableArray *keyStrength;
  
}
@end

@implementation KeyStrengthsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Our Strengths") :@"":@"":@""];
  
  keyStrength = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"key1"),LocalizedString(@"key2"),LocalizedString(@"key3"),LocalizedString(@"key4"),LocalizedString(@"key5"),LocalizedString(@"key6"),LocalizedString(@"key7"),LocalizedString(@"key8"),LocalizedString(@"key9"),LocalizedString(@"key10"), nil];

  
  tableForKeyStrength.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
  tableForKeyStrength.separatorColor = [UIColor clearColor];
  tableForKeyStrength.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

#pragma mark - UITableView Life Cycle
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
  return [keyStrength count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableForKeyStrength.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  int i = 0;
  float heightForTime = 30;
  if (IS_IPAD) {
    i = 30;
    heightForTime = 40;
  }else  if (kScreenHeight > 667) {
    heightForTime = 42;
  }
  
  NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:[keyStrength objectAtIndex:indexPath.row] attributes:@
                                        {
                                        NSFontAttributeName: [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:18 + i/5]
                                        }];
  CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - 80 - i/4 , CGFLOAT_MAX}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil];
  CGSize size = rect.size;
  CGFloat height = size.height;
  return (height + heightForTime  + i);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  int i = 0;
  float heightForTime = 30;
  if (IS_IPAD) {
    i = 30;
    heightForTime = 40;
  }else  if (kScreenHeight > 667) {
    heightForTime = 42;
  }
  NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:[keyStrength objectAtIndex:indexPath.row] attributes:@
                                        {
                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:18 + i/5]
                                        }];
  CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - 80 - i/4 , CGFLOAT_MAX}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil];
  CGSize size = rect.size;
  CGFloat height = size.height;
  height = height + heightForTime + i;
  
  NSString *simpleTableIdentifier = @"KeyStrengthTableViewCell";
  KeyStrengthTableViewCell *cell = (KeyStrengthTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  NSArray *  nib = [[NSBundle mainBundle] loadNibNamed:@"KeyStrengthTableViewCell" owner:self options:nil];;
  cell = [[KeyStrengthTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  cell = [nib objectAtIndex:0];
  
  
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.contentView.frame = CGRectMake(0, 0, kScreenWidth, height );
  
  cell.labelKey.frame = CGRectMake(40 + i/4, height/2 - size.height/2, kScreenWidth - 80, size.height);
  cell.labelNo.frame = CGRectMake(10, cell.labelKey.frame.origin.y, 30 + i/4, 21 + i/3);
  cell.imageArrow.frame = CGRectMake(kScreenWidth - 40,height/2 - (30 + i/3)/2 , 30, 30 + i/3);
  cell.lableLine.frame = CGRectMake(0, height - 8, kScreenWidth + 10, 10);
  cell.imageArrow.image = [UIImage imageNamed:@"next_ico.png"];
  cell.lableLine.backgroundColor = [UIColor clearColor];
  cell.lableLine.textColor = [UIColor blackColor];
  cell.lableLine.font = [UIFont fontWithName:kFontHelvaticaLight size:5 + i/12];
  cell.labelKey.text = [keyStrength objectAtIndex:indexPath.row];
 
  cell.labelKey.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + i/5];
  cell.labelNo.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + i/5];
  NSInteger index = indexPath.row + 1;
  cell.labelNo.text =  [NSString stringWithFormat:@"%ld.",(long)index];
  
  if (indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 8) {
    cell.backgroundColor = [UIColor colorWithRed:(204.0f/255.0f) green:(204.0f/255.0f) blue:(204.0f/255.0f) alpha:1.0f];
  }else if (indexPath.row == 1 || indexPath.row == 5 || indexPath.row == 9){
    cell.backgroundColor = [UIColor colorWithRed:(237.0f/255.0f) green:(0.0f/255.0f) blue:(140.0f/255.0f) alpha:1.0f];
  }else if (indexPath.row == 2 || indexPath.row == 6 || indexPath.row == 10){
    cell.backgroundColor = [UIColor colorWithRed:(158.0f/255.0f) green:(142.0f/255.0f) blue:(179.0f/255.0f) alpha:1.0f];
  }else if (indexPath.row == 3 || indexPath.row == 7 ){
    cell.backgroundColor = [UIColor colorWithRed:(139.0f/255.0f) green:(200.0f/255.0f) blue:(224.0f/255.0f) alpha:1.0f];
  }

  
  return cell;
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    NewKeyStrengthDetailViewController *obj = [[NewKeyStrengthDetailViewController alloc]initWithNibName:@"NewKeyStrengthDetailViewController" bundle:nil];
  obj.indexPathRow = indexPath.row;
    [self.navigationController pushViewController:obj animated:NO];
    
 
}


@end
