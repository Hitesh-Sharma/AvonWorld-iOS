//
//  NewMissionViewController.m
//  Avon
//
//  Created by Shipra Singh on 08/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NewMissionViewController.h"

@interface NewMissionViewController ()<UIScrollViewDelegate>
{
IBOutlet UIScrollView *scrollImage;
NSMutableArray *upperText;
NSMutableArray *heading;
NSMutableArray *colorForR;
NSMutableArray *colorForG;
NSMutableArray *colorForB;
NSMutableArray *imageFor1x;
NSMutableArray *imageFor3x;
NSMutableArray *imageForIpad;

CGFloat offsetRadio;
}
@end

@implementation NewMissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Our Mission") :@"":@"":@""];
  
  //Array of text
  heading = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Our Mission"),LocalizedString(@"Our Values"),LocalizedString(@"Our Vision"), nil];
  upperText = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Mission1"),LocalizedString(@"Mission2"),LocalizedString(@"Mission3"), nil];
  
  //Array of RGB color
  colorForR = [[NSMutableArray alloc]initWithObjects:@"80",@"216",@"210", nil];
  colorForG = [[NSMutableArray alloc]initWithObjects:@"52",@"0",@"42", nil];
  colorForB = [[NSMutableArray alloc]initWithObjects:@"46",@"108",@"52", nil];
  
  //Array of image
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
  
  if ([countryName isEqualToString:@"Philippines"]) {
    imageFor1x = [[NSMutableArray alloc]initWithObjects:@"our_mission_Ph@2x.png",@"key_2@2x.png",@"our_vision_Ph@2x.png",nil];
    imageFor3x = [[NSMutableArray alloc]initWithObjects:@"our_mission_iphone6_Ph.png",@"key_2@3x.png",@"our_vision_iphone6_Ph.png", nil];
    imageForIpad = [[NSMutableArray alloc]initWithObjects:@"our_mission_ipad_Ph.png",@"key_2_ipad@2x.png",@"our_vision_ipad_Ph.png", nil];
  }else{
    imageFor1x = [[NSMutableArray alloc]initWithObjects:@"our_mission@2x.png",@"key_2@2x.png",@"our_vision@2x.png",nil];
    imageFor3x = [[NSMutableArray alloc]initWithObjects:@"our_mission_iphone6.png",@"key_2@3x.png",@"our_vision_iphone6.png", nil];
    imageForIpad = [[NSMutableArray alloc]initWithObjects:@"our_mission_ipad.png",@"key_2_ipad@2x.png",@"our_vision_ipad.png", nil];
  }

 
  
   [self createingSlider];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - customizing View

-(void)createingSlider{
  scrollImage.frame = CGRectMake(0,60,kScreenWidth , kScreenHeight - 60);
  scrollImage.contentSize = CGSizeMake(CGRectGetWidth(scrollImage.frame) * 3,0);
  
  int height;
  int imageHeight;
  int x = 0;
  
  int size = 25;
  if (kScreenHeight > 667) {
    size = 35;
  }else if(kScreenHeight == 568){
    size = 30;
  }else if(kScreenHeight == 667){
    size = 30;
  }
  
  if (IS_IPAD) {
    height = (70 * 780/100);
    imageHeight = (70 * height)/100;
    x = 30;
  }else{
    height = (80 * kScreenWidth/100);
    imageHeight = (80 * height)/100;
  }
  
  for (UIView *v  in scrollImage.subviews) {
    if ([v isKindOfClass:[UIScrollView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
  }
  
  for (int i = 0 ; i<3; i++) {
    UILabel* labelText = [[UILabel alloc]init];
    UIImageView* imageAvon = [[UIImageView alloc]init];
    UIImageView* imageTaiwan = [[UIImageView alloc]init];
    UIScrollView *viewBackgroundForKey = [[UIScrollView alloc]init];
    UIView *viewBackgroundForScroll = [[UIView alloc]init];
    UILabel *labelForHedding = [[UILabel alloc]init];
    UILabel *labelForUpperText = [[UILabel alloc]init];
    UIImageView *imageMiddle = [[UIImageView alloc]init];
    UIImageView *imageLower = [[UIImageView alloc]init];
    
    viewBackgroundForScroll.frame = CGRectMake(scrollImage.frame.size.width * i,0,scrollImage.frame.size.width , scrollImage.frame.size.height );
    viewBackgroundForKey.frame = CGRectMake(0, 0, kScreenWidth, viewBackgroundForScroll.frame.size.height);
    
    viewBackgroundForKey.delegate = self;
    viewBackgroundForKey.backgroundColor = [UIColor colorWithRed:([[colorForR objectAtIndex:i] floatValue]/255.0f) green:([[colorForG objectAtIndex:i] floatValue]/255.0f) blue:([[colorForB objectAtIndex:i] floatValue]/255.0f) alpha:1.0f];
    NSAttributedString *attributedForQ = [[NSAttributedString alloc]initWithString:[upperText objectAtIndex:i] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6]}];
    CGRect rectForHedding = [attributedForQ boundingRectWithSize:(CGSize){kScreenWidth - 40 - (2*x), CGFLOAT_MAX}
                                                         options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize sizeForHedding = rectForHedding.size;
    
    CGFloat heightsForHedding = sizeForHedding.height;
    
    [scrollImage addSubview:viewBackgroundForScroll];
    [viewBackgroundForScroll addSubview:viewBackgroundForKey];
    
    if (i == 0 || i == 2) {
      labelForHedding.frame = CGRectMake(20 + x, 25 + x/2 , kScreenWidth - 40 - (2*x), 60 + x/2);
      
      labelForUpperText.frame = CGRectMake(20 + x, labelForHedding.frame.origin.y + 67 , kScreenWidth - 40 - (2*x), heightsForHedding);
      
      imageMiddle.frame = CGRectMake(20 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8, kScreenWidth - 40 - (2*x), imageHeight);
      
      
      imageLower.frame = CGRectMake(20 + x, imageMiddle.frame.origin.y  +  imageHeight - 10, kScreenWidth - 40 - (2*x), imageHeight);
       labelText.frame = CGRectMake(20 + x, imageMiddle.frame.origin.y  +  imageHeight - 10, kScreenWidth - 40 - (2*x), imageHeight);
      imageAvon.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/2 - (imageHeight/3)/2 + 10,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
     imageTaiwan.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/6 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
      [viewBackgroundForKey addSubview:labelForHedding];
      [viewBackgroundForKey addSubview:labelForUpperText];
      [viewBackgroundForKey addSubview:imageMiddle];
      [viewBackgroundForKey addSubview:labelText];
      [viewBackgroundForKey addSubview:imageAvon];
       NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
      if ([countryName isEqualToString:@"Taiwan"]) {
        [viewBackgroundForKey addSubview:imageTaiwan];
        
      }
      
    }else {
      
      
      UILabel *labelForBelief = [[UILabel alloc]init];
      UILabel *labelForIntegrity = [[UILabel alloc]init];
      UILabel *labelForRespect = [[UILabel alloc]init];
      UILabel *labelForTrust = [[UILabel alloc]init];
      UILabel *labelForHumility = [[UILabel alloc]init];
      UILabel *labelForBeliefMalaysia = [[UILabel alloc]init];
      UILabel *labelForIntegrityMalaysia = [[UILabel alloc]init];
      UILabel *labelForRespectMalaysia = [[UILabel alloc]init];
      UILabel *labelForTrustMalaysia = [[UILabel alloc]init];
      UILabel *labelForHumilityMalaysia = [[UILabel alloc]init];

      
      labelForHedding.frame = CGRectMake(20 + x, 25 + x/2 , kScreenWidth - 40 - (2*x), 60 + x/2);
      
      labelForUpperText.frame = CGRectMake(20 + x, labelForHedding.frame.origin.y + 67 , kScreenWidth - 40 - (2*x), heightsForHedding+ 5);
      
      labelForBelief.frame = CGRectMake(20 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8, kScreenWidth - 40 - (2*x), 45 + x/2);
      labelForBeliefMalaysia.frame = CGRectMake(20 + x,  labelForBelief.frame.origin.y + 42 + x/2, kScreenWidth - 40 - (2*x), 20 + x/4);
      labelForIntegrity.frame = CGRectMake(20 + x,  labelForBeliefMalaysia.frame.origin.y + 25 + x/4, kScreenWidth - 40 - (2*x), 45 + x/2);
      labelForIntegrityMalaysia.frame = CGRectMake(20 + x,  labelForIntegrity.frame.origin.y + 42 + x/2, kScreenWidth - 40 - (2*x), 20 + x/4);
      labelForRespect.frame = CGRectMake(20 + x,  labelForIntegrityMalaysia.frame.origin.y + 25 + x/4, kScreenWidth - 40 - (2*x), 45 + x/2);
      labelForRespectMalaysia.frame = CGRectMake(20 + x,  labelForRespect.frame.origin.y + 42 + x/2, kScreenWidth - 40 - (2*x), 20 + x/4);
      labelForTrust.frame = CGRectMake(20 + x,  labelForRespectMalaysia.frame.origin.y + 25 + x/4, kScreenWidth - 40 - (2*x), 45 + x/2);
      labelForTrustMalaysia.frame = CGRectMake(20 + x,  labelForTrust.frame.origin.y + 42 + x/2, kScreenWidth - 40 - (2*x), 20 + x/4);
      labelForHumility.frame = CGRectMake(20 + x,  labelForTrustMalaysia.frame.origin.y + 25 + x/4, kScreenWidth - 40 - (2*x), 45 + x/2);
      labelForHumilityMalaysia.frame = CGRectMake(20 + x,  labelForHumility.frame.origin.y + 42 + x/2, kScreenWidth - 40 - (2*x), 20 + x/4);
      
      imageLower.frame = CGRectMake(20 + x, labelForHumilityMalaysia.frame.origin.y  - 10 + x/4, kScreenWidth - 40 - (2*x), imageHeight);
       labelText.frame = CGRectMake(20 + x, labelForHumilityMalaysia.frame.origin.y  - 10 + x/4, kScreenWidth - 40 - (2*x), imageHeight);
      imageAvon.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/2 - (imageHeight/3)/2 + 10 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
      imageTaiwan.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/6 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
      
      
      
      
      NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
      
    
      [viewBackgroundForKey addSubview:labelForHedding];
      [viewBackgroundForKey addSubview:labelForUpperText];
      [viewBackgroundForKey addSubview:labelForBelief];
      [viewBackgroundForKey addSubview:labelForIntegrity];
      [viewBackgroundForKey addSubview:labelForRespect];
      [viewBackgroundForKey addSubview:labelForTrust];
      [viewBackgroundForKey addSubview:labelForHumility];
      labelForBelief.textColor = [UIColor whiteColor];
//      labelForBelief.textColor = [UIColor colorWithRed:(243.0f/255.0f) green:(119.0f/255.0f) blue:(197.0f/255.0f) alpha:1.0f];
      labelForIntegrity.textColor = [UIColor colorWithRed:(249.0f/255.0f) green:(188.0f/255.0f) blue:(202.0f/255.0f) alpha:1.0f];
      labelForRespect.textColor = [UIColor whiteColor];
//      labelForRespect.textColor = [UIColor colorWithRed:(243.0f/255.0f) green:(119.0f/255.0f) blue:(197.0f/255.0f) alpha:1.0f];
      labelForTrust.textColor = [UIColor colorWithRed:(249.0f/255.0f) green:(188.0f/255.0f) blue:(202.0f/255.0f) alpha:1.0f];
      labelForHumility.textColor = [UIColor whiteColor];
//      labelForHumility.textColor = [UIColor colorWithRed:(243.0f/255.0f) green:(119.0f/255.0f) blue:(197.0f/255.0f) alpha:1.0f];

        if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
        [viewBackgroundForKey addSubview:labelForBeliefMalaysia];
        [viewBackgroundForKey addSubview:labelForIntegrityMalaysia];
        [viewBackgroundForKey addSubview:labelForRespectMalaysia];
        [viewBackgroundForKey addSubview:labelForTrustMalaysia];
        [viewBackgroundForKey addSubview:labelForHumilityMalaysia];
        labelForBeliefMalaysia.text = @"Kepercayaan";
        labelForIntegrityMalaysia.text = @"Integriti";
        labelForRespectMalaysia.text = @"Hormat";
        labelForTrustMalaysia.text = @"Amanah";
        labelForHumilityMalaysia.text = @"Rendah Hati";
        
        labelForBeliefMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
        labelForIntegrityMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
        labelForRespectMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
        labelForTrustMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
        labelForHumilityMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
        labelForBeliefMalaysia.textAlignment = NSTextAlignmentCenter;
        labelForIntegrityMalaysia.textAlignment = NSTextAlignmentCenter;
        labelForRespectMalaysia.textAlignment = NSTextAlignmentCenter;
        labelForTrustMalaysia.textAlignment = NSTextAlignmentCenter;
        labelForHumilityMalaysia.textAlignment = NSTextAlignmentCenter;
          labelForBeliefMalaysia.textColor = [UIColor whiteColor];
          labelForIntegrityMalaysia.textColor = [UIColor whiteColor];
          labelForRespectMalaysia.textColor = [UIColor whiteColor];
          labelForTrustMalaysia.textColor = [UIColor whiteColor];
          labelForHumilityMalaysia.textColor = [UIColor whiteColor];

        
      }
     
      [viewBackgroundForKey addSubview:labelText];
      [viewBackgroundForKey addSubview:imageAvon];
      if ([countryName isEqualToString:@"Taiwan"]) {
        [viewBackgroundForKey addSubview:imageTaiwan];
        
      }
      
      labelForBelief.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:35 + x/6];
      labelForIntegrity.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:35 + x/6];
      labelForRespect.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:35 + x/6];
      labelForTrust.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:35 + x/6];
      labelForHumility.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:35 + x/6];
      labelForBelief.text = LocalizedString(@"Mission21");
      labelForIntegrity.text = LocalizedString(@"Mission22");
      labelForRespect.text = LocalizedString(@"Mission23");
      labelForTrust.text = LocalizedString(@"Mission24");
      labelForHumility.text = LocalizedString(@"Mission25");
      labelForBelief.textAlignment = NSTextAlignmentCenter;
      labelForIntegrity.textAlignment = NSTextAlignmentCenter;
      labelForRespect.textAlignment = NSTextAlignmentCenter;
      labelForTrust.textAlignment = NSTextAlignmentCenter;
      labelForHumility.textAlignment = NSTextAlignmentCenter;
      
    }
    
    labelForHedding.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:45 + x/6];
    labelForUpperText.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
    labelForHedding.numberOfLines = 0;
    labelForUpperText.numberOfLines = 0;
    labelForHedding.textAlignment = NSTextAlignmentCenter;
    labelForUpperText.textAlignment = NSTextAlignmentCenter;
    
    
    
    
    labelForHedding.text =  [heading objectAtIndex:i];
    labelForUpperText.text =  [upperText objectAtIndex:i];
    if (IS_IPAD){
      imageMiddle.image = [UIImage imageNamed:[imageForIpad objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo_ipad.png"];
       imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purposeipad@1x.png"];
      //imageLower.image = [UIImage imageNamed:@"logo_ipad@2x.png"];
    }else if (kScreenHeight > 600) {
      imageMiddle.image = [UIImage imageNamed:[imageFor3x objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo@3x.png"];
      imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@3x.png"];
      //imageLower.image = [UIImage imageNamed:@"logo@3x.png"];
    }else{
      imageMiddle.image = [UIImage imageNamed:[imageFor1x objectAtIndex:i]];
      imageAvon.image = [UIImage imageNamed:@"logo@2x.png"];
      imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@2x.png"];
     // imageLower.image = [UIImage imageNamed:@"logo@2x.png"];
    }
    
    labelForHedding.textColor = [UIColor whiteColor];
    labelForUpperText.textColor = [UIColor whiteColor];
    
    // imageMiddle.backgroundColor = [UIColor blackColor];
    imageMiddle.contentMode = UIViewContentModeScaleAspectFit;
    imageLower.contentMode = UIViewContentModeCenter;
    viewBackgroundForKey.contentSize = CGSizeMake(0, labelText.frame.origin.y + imageHeight - 48 - x - x/2);
    
    NSMutableAttributedString *attributedString;
    attributedString = [[NSMutableAttributedString alloc] initWithString:[upperText objectAtIndex:i] attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaLight size:15 + x/6]}];
    
    NSError *error2 = nil;
    NSRegularExpression *    regex2 = [NSRegularExpression regularExpressionWithPattern:LocalizedString(@"boldMission") options:0 error:&error2];
    
    NSArray *matches = [regex2 matchesInString:[upperText objectAtIndex:i]
                                       options:0
                                         range:NSMakeRange(0, labelForUpperText.text.length)];
    
    for (NSTextCheckingResult *match in matches)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:kFontHelavaticaBold size:15 + x/6]
                               range:matchRange];
      
    }
    labelForUpperText.attributedText = attributedString;
    
    NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
    NSString* logoText;
    if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
      logoText = @"Beauty for a purpose \n\n\n\n my.avon.com";
    }else  if ([countryName isEqualToString:@"Taiwan"]) {
      logoText = @"\n\n\n\n\n avon.com.tw";
    }else if ([countryName isEqualToString:@"India"]){
      logoText = @"Beauty for a purpose \n\n\n\n NEW YORK";
      
    }else if ([countryName isEqualToString:@"Philippines"]){
      logoText = @"Beauty for a purpose \n\n\n\n avon.com.ph";
      
    }

   imageAvon.backgroundColor = [UIColor clearColor];
    labelText.backgroundColor = [UIColor clearColor];
    labelText.textAlignment = NSTextAlignmentCenter;
    labelText.text = logoText;
    
    imageAvon.contentMode = UIViewContentModeCenter;
    imageTaiwan.backgroundColor = [UIColor clearColor];
    imageTaiwan.contentMode = UIViewContentModeCenter;
    NSString* compareText;
    if ([countryName isEqualToString:@"Taiwan"]) {
      compareText = @"雅芳美麗好給力";
      
    }else{
      compareText = @"Beauty for a purpose";
      
    }
  
    NSString* compareText1 = @"\n AVON";
    labelText.numberOfLines = 0;
    
    NSMutableAttributedString *attributedString1;
    attributedString1 = [[NSMutableAttributedString alloc] initWithString:labelText.text attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaLight size:14 + x/2]}];
    
    NSError *error4 = nil;
    NSRegularExpression *    regex4 = [NSRegularExpression regularExpressionWithPattern:compareText options:0 error:&error4];
    
    NSArray *matches2 = [regex4 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    NSError *error3 = nil;
    NSRegularExpression *    regex3 = [NSRegularExpression regularExpressionWithPattern:@"AVON" options:0 error:&error3];
    
    NSArray *matches1 = [regex3 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    
    for (NSTextCheckingResult *match in matches2)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontCoalhandLukeTRIAL size:30 + x]
                                range:matchRange];
      
    }
    
    for (NSTextCheckingResult *match in matches1)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontHelvaticaLight size:30 + x]
                                range:matchRange];
      NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
      [paragraphStyle setLineSpacing:15];
      paragraphStyle.alignment = NSTextAlignmentCenter;
      [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [compareText length])];
      
      
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange([compareText length], [compareText1 length])];
    
    
    labelText.attributedText = attributedString1;
    labelText.textColor = [UIColor whiteColor];

  

}}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollImage) {
    
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    if (scrollViews.contentOffset.x <= -1) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(0, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else if (scrollViews.contentOffset.x >= (kScreenWidth * 2)+1 ) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(kScreenWidth * 2, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else{
      self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
    }
  }
}

- (void)setOffsetRadio:(CGFloat)offsetRadios
{
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    
    [scrollImage setContentOffset:CGPointMake(scrollImage.frame.size.width + scrollImage.frame.size.width * offsetRadios, 0)];
  }
}

-(void)backButtonTap{
  
  [slideNavController toggleLeftMenu];
}


@end
