//
//  ProductViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductCollectionViewCell.h"
#import "HomeViewController.h"
#import "Database.h"
#import "ProductBO.h"
#import "BoutiqueLocatorViewController.h"
#import "UIImageView+WebCache.h"
#import "CategoriesBO.h"
#import "PersentageBO.h"


@interface ProductViewController ()<UIGestureRecognizerDelegate,UIScrollViewDelegate,ConnectionManager_Delegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
  
  
  IBOutlet UITableView *tableForQuantity;
  IBOutlet UIView *viewForTableQuanitity;
  IBOutlet UIImageView *buttonRightInlargeImage;
  IBOutlet UIImageView *buttonLeftInlargeImage;
  IBOutlet UIImageView *buttonCancelInlargeImage;
  IBOutlet UIView *viewForInlargeImage;
  IBOutlet UIScrollView *scrollViewForInLargeImage;
  IBOutlet UIScrollView *scrollViewForProductScreen;
  IBOutlet UIButton *buttonOk;
  IBOutlet UILabel *labelPopupText;
  IBOutlet UIView *viewForPopup;
  IBOutlet UIView *viewForPopUpScreen;
  NSMutableArray* arrayCategoryAndProduct;
  NSMutableArray* arrayForPercentage;
  ConnectionManager *requestManager;
  NSInteger indexPathRow;
  
}
@property (nonatomic) CGFloat offsetRadio;
@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  labelPopupText.text = LocalizedString(@"TextForProductScreen");
  [buttonOk setTitle:LocalizedString(@"OK") forState:UIControlStateNormal];
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  arrayCategoryAndProduct = [[NSMutableArray alloc]init];
  arrayForPercentage = [[NSMutableArray alloc]init];
    if ( [[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {
      [self designHeaderOfTheScreen:@"back_icon.png" :@"" :@"next_ico.png":@"":@""];
    }else{
      [self designHeaderOfTheScreen:@"menu_icon.png" :@"" :@"next_ico.png":@"":@""];
    }
  
  
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
    [self fromDatabase];
   // if ([countryID isEqualToString:@"1"]) {
  
  //  }
    ALERT(@"", LocalizedString(@"No Internet Connection"));
    
  }else {

  [self sendDataToServerWithTag:TASK_GET_PRODUCT_CATEGORY];
  }

 }

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  
}

-(void)fromDatabase{
 
  NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
  
  NSMutableArray* data = [[NSMutableArray alloc]init];
  data = [[Database getSharedInstance]getCategories:countryID];
  arrayForPercentage = [[Database getSharedInstance]getMinMax:countryID];
  for (CategoriesBO* obj in data) {
    NSMutableArray* dataProduct = [[NSMutableArray alloc]init];
    dataProduct = [[Database getSharedInstance]getProduct:countryID withcategoryId:obj.categoryId];
    CategoriesBO* allData = [[CategoriesBO alloc]init];
    allData = obj;
    allData.productArray = dataProduct;
    [arrayCategoryAndProduct addObject:allData];
    
  }
  if (arrayCategoryAndProduct.count != 0)
  {
    CategoriesBO* category = [arrayCategoryAndProduct objectAtIndex:0];
    NSString* tittle = category.categoryName;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {
      [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
    }else{
      [self designHeaderOfTheScreen:@"menu_icon.png" :tittle :@"next_ico.png":@"":@""];
    }
    
    [self customView];
    [self screenAppears];
  }
}

#pragma mark - CustomView
-(void)customView{
  
    int lowerHeight;
    int buttonHeight;
    int heights;
    int i = 0;
    if (IS_IPAD) {
      lowerHeight = (50 * 500)/100;
      buttonHeight = (25 * lowerHeight)/100;
      heights = (80 * 780/100);
      i = 30;
    }else{
      lowerHeight = (50 * kScreenWidth)/100;
      buttonHeight = (25 * lowerHeight)/100;
      heights = (90 * kScreenWidth/100);
    }
    
    scrollViewForProductScreen.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
    scrollViewForProductScreen.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForProductScreen.frame) * ((arrayCategoryAndProduct.count)*2 + 1), 0);
    scrollViewForProductScreen.pagingEnabled = YES;
    scrollViewForProductScreen.delegate = self;
 
  
    //customise view for inlarge Image
    viewForInlargeImage.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    scrollViewForInLargeImage.frame = CGRectMake(40 + 2*i,(kScreenHeight + 60 )/2 - heights/2,kScreenWidth - 80 - 4*i , heights);
    scrollViewForInLargeImage.pagingEnabled = YES;
    scrollViewForInLargeImage.delegate = self;
    buttonCancelInlargeImage.frame = CGRectMake((kScreenWidth - 50 - 2*i) - 15i, (kScreenHeight + 60)/2  - heights/2 - 40, 50, 50);
    buttonLeftInlargeImage.frame = CGRectMake((40 + 2*i)/2 - 25, (kScreenHeight + 60)/2 - 50/2 - i/20, 50 + i/5, 50 + i/10);
    buttonRightInlargeImage.frame = CGRectMake((kScreenWidth - 40 - 2*i) +((40 + 2*i)/2 - 25), (kScreenHeight + 60)/2 - 50/2 - i/20, 50 + i/5, 50 + i/10);
  
  
    UITapGestureRecognizer *leftClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftClick:)];
    UITapGestureRecognizer *rightClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightClick:)];
    UITapGestureRecognizer *cancelClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelClick:)];
    buttonLeftInlargeImage.userInteractionEnabled=YES;
    buttonRightInlargeImage.userInteractionEnabled=YES;
    buttonCancelInlargeImage.userInteractionEnabled=YES;
    [buttonLeftInlargeImage addGestureRecognizer:leftClick];
    [buttonRightInlargeImage addGestureRecognizer:rightClick];
    [buttonCancelInlargeImage addGestureRecognizer:cancelClick];
    [self roundCornersOfView:scrollViewForInLargeImage radius:scrollViewForInLargeImage.frame.size.width/23 borderColor:[UIColor whiteColor] borderWidth:0];
  
  
    //customView For Pop Up Screen
  
    viewForPopUpScreen.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    viewForPopup.frame = CGRectMake(0,kScreenHeight - lowerHeight, kScreenWidth, lowerHeight);
    labelPopupText.frame = CGRectMake(25 + (4* i), 20 , kScreenWidth - 50 - 8*i , lowerHeight/2);
    buttonOk.frame = CGRectMake(25 + (4 *i), viewForPopup.frame.size.height - buttonHeight - 10 - i/6, kScreenWidth - 50 - 8*i, buttonHeight);
   labelPopupText.font = [UIFont fontWithName:kFontHelvaticaLight size:20 + i/3];
  [self roundCornersOfView:buttonOk radius:buttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  
  
  
    //customView for Quantity Selection View
  
    int heightForQuantityView = (15 *((kScreenHeight - 60)/2))/100;
    tableForQuantity.separatorColor = [UIColor clearColor];
    buttonOk.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
  
  
    //table Quantity View
    viewForTableQuanitity.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    tableForQuantity.frame = CGRectMake(40 + 2*i, (kScreenHeight + 60 )/2 - ((heightForQuantityView - i/4) * 6)/2, kScreenWidth - 80 - 4*i, (heightForQuantityView - i/4) * 6);
  
   [self roundCornersOfView:tableForQuantity radius:(heightForQuantityView - i/4)/8  borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] borderWidth:0];

  [self.view addSubview: scrollViewForProductScreen];
  [viewForInlargeImage addSubview:scrollViewForInLargeImage];

}

#pragma mark - customise screen
-(void)screenAppears{
  
  [self created];
  
  //firs app use check
  
//  if ([[NSUserDefaults standardUserDefaults]boolForKey:kFirstLogin] == YES) {
//    [[NSUserDefaults standardUserDefaults]setBool: NO forKey:kFirstLogin];
//    [[NSUserDefaults standardUserDefaults] synchronize];
  
    [self.view addSubview:viewForPopUpScreen];
    
    //animation for popup
    
    CGAffineTransform baseTransform = viewForPopup.transform;
    viewForPopup.transform = CGAffineTransformTranslate(baseTransform,0,viewForPopup.bounds.size.height);
    
    [UIView animateWithDuration: 0.5 animations:^{
      viewForPopup.transform = baseTransform;
    }];
    
 // }
  
  
}


//create view for scrollview

-(void)created{
 NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  int lowerHeight;
  int buttonHeight;
  //int heights;
  int x = 0;
  if (IS_IPAD) {
    lowerHeight = (50 * 500)/100;
    buttonHeight = (25 * lowerHeight)/100;
    //heights = (80 * 780/100);
    x = 30;
  }else{
    lowerHeight = (50 * kScreenWidth)/100;
    buttonHeight = (25 * lowerHeight)/100;
//heights = (90 * kScreenWidth/100);
  }

   int size = 0;
  if (kScreenHeight == 736) {
    size = 23;
  }else if(kScreenHeight == 568){
    size = 6;
  }else if(kScreenHeight == 667){
    size = 18;
  }
  for (UIView *v  in scrollViewForProductScreen.subviews) {
    if ([v isKindOfClass:[UICollectionView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIButton class]]) {
      [v removeFromSuperview];
    }
  }

  for (int i = 0 ; i<([arrayCategoryAndProduct count]*2 + 1); i++) {
    
   if (i % 2 == 0 && i != ([arrayCategoryAndProduct count]*2)) {
     UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
     UICollectionView* collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(kScreenWidth * i, 0, kScreenWidth, kScreenHeight - 60) collectionViewLayout:layout];
    // collectionView.backgroundColor = [UIColor blackColor];
      [collectionView setDataSource:self];
      [collectionView setDelegate:self];
      collectionView.alwaysBounceVertical = YES;
      collectionView.tag = i/2 + 1011;
      [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
      collectionView.backgroundColor = [UIColor clearColor];
      [scrollViewForProductScreen addSubview:collectionView];

    }else{

      UIView *viewSave = [[UIView alloc]initWithFrame:CGRectMake(kScreenWidth * i, 0, kScreenWidth, kScreenHeight - 60)];
      UIButton *butiqueLocator = [[UIButton alloc]initWithFrame:CGRectMake(25 + 4*x, kScreenHeight - buttonHeight - 70 -i/6, kScreenWidth - 50 - 8*x, buttonHeight )];
      UILabel* labelAveragePrice = [[UILabel alloc]initWithFrame:CGRectMake(0, (kScreenHeight - 60)/2 - (kScreenHeight)/4 - 55 - size/2  - (3*x)/2 - x, kScreenWidth, 30 + x)];
      UILabel* labelMinimumSaving = [[UILabel alloc]initWithFrame:CGRectMake(0, (kScreenHeight - 60)/2 - 55 - size/2 -  (3*x)/2 - x, kScreenWidth, 30 + x)];
      UILabel* labelMaximumSaving = [[UILabel alloc]initWithFrame:CGRectMake(0, (kScreenHeight - 60)/2 + (kScreenHeight)/4 - 55 - size/2  - (3*x)/2 - x, kScreenWidth, 30 + x)];
     UILabel* lableAveragePriceNumber = [[UILabel alloc]initWithFrame:CGRectMake(kScreenWidth/2 - kScreenWidth/2, (kScreenHeight - 60)/2 - (kScreenHeight)/4 - 25 - size/2 - (3*x)/2,  kScreenWidth, 50 + size + 3 *x )];
     UILabel* lableMinimumSavingNumber = [[UILabel alloc]initWithFrame:CGRectMake(0, (kScreenHeight - 60)/2 - 25 - size/2 - (3*x)/2, kScreenWidth, 50 + size + 3 *x)];
     UILabel* lableMaximumSavingNumber = [[UILabel alloc]initWithFrame:CGRectMake(0, (kScreenHeight - 60)/2 + (kScreenHeight)/4 - 25 - size/2 - (3*x)/2, kScreenWidth, 50 + size + 3 *x)];

      [scrollViewForProductScreen addSubview:viewSave];
      [viewSave addSubview:labelAveragePrice];
      [viewSave addSubview:lableAveragePriceNumber];
      [viewSave addSubview:labelMinimumSaving];
      [viewSave addSubview:lableMinimumSavingNumber];
      [viewSave addSubview:labelMaximumSaving];
      [viewSave addSubview:lableMaximumSavingNumber];
      [viewSave addSubview:butiqueLocator];
      butiqueLocator.hidden = YES;
      butiqueLocator.titleLabel.textColor = [UIColor whiteColor];
      butiqueLocator.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
      butiqueLocator.titleLabel.font =  [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
      [butiqueLocator addTarget:self
                         action:@selector(boutiqueLocator:)
               forControlEvents:UIControlEventTouchUpInside];
      [butiqueLocator setTitle:LocalizedString(@"Boutique LocatorOnProduct") forState:UIControlStateNormal];
      [self roundCornersOfView:butiqueLocator radius:buttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
      viewSave.backgroundColor = [UIColor whiteColor];

      int value = 0;

      if (i == ([arrayCategoryAndProduct count]*2)) {
       // butiqueLocator.hidden = NO;
      for (CategoriesBO *data in arrayCategoryAndProduct ) {
        value = value + [data.totalProductPriceCount intValue];
      }
      }else{
       
        CategoriesBO *data = [arrayCategoryAndProduct objectAtIndex:floor(i/2)];
        for (ProductBO *product in data.productArray) {
           if ([product.quantity length] != 0) {
              value = value + ([product.itemAvaragePrice intValue] * [product.quantity intValue]);
           }
        }
        data.totalProductPriceCount = [NSString stringWithFormat:@"%d",value ];
        
        
      }
      PersentageBO* persent = [arrayForPercentage objectAtIndex:0];
      NSLog(@"%f",((value * [persent.minimumPersentage floatValue])/100));
      NSLog(@"%f",((value * [persent.maximumPersentage floatValue])/100));
      int minAverage = roundf((value * [persent.minimumPersentage floatValue])/100);
      int maxAverage = roundf((value * [persent.maximumPersentage floatValue])/100);
      if ([countryName isEqualToString:@"Philippines"]){
       
        lableAveragePriceNumber.text = [ @"₱ " stringByAppendingString: [NSString stringWithFormat:@"%d",value]];
        lableMinimumSavingNumber.text = [ @"₱ " stringByAppendingString:[NSString stringWithFormat:@"%d",minAverage]];
        lableMaximumSavingNumber.text = [ @"₱ " stringByAppendingString:[NSString stringWithFormat:@"%d",maxAverage]];

      }else  if ([countryName isEqualToString:@"Taiwan"]){
        lableAveragePriceNumber.text = [[NSString stringWithFormat:@"%d",value]stringByAppendingString: LocalizedString(@"Currency") ];
        lableMinimumSavingNumber.text = [[NSString stringWithFormat:@"%d",minAverage] stringByAppendingString:LocalizedString(@"Currency")];
        lableMaximumSavingNumber.text = [[NSString stringWithFormat:@"%d",maxAverage] stringByAppendingString:LocalizedString(@"Currency")];
        
      }else{
      lableAveragePriceNumber.text = [LocalizedString(@"Currency") stringByAppendingString: [NSString stringWithFormat:@"%d",value]];
      lableMinimumSavingNumber.text = [LocalizedString(@"Currency") stringByAppendingString:[NSString stringWithFormat:@"%d",minAverage]];
      lableMaximumSavingNumber.text = [LocalizedString(@"Currency") stringByAppendingString:[NSString stringWithFormat:@"%d",maxAverage]];
      }
      
      labelAveragePrice.textColor = [UIColor blackColor];
      labelAveragePrice.text = LocalizedString(@"Average Price");
      labelAveragePrice.textAlignment = NSTextAlignmentCenter;
      labelAveragePrice.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + x/2];
      labelMinimumSaving.textColor = [UIColor blackColor];
      labelMinimumSaving.text = LocalizedString(@"Minimum Savings");
      labelMinimumSaving.textAlignment = NSTextAlignmentCenter;
      labelMinimumSaving.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + x/2];
      labelMaximumSaving.textColor = [UIColor blackColor];
      labelMaximumSaving.text = LocalizedString(@"Maximum Savings");
      labelMaximumSaving.textAlignment = NSTextAlignmentCenter;
      labelMaximumSaving.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + x/2];
      
      
      lableAveragePriceNumber.textColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.f alpha:1.0f];
      lableMinimumSavingNumber.textColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.f alpha:1.0f];
      lableMaximumSavingNumber.textColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.f alpha:1.0f];
      
      
      lableAveragePriceNumber.textAlignment = NSTextAlignmentCenter;
      lableAveragePriceNumber.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:lableMaximumSavingNumber.frame.size.height -  lableMaximumSavingNumber.frame.size.height/4];
      lableMinimumSavingNumber.textAlignment = NSTextAlignmentCenter;
      lableMinimumSavingNumber.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:lableMaximumSavingNumber.frame.size.height -  lableMaximumSavingNumber.frame.size.height/4];
      lableMaximumSavingNumber.textAlignment = NSTextAlignmentCenter;
      lableMaximumSavingNumber.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:lableMaximumSavingNumber.frame.size.height -  lableMaximumSavingNumber.frame.size.height/4];
      
   NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];

   if ([countryName isEqualToString:@"Philippines"] || [countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
     if ( i == [arrayCategoryAndProduct count]*2) {
       butiqueLocator.hidden = NO;

     }else{
       butiqueLocator.hidden = YES;
     }
     }else{
    
   butiqueLocator.hidden = YES;
   }
  }
    

//    [self roundCornersOfView:buttonButiqueLocator radius:buttonButiqueLocator.frame.size.height/8 borderColor:[UIColor purpleColor] borderWidth:0];

    //set color
     }
}


//create view for large image
-(void)createdForInLargeImage{
 int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  int height;
  int imageHeight;
  int x = 0;

  int size = 0;
  if (kScreenHeight > 667) {
    size = 23;
  }else if(kScreenHeight == 568){
    size = 6;
  }else if(kScreenHeight == 667){
    size = 18;
  }

  if (IS_IPAD) {
    height = (80 * 780/100);
    imageHeight = (70 * height)/100;
    x = 30;
  }else{
    height = (80 * kScreenWidth/100);
    imageHeight = (70 * height)/100;
  }

   for (UIView *v  in scrollViewForInLargeImage.subviews) {
    if ([v isKindOfClass:[UICollectionView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIButton class]]) {
      [v removeFromSuperview];
    }
  }
 
  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];
  NSArray* arrayProduct = obj.productArray;

  for (int i = 0 ; i <= [arrayProduct count]; i++) {

    UIView *viewBackgroundForProductDetail = [[UIView alloc]init];
    viewBackgroundForProductDetail.frame = CGRectMake(scrollViewForInLargeImage.frame.size.width * i,0,scrollViewForInLargeImage.frame.size.width , scrollViewForInLargeImage.frame.size.height);
         UIImageView *imageInlarge = [[UIImageView alloc]init];
    imageInlarge.frame = CGRectMake(40 + x, 40 + x,viewBackgroundForProductDetail.frame.size.width - 80 - 2*x , imageHeight - x);
    UILabel *labelInlargeImageName = [[UILabel alloc]init];
    
    
    
    UILabel *labelPrice = [[UILabel alloc]init];
    labelPrice.frame = CGRectMake(30, viewBackgroundForProductDetail.frame.size.height - 35 - 2*x/3,viewBackgroundForProductDetail.frame.size.width - 60 , 30 + x/2);
    
//    labelInlargeImageName.minimumScaleFactor = 0.7;
//    labelInlargeImageName.adjustsFontSizeToFitWidth = YES;
    imageInlarge.contentMode = UIViewContentModeScaleAspectFit;
    if (i != [arrayProduct count]) {
    
    ProductBO *obj = [arrayProduct objectAtIndex:i];
    
    NSAttributedString *attributedForHedding = [[NSAttributedString alloc]initWithString:obj.itemName attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:20 + x/3]}];
      CGRect rectForHedding = [attributedForHedding boundingRectWithSize:(CGSize){viewBackgroundForProductDetail.frame.size.width - 2 * (10 + 2*x/3), CGFLOAT_MAX}
                                                                 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
     
      CGSize sizeForHedding = rectForHedding.size;
      
      CGFloat heightsForHedding = sizeForHedding.height;
       int imageSizeEnd = imageInlarge.frame.size.height + imageInlarge.frame.origin.y;
      if (heightsForHedding > (30 + x/2)) {
        if ((imageSizeEnd + 5) < (viewBackgroundForProductDetail.frame.size.height - 2*heightsForHedding)) {
           labelInlargeImageName.frame = CGRectMake(10 + 2*x/3, viewBackgroundForProductDetail.frame.size.height - 2*heightsForHedding,viewBackgroundForProductDetail.frame.size.width - 2 * (10 + 2*x/3) , heightsForHedding + 5);
        }else{
          labelInlargeImageName.frame = CGRectMake(10 + 2*x/3, imageSizeEnd + 5,viewBackgroundForProductDetail.frame.size.width - 2 * (10 + 2*x/3) , viewBackgroundForProductDetail.frame.size.height - (imageSizeEnd + 5 + 35 + 2*x/3));
          
        }
       
      }else{
        labelInlargeImageName.frame = CGRectMake(10 + 2*x/3, viewBackgroundForProductDetail.frame.size.height - 60 - x,viewBackgroundForProductDetail.frame.size.width - 2 * (10 + 2*x/3) , 30 + x/2);
      }
      labelInlargeImageName.adjustsFontSizeToFitWidth = YES;
     labelInlargeImageName.numberOfLines = 0;
      //labelInlargeImageName.backgroundColor = [UIColor redColor];

      
      
      
      
    [imageInlarge sd_setImageWithURL:[NSURL URLWithString:obj.itemFullImage]
                     placeholderImage:nil
                              options:SDWebImageProgressiveDownload
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            }];

    labelInlargeImageName.text = obj.itemName;
      if ([countryName isEqualToString:@"Philippines"]){
        labelPrice.text = [@"₱" stringByAppendingString:obj.itemAvaragePrice];
        }else if ([countryName isEqualToString:@"Taiwan"]){
      labelPrice.text = [obj.itemAvaragePrice stringByAppendingString:LocalizedString(@"Currency") ];
        }else{
     labelPrice.text = [LocalizedString(@"Currency") stringByAppendingString:obj.itemAvaragePrice];
      }
    }
    labelPrice.textAlignment = NSTextAlignmentCenter;
    labelPrice.textColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
    labelInlargeImageName.textAlignment = NSTextAlignmentCenter;
      labelInlargeImageName.textColor = [UIColor blackColor];
    viewBackgroundForProductDetail.backgroundColor = [UIColor whiteColor];
    imageInlarge.backgroundColor = [UIColor clearColor];
    [scrollViewForInLargeImage addSubview:viewBackgroundForProductDetail];
    [viewBackgroundForProductDetail addSubview:imageInlarge];
    [viewBackgroundForProductDetail addSubview:labelInlargeImageName];
    [viewBackgroundForProductDetail addSubview:labelPrice];
    labelInlargeImageName.font = [UIFont fontWithName:kFontHelvaticaLight size:20 + x/3];
    labelPrice.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + x/3];

      }
}


-(void)imageClick:(UITapGestureRecognizer*)recognize{
  
   int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  CategoriesBO*obj = [arrayCategoryAndProduct objectAtIndex:page/2];
  NSArray* arrayProduct = obj.productArray;

  NSInteger section = (recognize.view.tag)/1000;
  NSInteger row = (recognize.view.tag)%1000;
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
 
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
  if(remoteHostStatus == NotReachable) {
    
    //if ([countryID isEqualToString:@"1"]) {
      
       scrollViewForInLargeImage.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForInLargeImage.frame) * [arrayProduct count], CGRectGetHeight(scrollViewForInLargeImage.frame));
      
   // }
  }else{

  if ([obj.isLodingFinish isEqualToString:@"1"]) {
    scrollViewForInLargeImage.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForInLargeImage.frame) * [arrayProduct count], CGRectGetHeight(scrollViewForInLargeImage.frame));

  }else{
  scrollViewForInLargeImage.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForInLargeImage.frame) * ([arrayProduct count] + 1), CGRectGetHeight(scrollViewForInLargeImage.frame));
  }
  }

   [self createdForInLargeImage];

  if (indexPath.row == 0) {

    buttonRightInlargeImage.hidden = NO;
    buttonLeftInlargeImage.hidden = YES;

  }else if((indexPath.row == [arrayProduct count] - 1) && ([obj.isLodingFinish isEqualToString:@"1"]|| remoteHostStatus == NotReachable)){
    buttonRightInlargeImage.hidden = YES;
    buttonLeftInlargeImage.hidden = NO;
  }else{
    buttonRightInlargeImage.hidden = NO;
    buttonLeftInlargeImage.hidden = NO;
  }

  [scrollViewForInLargeImage setContentOffset:CGPointMake((indexPath.row * CGRectGetWidth(scrollViewForInLargeImage.frame)) , 0) animated:YES];
   [self.view addSubview:viewForInlargeImage];

   }




#pragma mark - UICollectionViewDelegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
  return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
  return 0.0;
}


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {

  NSUInteger numberOfRow = 0;
  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:view.tag - 1011];
     numberOfRow = [obj.productArray count];
  return numberOfRow;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
  return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  int height = 208;

  if (kScreenHeight == 667) {
    height = 246;

  }else if (kScreenHeight == 736)
  {
    height = 272;

  }else if (kScreenWidth > 736)
  {
    height = 506;

  }

  UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionView.collectionViewLayout;
    CGSize size = flowLayout.itemSize;
    size.width = kScreenWidth/2;
    size.height=height;
     return CGSizeMake (kScreenWidth/2 ,height );
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {

  int height = 202;
  int pixel = 2;
  int i = 0;
  if (kScreenHeight == 667) {
    height = 236;
    pixel = 5;
  }else if (kScreenHeight == 736)
  {
    height = 260;
    pixel = 6;
  }else if (IS_IPAD)
  {
    height = 486;
    pixel = 10;
    i = 30;
  }


  int width = (kScreenWidth - 3 * pixel)/2;
  int heightForText = (14 * height)/100;
  int widthofQuntity = (26 * width)/100;

  NSString *identifier;
      ProductCollectionViewCell *cell;
      identifier= @"ProductCollectionViewCell";
      static BOOL nibMyCellloaded = NO;

      UINib *nib;

    nib = [UINib nibWithNibName:@"ProductCollectionViewCell" bundle: nil];

         [cv registerNib:nib forCellWithReuseIdentifier:identifier];
      nibMyCellloaded = YES;

      ProductCollectionViewCell *celltwoImage1 = (ProductCollectionViewCell*)[cv dequeueReusableCellWithReuseIdentifier:@"ProductCollectionViewCell" forIndexPath:indexPath];
      cell=celltwoImage1;
  
  for (UIView *v  in cell.viewForImage.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }

  }
UIImageView * imageProducts;


    cell.contentView.frame = CGRectMake(0, 0, kScreenWidth/2 , height + 2 * pixel);

  if (indexPath.row == 0) {
    cell.viewBackGround.frame = CGRectMake(pixel, pixel,  width, height + pixel + pixel);
  }else if (indexPath.row == 1){
     cell.viewBackGround.frame = CGRectMake(pixel/2 , pixel,  width, height + pixel + pixel);
  }else if (indexPath.row %2 == 0) {
    cell.viewBackGround.frame = CGRectMake(pixel, pixel,  width, height + pixel + pixel );
  }else{
    cell.viewBackGround.frame = CGRectMake(pixel/2 , pixel,  width, height + pixel + pixel);
  }
  imageProducts = [[UIImageView alloc]initWithFrame:CGRectMake(20 + i, 20 + i,  width - 40 - 2*i, height - heightForText - 25 - 2 *i)];
    imageProducts.contentMode = UIViewContentModeScaleAspectFit;
   cell.viewForImage.frame = CGRectMake(0, 0,width , (height - heightForText));

    cell.labelProductName.frame = CGRectMake(7 + i/3,(height - heightForText) + 4, width - widthofQuntity - 9 -  i/3, heightForText );
  cell.labelLineForImage.frame = CGRectMake(0, (height - heightForText), width, 1);
   cell.labelSelectedProductCount.frame = CGRectMake(cell.viewBackGround.frame.size.width - widthofQuntity , (height - heightForText) + 4, widthofQuntity/2 + widthofQuntity/4, heightForText );
  cell.ImageArrow.frame = CGRectMake(cell.viewBackGround.frame.size.width - widthofQuntity/3 + 1, (height - heightForText/2) + 1 , 10, 7);
  cell.labelLineForCount.frame = CGRectMake(width - widthofQuntity - 2, (height - heightForText) + 1, 1, height - heightForText);
  cell.buttonQuantityClick.frame = CGRectMake(width-widthofQuntity, (height - heightForText), widthofQuntity, height - heightForText);



  cell.labelProductName.font = [UIFont fontWithName:kFontHelvaticaLight size:16 + i/4];
  cell.labelSelectedProductCount.font = [UIFont fontWithName:kFontHelvaticaLight size:13 + i/4];

  if (IS_IPAD) {
    cell.ImageArrow.image = [UIImage imageNamed:@"drop_icon_ipad.png"];
  }else{
    cell.ImageArrow.image = [UIImage imageNamed:@"drop_icon.png"];
  }

  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:cv.tag - 1011];
  ProductBO* product = [obj.productArray objectAtIndex:indexPath.row] ;
 
  [imageProducts sd_setImageWithURL:[NSURL URLWithString:product.itemThumbImage]
                         placeholderImage:nil
                                  options:SDWebImageProgressiveDownload
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                }];

   // imageProducts.image = [UIImage imageNamed:obj.itemThumbImage];
  
    cell.labelProductName.text= product.itemName;
  //  cell.imageProduct.tag=indexPath.row;
  if ([product.quantity length ]!= 0) {
    cell.labelSelectedProductCount.text = product.quantity;
  }else{
    cell.labelSelectedProductCount.text = @"QTY";
  }
    UITapGestureRecognizer *onClickImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
    UITapGestureRecognizer *onClickCountView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countViewClick:)];
     imageProducts.userInteractionEnabled=YES;
//  if (indexPath.row % 2 == 0) {
//     [imageProducts setImage:[UIImage imageNamed:@"play_icon_ipad.png"]];
//  }else{
//     [imageProducts setImage:[UIImage imageNamed:@"map_icon_ipad.png"]];
//  }
 
    [imageProducts addGestureRecognizer:onClickImage];
    imageProducts.tag = indexPath.row + 1000;
 // imageProducts.backgroundColor = [UIColor purpleColor];
  cell.buttonQuantityClick.userInteractionEnabled=YES;
  [cell.buttonQuantityClick addGestureRecognizer:onClickCountView];
  cell.buttonQuantityClick.tag = indexPath.row + 1000;
  [cell.viewForImage addSubview:imageProducts];
      return cell;
    }

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
		// TODO: Select Item
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
		// TODO: Deselect item
}



#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollViewForProductScreen) {

   if (scrollViews.contentOffset.y <= -1 ) {
    CGPoint offset = scrollViews.contentOffset;
    offset.y = 0;
    scrollViews.contentOffset = offset;
  }
    if (scrollViews.contentOffset.x <= -1) {
     // [scrollViews setScrollEnabled:NO];
    [scrollViewForProductScreen setContentOffset:CGPointMake(0, 0) animated:NO];
      //[scrollViews setScrollEnabled:YES];
    }else if (scrollViews.contentOffset.x >= (kScreenWidth * [arrayCategoryAndProduct count]*2)+2 ) {
      [scrollViews setScrollEnabled:NO];
      [scrollViewForProductScreen setContentOffset:CGPointMake(kScreenWidth * ([arrayCategoryAndProduct count]*2 ), 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    } else if (scrollViews.contentOffset.y == 0 ){
   self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
    }

  }else if (scrollViews.tag == 44){
    UIImageView *horizontalIndicator = ((UIImageView *)[tableForQuantity.subviews objectAtIndex:(tableForQuantity.subviews.count-2)]);
    //set color to horizontal indicator
    [horizontalIndicator setBackgroundColor:[UIColor blueColor]];
  }
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {

  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  CategoriesBO*obj;
  NSArray* arrayProduct = [[NSArray alloc]init];
  if (page != [arrayCategoryAndProduct count]*2) {
    obj = [arrayCategoryAndProduct objectAtIndex:page/2];
    arrayProduct = obj.productArray;
  }
 
  
  if (scrollViews.tag != 33 && scrollViews.tag != 35 && scrollViews.tag != 44) {
      CGFloat currentOffset = scrollViews.contentOffset.y;
      CGFloat maximumOffset = scrollViews.contentSize.height - scrollViews.frame.size.height;
    //method call for load more data
      if (maximumOffset - currentOffset <= 10.0) {
      [self performSelector:@selector(loadDataDelayed) withObject:nil afterDelay:1];
      }
   }

  if (scrollViews.tag == 33) {
    CGFloat pageWidth = scrollViewForInLargeImage.contentOffset.x;
    int i = pageWidth/scrollViewForInLargeImage.frame.size.width;

    if (i == 0) {

      buttonRightInlargeImage.hidden = NO;
      buttonLeftInlargeImage.hidden = YES;

    }else if(i == [arrayProduct count]){
     [self loadDataDelayed];

    }else{

      if (i == [arrayProduct count]-1 && [obj.isLodingFinish isEqualToString:@"0"]) {
        buttonRightInlargeImage.hidden = NO;
        buttonLeftInlargeImage.hidden = NO;
      }else if(i == [arrayProduct count]-1){
        buttonRightInlargeImage.hidden = YES;
        buttonLeftInlargeImage.hidden = NO;
      }else{
        buttonRightInlargeImage.hidden = NO;
        buttonLeftInlargeImage.hidden = NO;
      }

    }


  }else{
  if (scrollViews == scrollViewForProductScreen) {
  CGFloat pageWidth = scrollViews.contentOffset.x;
  int i = pageWidth/kScreenWidth;
//    screen = i;
//
////    if (scrollView.contentOffset.x >= 0) {
////      if (![tittleforchek isEqualToString:[arrayProductType objectAtIndex:i]]) {
         [self created];
////      }
//
//   // }
    NSString *tittle;
    if (i == [arrayCategoryAndProduct count]*2 ) {
      tittle = LocalizedString(@"Total Savings");
    }else if (page % 2 == 0) {
      tittle = obj.categoryName;
    }else{
      tittle = obj.categoryNameTotalSaving;
    }
    
    if ([tittle isEqualToString:LocalizedString(@"Total Savings")] && i == [arrayCategoryAndProduct count]*2  )
    {
      [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"home_icon.png":@"":@""];
    }else{
      if (scrollViewForProductScreen.contentOffset.x/kScreenWidth == 0) {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] != YES) {
          [self designHeaderOfTheScreen:@"menu_icon.png" :tittle :@"next_ico.png":@"":@""];
        }else{
          [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
        }
        
      }else{
        [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
      }
    }
    if (scrollViewForProductScreen.contentOffset.x >= 0) {
      
    }
  }
  }
}


- (void)setOffsetRadio:(CGFloat)offsetRadio
{
  if (_offsetRadio != offsetRadio)
  {
    _offsetRadio = offsetRadio;

    [scrollViewForProductScreen setContentOffset:CGPointMake(scrollViewForProductScreen.frame.size.width + scrollViewForProductScreen.frame.size.width * offsetRadio, 0)];
  }
}



#pragma mark - UITableView Life Cycle
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

  return 13;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableForQuantity.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

  int i = 0;
   int heightForQuantityView = (15 *((kScreenHeight - 60)/2))/100;
  if (IS_IPAD) {
    i = 30;
  }
    return (heightForQuantityView - i/4);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;

   CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];
  ProductBO* product = [obj.productArray objectAtIndex:indexPathRow];
 
  
  int i = 0;
  int heightForQuantityView = (15 *((kScreenHeight - 60)/2))/100;

  if (IS_IPAD) {
  i = 30;
  }

  static NSString *simpleTableIdentifier = @"SimpleTableItem";

  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  }

  //remove view to avoid duplicacy
  for (UIView *v in cell.contentView.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UILabel class]]) {
      [v removeFromSuperview];
    }
  }
  cell.selectionStyle = UITableViewCellSelectionStyleNone;


  cell.contentView.frame = CGRectMake(0, 0, tableForQuantity.frame.size.width, (heightForQuantityView - i/4) );
  UIImageView * radioButtonImage = [[UIImageView alloc]initWithFrame:CGRectMake(tableForQuantity.frame.size.width - 60, 5, 50, (heightForQuantityView - i/4) - 10)];

  UILabel * lableCount = [[UILabel alloc]initWithFrame:CGRectMake(30,0 , 70, (heightForQuantityView - i/4))];
  UILabel * lableLine = [[UILabel alloc]initWithFrame:CGRectMake(0,(heightForQuantityView - i/4)-1 ,tableForQuantity.frame.size.width  ,1 )];

lableLine.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
   NSInteger index = indexPath.row;
  lableCount.text =  [NSString stringWithFormat:@"%ld",(long)index];
  lableCount.font = [UIFont fontWithName:kFontTimesRomanItalic size:heightForQuantityView/2];

  radioButtonImage.contentMode = UIViewContentModeCenter;
  if (indexPath.row == [product.quantity intValue]) {
    cell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
    if (IS_IPAD) {
       radioButtonImage.image = [UIImage imageNamed:@"radio_btn_select_ipad.png"];
    }else{
     radioButtonImage.image = [UIImage imageNamed:@"radio_btn_select@2x.png"];
    }
  }else{
    cell.contentView.backgroundColor = [UIColor whiteColor];
    if (IS_IPAD) {
      radioButtonImage.image = [UIImage imageNamed:@"radio_btn_ipad.png"];
    }else{
      radioButtonImage.image = [UIImage imageNamed:@"radio_btn@2x.png"];
    }

  }
  radioButtonImage.tag = indexPath.row;
  [cell.contentView addSubview:radioButtonImage];
  [cell.contentView addSubview:lableCount];
  [cell.contentView addSubview:lableLine];


  return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                              indexPath];
  oldCell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];

  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  
  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];
  ProductBO* product = [obj.productArray objectAtIndex:indexPathRow];
 
  NSInteger index = indexPath.row;
  product.quantity = [NSString stringWithFormat:@"%ld",(long)index];
  NSIndexPath *indexPat = [NSIndexPath indexPathForRow:indexPathRow inSection:0];
  NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPat, nil];
  UICollectionView *collectionView = (UICollectionView *)[self.view viewWithTag:page/2 + 1011];
  
  
   [collectionView reloadItemsAtIndexPaths:indexPaths];
   [viewForTableQuanitity removeFromSuperview];

}





-(void)countViewClick:(UITapGestureRecognizer*)recognize{

  NSInteger section = (recognize.view.tag)/1000;
  NSInteger row = (recognize.view.tag)%1000;
  
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
  indexPathRow = indexPath.row;
  
  [tableForQuantity reloadData];
  [self.view addSubview:viewForTableQuanitity];
  
}


-(void)backButtonTap{
 int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  NSString *tittle;
  CategoriesBO*obj;
  if ((page - 1) >= 0) {
    obj = [arrayCategoryAndProduct objectAtIndex:(page - 1)/2];
    }

  
  if ((page - 1) == [arrayCategoryAndProduct count]*2 ) {
    tittle = LocalizedString(@"Total Savings");
  }else if ((page - 1) % 2 == 0) {
    tittle = obj.categoryName;
  }else{
    tittle = obj.categoryNameTotalSaving;
  }

  

  if (scrollViewForProductScreen.contentOffset.x/kScreenWidth <= 0 ) {
     scrollViewForProductScreen.delegate = nil;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {
      [scrollViewForProductScreen setContentOffset:CGPointMake(0, 0) animated:YES];

    [self.navigationController popViewControllerAnimated:YES];
    }else{
      if ( [[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {

      }
       scrollViewForProductScreen.delegate = self;
     // [self screenAppears];
      [slideNavController toggleLeftMenu];
    }

  }else {
    
     if (scrollViewForProductScreen.contentOffset.x/kScreenWidth == 1) {
       if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] != YES) {
         [self designHeaderOfTheScreen:@"menu_icon.png" :tittle :@"next_ico.png":@"":@""];
       }else{
          [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
       }

     }else{
  [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
     }
     [scrollViewForProductScreen setContentOffset:CGPointMake((page  *kScreenWidth)- kScreenWidth , 0) animated:YES];
    if (scrollViewForProductScreen.contentOffset.x >= 0) {
      [self created];
    }
  }
  }


-(void)rightButtonTap{

  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  NSString *tittle;
  CategoriesBO*obj;
  if ((page + 1) < [arrayCategoryAndProduct count]*2 ) {
    obj = [arrayCategoryAndProduct objectAtIndex:(page + 1)/2];
     }
  if ((page + 1) == [arrayCategoryAndProduct count]*2 ) {
    tittle = LocalizedString(@"Total Savings");
  }else if ((page + 1) % 2 == 0) {
    
    tittle = obj.categoryName;
  }else{
    tittle = obj.categoryNameTotalSaving;
  }


  if (scrollViewForProductScreen.contentOffset.x/kScreenWidth == ([arrayCategoryAndProduct count]*2)) {
    scrollViewForProductScreen.delegate = nil;

    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {
      [self.navigationController popViewControllerAnimated:YES];
    }else{
      HomeViewController *mainVC=[[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
      [self.navigationController pushViewController:mainVC animated:NO];

    }
  }else {
    if (scrollViewForProductScreen.contentOffset.x/kScreenWidth == ([arrayCategoryAndProduct count]*2)-1)
    {
    [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"home_icon.png":@"":@""];
    }else{
      [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
    }
  [scrollViewForProductScreen setContentOffset:CGPointMake((page  *kScreenWidth) + kScreenWidth, 0) animated:YES];
    if (scrollViewForProductScreen.contentOffset.x >= 0) {
      [self created];
    }
  }
 }


- (IBAction)buttonOkClick:(id)sender {

  [UIView animateWithDuration:0.5
                        delay:0.1
                      options: UIViewAnimationCurveEaseIn
                   animations:^{
                     viewForPopup.frame = CGRectMake(0, kScreenHeight, viewForPopup.frame.size.width, viewForPopup.frame.size.height);

                   }
                   completion:^(BOOL finished){
                    [viewForPopUpScreen removeFromSuperview];
                   }];
    }



- (IBAction)okTouch:(id)sender {
  buttonOk.titleLabel.textColor = [UIColor whiteColor];
  buttonOk.backgroundColor = [UIColor colorWithRed:212.0f/255.0f green:0.0f/255.0f blue:123.0f/255.0f alpha:1.0f];
}




-(void)leftClick:(UITapGestureRecognizer*)recognize{
  CGFloat pageWidth = scrollViewForInLargeImage.contentOffset.x;
  int i = pageWidth/scrollViewForInLargeImage.frame.size.width;

  [scrollViewForInLargeImage setContentOffset:CGPointMake((i *scrollViewForInLargeImage.frame.size.width) - scrollViewForInLargeImage.frame.size.width, 0) animated:YES];

  if (i == 1) {
        buttonRightInlargeImage.hidden = NO;
    buttonLeftInlargeImage.hidden = YES;
  }else{
    buttonRightInlargeImage.hidden = NO;
    buttonLeftInlargeImage.hidden = NO;
  }
}

-(void)rightClick:(UITapGestureRecognizer*)recognize{

  CGFloat pageWidth = scrollViewForInLargeImage.contentOffset.x;
  int i = pageWidth/scrollViewForInLargeImage.frame.size.width;
  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];
  
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  int totalProduct = 0;
  if(remoteHostStatus == NotReachable) {
    totalProduct = [obj.productArray count] - 2;
    if(i == [obj.productArray count] - 1){
 [scrollViewForInLargeImage setContentOffset:CGPointMake((i *scrollViewForInLargeImage.frame.size.width), 0) animated:YES];
    }else{
      [scrollViewForInLargeImage setContentOffset:CGPointMake((i *scrollViewForInLargeImage.frame.size.width) + scrollViewForInLargeImage.frame.size.width, 0) animated:YES];
    }
  }else{
    totalProduct = [obj.productArray count] - 1;
   [scrollViewForInLargeImage setContentOffset:CGPointMake((i *scrollViewForInLargeImage.frame.size.width) + scrollViewForInLargeImage.frame.size.width, 0) animated:YES];
  }

  
  if(i == totalProduct){
    buttonRightInlargeImage.hidden = YES;
    buttonLeftInlargeImage.hidden = NO;
  }else{
    buttonRightInlargeImage.hidden = NO;
    buttonLeftInlargeImage.hidden = NO;
  }
  if(i == [obj.productArray count] - 1){
    [self loadDataDelayed];
  }

}



- (IBAction)boutiqueLocator:(id)sender {

  [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kisBoutiqueByButton];
  [[NSUserDefaults standardUserDefaults]synchronize];
  BoutiqueLocatorViewController *obj = [[BoutiqueLocatorViewController alloc]initWithNibName:@"BoutiqueLocatorViewController" bundle:nil];
  [self.navigationController pushViewController:obj animated:NO];

}





-(void)loadDataDelayed{
 
  int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
  CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];

  if ([obj.isLodingFinish isEqualToString:@"0"]||obj.isLodingFinish.length == 0) {
    
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
      ALERT(@"", LocalizedString(@"No Internet Connection"));
 
    }else{
        
    [self sendDataToServerWithTag:TASK_GET_PRODUCT];
    }
  }else{

    scrollViewForInLargeImage.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForInLargeImage.frame) * [obj.productArray count] , 0);
    [self createdForInLargeImage];

    buttonRightInlargeImage.hidden = YES;
   // buttonLeftInlargeImage.hidden = YES;

    [scrollViewForInLargeImage setContentOffset:CGPointMake((([obj.productArray count] - 1) * CGRectGetWidth(scrollViewForInLargeImage.frame)) , 0) animated:YES];
  }

}

-(void)cancelClick:(UITapGestureRecognizer*)recognize{
  [viewForInlargeImage removeFromSuperview];
  
}




-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_GET_PRODUCT_CATEGORY:
    {
      [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      NSString* numberCount;
      if ([countryID isEqualToString:@"1" ]) {
        numberCount = @"100";
      }else{
        numberCount = @"10";
      }
      
      NSString* timeStamp = @"";
      NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
      
      if ([countryName isEqualToString:@"East Malaysia"]) {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamEMalaysia ]) {
          
        timeStamp =  [[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamEMalaysia ];
          
        }
        
        
      }else if ([countryName isEqualToString:@"West Malaysia"]) {
       if ([[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamWMalaysia ]) {
        timeStamp =  [[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamWMalaysia ];
       }
       
      }else if ([countryName isEqualToString:@"Taiwan"]) {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamTaiwan ]) {
         timeStamp =  [[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamTaiwan ];
        }
        
      }else if ([countryName isEqualToString:@"Philippines"]) {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamPhil ]) {
         timeStamp =  [[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamPhil ];
        }
      }else if ([countryName isEqualToString:@"India"]) {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamIndia ]) {
         timeStamp =  [[NSUserDefaults standardUserDefaults]objectForKey:kTimeStamIndia ];
        }
      }

      
      
      [dictRoot setObject:timeStamp forKey:kTimeStamp];
      [dictRoot setObject:countryID forKey:kCountryId];
      [dictRoot setObject:@"0" forKey:kOffset];
     
      [dictRoot setObject:numberCount forKey:kLimit];
      [dictRoot setObject:@"asc" forKey:@"order"];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_PRODUCT_CATEGORY withURL:kUrlForProduct with:@"POST"];
      break;
    }
    case TASK_GET_PRODUCT:
    {
      int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
      CategoriesBO*obj = [arrayCategoryAndProduct objectAtIndex:page/2];
      NSArray* arrayProduct = obj.productArray;
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      
      [dictRoot setObject:countryID forKey:kCountryId];
      [dictRoot setObject:obj.categoryId forKey:kCateogryId];
      [dictRoot setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arrayProduct count]] forKey:kOffset];
       //[dictRoot setObject:@"" forKey:kTimeStamp];
        [dictRoot setObject:@"10" forKey:kLimit];
       [dictRoot setObject:@"asc" forKey:@"order"];
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_PRODUCT withURL:kUrlForProduct with:@"POST"];
      break;
    }
    case TASK_GET_PERCENTAGE:
    {
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      
      [dictRoot setObject:countryID forKey:kCountryId];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_PERCENTAGE withURL:kUrlForPercentage with:@"POST"];
      break;
    }

      
    default:
      break;
      

   }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_GET_PRODUCT_CATEGORY:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllCategories = [[NSMutableArray alloc]init];
          arrayAllCategories = [[recievedDictionary objectForKey:kResponse]objectForKey:kCategories];
          NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
           NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
          NSMutableArray* arrayAllCategoriesDatabase = [[NSMutableArray alloc]init];
          arrayAllCategoriesDatabase = [[Database getSharedInstance]getCategories:countryID];
          
           if ([arrayAllCategories count] > 0) {
           for (NSDictionary *obj in arrayAllCategories) {
          CategoriesBO* category = [[CategoriesBO alloc]initWithResponse:obj product:[[recievedDictionary objectForKey:kResponse]objectForKey:kProducts]];
            
             if ([countryName isEqualToString:@"East Malaysia"]) {
              
               [[NSUserDefaults standardUserDefaults]setObject:[recievedDictionary objectForKey:@"timestamp"] forKey:kTimeStamEMalaysia];
            
             }else if ([countryName isEqualToString:@"West Malaysia"]) {
               
               [[NSUserDefaults standardUserDefaults]setObject:[recievedDictionary objectForKey:@"timestamp"] forKey:kTimeStamWMalaysia];
           
             }else if ([countryName isEqualToString:@"Taiwan"]) {
               
               [[NSUserDefaults standardUserDefaults]setObject:[recievedDictionary objectForKey:@"timestamp"] forKey:kTimeStamTaiwan];
             
             }else if ([countryName isEqualToString:@"Philippines"]) {
               
               [[NSUserDefaults standardUserDefaults]setObject:[recievedDictionary objectForKey:@"timestamp"] forKey:kTimeStamPhil];
            
             }else if ([countryName isEqualToString:@"India"]) {
             
               [[NSUserDefaults standardUserDefaults]setObject:[recievedDictionary objectForKey:@"timestamp"] forKey:kTimeStamIndia];
            
             }
             
             
             [[NSUserDefaults standardUserDefaults] synchronize];
              //if ([countryID isEqualToString:@"1"]) {
          if ([arrayAllCategoriesDatabase count] == 0) {
          [[Database getSharedInstance]storeCategory:category];
        }else{
              int count = 0;
      for (CategoriesBO* obj in arrayAllCategoriesDatabase) {
      if ([category.categoryId isEqualToString:obj.categoryId]) {
          count++;
                   
        }
      }
    if (count == 0) {
      [[Database getSharedInstance]storeCategory:category];
                  }
                
              }
             // }
             
              [arrayCategoryAndProduct addObject:category];
          
           }
          CategoriesBO* category = [arrayCategoryAndProduct objectAtIndex:0];
          NSString* tittle = category.categoryName;
          
          if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsClickOnHome] == YES) {
            [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"next_ico.png":@"":@""];
          }else{
            [self designHeaderOfTheScreen:@"menu_icon.png" :tittle :@"next_ico.png":@"":@""];
          }

             
          [self sendDataToServerWithTag:TASK_GET_PERCENTAGE];
         
           }
        }else if([[recievedDictionary objectForKey:kResponseCode]integerValue] == 202){
          [self fromDatabase];
          
        }else{
            ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        break;
      }
      case TASK_GET_PRODUCT:
      {
        int page = scrollViewForProductScreen.contentOffset.x / scrollViewForProductScreen.frame.size.width;
        int num = 0;
        CategoriesBO* obj = [arrayCategoryAndProduct objectAtIndex:page/2];
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllCategories = [[NSMutableArray alloc]init];
          arrayAllCategories = [[recievedDictionary objectForKey:kResponse]objectForKey:kProducts];
          if ([arrayAllCategories count] > 0) {
            
       for (NSDictionary *ojectAtIndex in arrayAllCategories) {
         ProductBO* product = [[ProductBO alloc]initWithResponse:ojectAtIndex];
         NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
         NSMutableArray* arrayAllCategoriesDatabase = [[NSMutableArray alloc]init];
         arrayAllCategoriesDatabase = [[Database getSharedInstance]getProduct:countryID withcategoryId:obj.categoryId];
         
         //  if ([countryID isEqualToString:@"1"]) {
         
         
         UIImageView* im = [[UIImageView alloc]init];
         [im sd_setImageWithURL:[NSURL URLWithString:product.itemThumbImage]];
         
         UIImageView* ims = [[UIImageView alloc]init];
         [ims sd_setImageWithURL:[NSURL URLWithString:product.itemFullImage]];
         
         
         if ([arrayAllCategoriesDatabase count] == 0) {
           [[Database getSharedInstance]storeProduct:product andCountryId:countryID withCategoryId:obj.categoryId];
         }else{
           int count = 0;
           for ( ProductBO *obj in  arrayAllCategoriesDatabase) {
             if ([product.itemId isEqualToString:obj.itemId]) {
               count++;
             }
           }
           if (count == 0) {
             [[Database getSharedInstance]storeProduct:product andCountryId:countryID withCategoryId:obj.categoryId];
           }
         }
         // }
         
       
         int counting = 0;
         for (ProductBO *pro in obj.productArray) {
           if ([pro.itemName isEqualToString:product.itemName]) {
             counting++;
           }
         }
         if (counting == 0) {
           [obj.productArray addObject:product];
         }

         
         num = 1;
       }
          }else{
            obj.isLodingFinish = @"1";
          }
         
          
      scrollViewForInLargeImage.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForInLargeImage.frame) * ([obj.productArray count] + num), CGRectGetHeight(scrollViewForInLargeImage.frame));
      [self createdForInLargeImage];
          if ([obj.isLodingFinish isEqualToString:@"1"]) {
            buttonRightInlargeImage.hidden = YES;
            buttonLeftInlargeImage.hidden = NO;
        [scrollViewForInLargeImage setContentOffset:CGPointMake((([obj.productArray count]-1) * CGRectGetWidth(scrollViewForInLargeImage.frame)) , 0) animated:YES];
          }else{
            buttonRightInlargeImage.hidden = NO;
            buttonLeftInlargeImage.hidden = NO;
            
          }

    UICollectionView *collectionView = (UICollectionView *)[self.view viewWithTag:page/2 + 1011];
    [collectionView reloadData];
        }else{
            ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        break;
      }
      case TASK_GET_PERCENTAGE:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllCategories = [[NSMutableArray alloc]init];
          arrayAllCategories = [recievedDictionary objectForKey:kResponse];
          
          NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
          NSMutableArray* arrayAllPersentageDatabase = [[NSMutableArray alloc]init];
          arrayAllPersentageDatabase = [[Database getSharedInstance]getMinMax:countryID];
          
          
          for (NSDictionary *ojectAtIndex in arrayAllCategories) {
                    
          PersentageBO* category = [[PersentageBO alloc]initWithResponse:ojectAtIndex];
           // if ([countryID isEqualToString:@"1"]) {
              if ([arrayAllPersentageDatabase count] == 0) {
                [[Database getSharedInstance]storeMinMax:category withCountryId:countryID];
              }else{
                int count = 0;
            for (PersentageBO* obj in arrayAllPersentageDatabase) {
              if ([obj.maximumPersentage isEqualToString:category.maximumPersentage] && [obj.minimumPersentage isEqualToString:category.minimumPersentage]) {
                count++;
              }
                }
                if (count == 0) {
                  [[Database getSharedInstance]storeMinMax:category withCountryId:countryID];
                }

              }
//            }
          [arrayForPercentage addObject:category];
          }
          [self customView];
          [self screenAppears];
        }else{
            ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        
        break;
      }

    
      default:
        break;
    }
  }
  else
  {
     ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  [self remove_Loader];
}



@end
