//
//  BaseViewController.h
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "AppDelegate.h"
#import "Constants.pch"
#import "MBProgressHUD.h"
#import "LocalizeHelper.h"
#import "ConnectionManager.h"
#import "NSJSONSerialization+UAAdditions.h"
#import "NSDictionary+BVJSONString.h"
#import "ASIHTTPRequest.h"
#import "Reachability.h"
#import "Database.h"

@interface BaseViewController : UIViewController<SlideNavigationControllerDelegate,MBProgressHUDDelegate>
{
  SlideNavigationController* slideNavController;
  AppDelegate *app;
   UILabel *lblHeader;
  MBProgressHUD *HUD;
  
}

/*This method design topHeader view of 60Pixel*/
-(void)designHeaderOfTheScreen:(NSString *)leftIcon :(NSString *)strTitle :(NSString *)rightIcon :(NSString*)pre :(NSString *)next;
-(void)roundCornersOfView:(UIView *)view radius:(int)radius borderColor:(UIColor *)color borderWidth:(int)borderWidth;
- (void)drawBorderOnView:(UIView *)view borderColor:(UIColor *)color borderWidth:(int)width;
-(void)circle:(UIView *)view borderColor:(UIColor *)color borderWidth:(int)borderWidth;
-(void)animationViweUp:(UIView *)whiteView andTransparentView:(UIView *)transparentView;
-(void)animationViweDown:(UIView *)whiteView andTransparentView:(UIView *)transparentView;
- (void)show_Loader;
-(void)remove_Loader;

@end
