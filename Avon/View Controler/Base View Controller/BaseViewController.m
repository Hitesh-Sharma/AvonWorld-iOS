//
//  BaseViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   slideNavController = [SlideNavigationController sharedInstance];
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
      // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*This method design topHeader view of 60Pixel*/
-(void)designHeaderOfTheScreen:(NSString *)leftIcon :(NSString *)strTitle :(NSString *)rightIcon :(NSString*)pre :(NSString *)next{
  UIView *temp=(UIView *)[self.view viewWithTag:1000];
  [temp removeFromSuperview];
   UIView *viewHeader=[[UIView alloc] initWithFrame:CGRectMake(0, 0 , kScreenWidth, 60)];
  viewHeader.backgroundColor = [UIColor colorWithRed:(237.0f/255.0f) green:(0.0f/255.0f) blue:(140.0f/255.0f) alpha:1.0f];
  
   if (leftIcon !=nil) {
    /*Top Left button of the screen*/
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 15, 60, 45)];
    [btn addTarget:self action:@selector(backButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btn];
    
    
    UIImageView *imgVBackIcon=[[UIImageView alloc] initWithFrame:CGRectMake(10, 35, 19, 13)];
    if ([leftIcon isEqualToString:@"back_icon.png"]) {
      [imgVBackIcon setFrame:CGRectMake(10, 33, 9, 15)];
    }
    [imgVBackIcon setBackgroundColor:[UIColor clearColor]];
    [imgVBackIcon setImage:[UIImage imageNamed:leftIcon]];
    [viewHeader addSubview:imgVBackIcon];
    
    
  }
  if (strTitle !=nil&& [strTitle length]!=0) {
    /*Title Top Center of the screen*/
    lblHeader=[[UILabel alloc] initWithFrame:CGRectMake(40, 20, kScreenWidth-80, 40)];
    [lblHeader setTextAlignment:NSTextAlignmentCenter];
    [lblHeader setText:strTitle];
    [lblHeader setTextColor:[UIColor whiteColor]];
     lblHeader.font = [UIFont fontWithName:kFontHelavaticaBold size:18];
   // [lblHeader setFont:titleTextFont];
    [viewHeader addSubview:lblHeader];
  }
  if (rightIcon !=nil) {
    /*Top Right button of the screen*/
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(kScreenWidth-60, 15, 60, 45)];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn addTarget:self action:@selector(rightButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [viewHeader addSubview:btn];
    
    
    UIImageView *imgVBackIcon=[[UIImageView alloc] initWithFrame:CGRectMake(kScreenWidth-19, 33, 9, 15)];
    if ([rightIcon isEqualToString:@"home_icon.png"]) {
      [imgVBackIcon setFrame:CGRectMake(kScreenWidth-23, 33, 17, 13)];
    }else if ([rightIcon isEqualToString:@"location_icon.png"]) {
      [imgVBackIcon setFrame:CGRectMake(kScreenWidth-32, 33, 13, 19)];
    }
    [imgVBackIcon setBackgroundColor:[UIColor clearColor]];
    [imgVBackIcon setImage:[UIImage imageNamed:rightIcon]];
    [viewHeader addSubview:imgVBackIcon];
  }
  if (next !=nil&& [next length]!=0) {
    /*Title Top Center of the screen*/
    UILabel* nextlabel=[[UILabel alloc] init];
    if ([next isEqualToString:@"Home"]) {
      nextlabel.frame = CGRectMake(kScreenWidth-59, 20, 50, 40);
    }else{
       nextlabel.frame = CGRectMake(kScreenWidth-59, 20, 35, 40);
    }
   
    [nextlabel setTextAlignment:NSTextAlignmentRight];
    [nextlabel setText:next];
    [nextlabel setTextColor:[UIColor whiteColor]];
    nextlabel.font = [UIFont fontWithName:kFontHelvaticaLight size:16];
    // [lblHeader setFont:titleTextFont];
    [viewHeader addSubview:nextlabel];
    
  }
  if (pre !=nil&& [pre length]!=0) {
    /*Title Top Center of the screen*/
    UILabel* prelabel=[[UILabel alloc] initWithFrame:CGRectMake(24, 20, 35, 40)];
   
    [prelabel setTextAlignment:NSTextAlignmentLeft];
    [prelabel setText:pre];
    [prelabel setTextColor:[UIColor whiteColor]];
    prelabel.font = [UIFont fontWithName:kFontHelvaticaLight size:16];
    // [lblHeader setFont:titleTextFont];
    [viewHeader addSubview:prelabel];
    
  }


  [self.view addSubview:viewHeader];
  [self.view bringSubviewToFront:viewHeader];
}

-(void)backButtonTap{
  
}

-(void)rightButtonTap{
  
}


-(void)circle:(UIView *)view borderColor:(UIColor *)color borderWidth:(int)borderWidth{
  view.layer.cornerRadius = view.frame.size.width / 2;
  view.clipsToBounds = YES;
  view.layer.borderWidth = borderWidth;
  view.layer.borderColor = color.CGColor;
}

-(void)roundCornersOfView:(UIView *)view radius:(int)radius borderColor:(UIColor *)color borderWidth:(int)borderWidth
{
  view.layer.masksToBounds = YES;
  view.layer.cornerRadius = radius;
  view.layer.borderWidth = borderWidth;
  view.layer.borderColor = color.CGColor;
  
}

- (void)drawBorderOnView:(UIView *)view borderColor:(UIColor *)color borderWidth:(int)width
{
  [self roundCornersOfView:view radius:0 borderColor:color borderWidth:width];
}



-(void)animationViweUp:(UIView *)whiteView andTransparentView:(UIView *)transparentView{
  
  CGAffineTransform baseTransform = whiteView.transform;
  whiteView.transform = CGAffineTransformTranslate(baseTransform,0,whiteView.bounds.size.height);
  
  [UIView animateWithDuration: 0.5 animations:^{
    whiteView.transform = baseTransform;
  }];
  
}

-(void)animationViweDown:(UIView *)whiteView andTransparentView:(UIView *)transparentView{

  [UIView animateWithDuration:0.5
                        delay:0.1
                      options: UIViewAnimationCurveEaseIn
                   animations:^{
                     whiteView.frame = CGRectMake(0, kScreenHeight, whiteView.frame.size.width, whiteView.frame.size.height);
                     
                   }
                   completion:^(BOOL finished){
                     [transparentView removeFromSuperview];
                     whiteView.frame = CGRectMake(0, kScreenHeight/2 + kScreenHeight/12 , kScreenWidth, kScreenHeight/2 - kScreenHeight/12);
//                     whiteView.frame = CGRectMake(0, kScreenHeight/2, kScreenWidth, kScreenHeight/2);
                   }];

}


#pragma mark - loader

- (void)show_Loader
{
  if (HUD!=nil) {
    HUD=nil;
  }
  HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES] ;
  HUD.frame = CGRectMake(0, 0, 120, 143);
  HUD.delegate = self;
  
  HUD.color = [UIColor colorWithRed:(95.0f/255.0f) green:(59.0f/255.0f) blue:(120.0f/255.0f) alpha:0.6f];
  // HUD.mode = MBProgressHUDModeAnnularDeterminate;
  //  NSString *strloadingText = [NSString stringWithFormat:@"Loading..."];
  //  HUD.labelText = strloadingText;
  
  
}

-(void)remove_Loader
{
  [HUD hide:YES afterDelay:0];
  if (HUD.superview) {
    [HUD removeFromSuperview];
  }
  HUD = nil;
}

@end
