//
//  GlobalPresenceViewController.m
//  Avon
//
//  Created by Shipra Singh on 31/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "GlobalPresenceViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface GlobalPresenceViewController ()
{
  
  IBOutlet UIScrollView *scrollImage;
  IBOutlet UIImageView *imageLogo;
  IBOutlet UIImageView *middleImage;
  IBOutlet UIView *viewOverImage;
  IBOutlet UILabel *labelForBelow;
  IBOutlet UILabel *labelForAbove;
   NSMutableArray *questions;
   NSMutableArray *answers;
  AVSpeechSynthesizer *synthesizer;
  CGFloat offsetRadio;
  
  NSMutableArray *imageDetailFor4;
  NSMutableArray *imageDetailFor5;
  NSMutableArray *imageDetailFor6;
  NSMutableArray *imageDetailFor6Plus;
  NSMutableArray *imageDetailForIpad;

}
@end

@implementation GlobalPresenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  viewOverImage.hidden = YES;
  NSString* tittle = [NSString stringWithFormat:@"%d. %@",self.indexPathRow + 1,@"Global Presence"];
  [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"":@"":@""];
  
  imageDetailFor4 = [[NSMutableArray alloc]initWithObjects:@"our_mission.jpg",@"our_values.jpg", @"our_vision.jpg",@"our_mission.jpg",@"our_values.jpg", @"our_vision.jpg",@"our_mission.jpg",@"our_values.jpg", @"our_vision.jpg",@"our_vision.jpg", nil];
  imageDetailFor5 = [[NSMutableArray alloc]initWithObjects:@"our_mission_iphone5.jpg",@"our_values_iphone5.jpg", @"our_vision_iphone5.jpg", nil];
  imageDetailFor6 = [[NSMutableArray alloc]initWithObjects:@"our_mission_iphone6.jpg",@"our_values_iphone6.jpg",@"our_vision_iphone6.jpg", nil];
  imageDetailFor6Plus = [[NSMutableArray alloc]initWithObjects: @"our_mission_iphone6_plus.jpg",@"our_values_iphone6_plus.jpg",@"our_vision_iphone6_plus.jpg", nil];
  imageDetailForIpad = [[NSMutableArray alloc]initWithObjects: @"our_mission_ipad.jpg",@"our_values_ipad.jpg",@"our_vision_ipad.jpg", nil];
  [self customView];
  [self createdForImage];
[scrollImage setContentOffset:CGPointMake((self.indexPathRow * CGRectGetWidth(scrollImage.frame)) , 0) animated:YES];
  
  
  
  
  
//  questions = [[NSMutableArray alloc]initWithObjects:@"Did you know that before women had the right to vote, they were already running their own businesses as Independent Avon Representatives? ",@"Did you know that 4 Avon lipsticks are sold every second around the world?",@"Did you know that Avon is headquartered in New York, the fashion capital of the world?",@"Did you know that Avon launches more than 1,000 new products each year, many of which are award-winning products?",@"Did you know that Avon’s world-class Research & Development Center is committed to delivering the very latest in technology to our customers?",@"Did you know that Avon's manufacturing facilities are committed to operating under the highest standards?",@"Did you know that Avon Foundation for Women is the world’s largest corporate-affiliated philanthropy for causes most important to women?",@"Did you know that Avon is everywhere?",@"Did you know that many of our Representatives and Sales Leaders have a large, profitable business while maintaining flexible working hours?",@"Did you know that Avon offers many more advantages, such as free training on business development and beauty and a platform to meet and develop new people?", nil];
//
//  answers = [[NSMutableArray alloc]initWithObjects:@"Yes, Avon was founded in 1886 by David H. McConnell!",@"Yes, Avon is a true global brand with presence in more than 100 countries in every continent around the world",@"Yes, no wonder Avon is a leader in the beauty industry when our heritage comes from the fashion capital of the world!",@"Yes, Avon has a long history of cutting-edge innovations, including anti-aging skincare products, more durable mascaras and light-adjusting makeup!",@"Yes, that’s why we have teams of 350 scientists and technicians working on each and every breakthrough product we create.",@"Yes, consumer safety is Avon’s number-one priority! Avon is committed to selling safe products, using safe ingredients, and complying with applicable regulations in every country in which Avon products are sold.",@"Yes, with more than $910 million donations in more than 50 countries, Avon philanthropy focuses its funding on breast cancer research and access to care and efforts to reduce domestic and gender violence.",@"Internet, Social media and Mobile – Avon seamlessly integrates the Representative's business with the way people connect, share and shop today",@"Yes, it is a risk-free business opportunity which delivers compelling earnings, overseas trips and other recognition awards",@"Yes, with Avon you can work and have fun at the same time!", nil];
  
  
  questions = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Question1"),LocalizedString(@"Question2"),LocalizedString(@"Question3"),LocalizedString(@"Question4"),LocalizedString(@"Question5"),LocalizedString(@"Question6"),LocalizedString(@"Question7"),LocalizedString(@"Question8"),LocalizedString(@"Question9"),LocalizedString(@"Question10"), nil];
  
  answers = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"Ans1"),LocalizedString(@"Ans2"),LocalizedString(@"Ans3"),LocalizedString(@"Ans4"),LocalizedString(@"Ans5"),LocalizedString(@"Ans6"),LocalizedString(@"Ans7"),LocalizedString(@"Ans8"),LocalizedString(@"Ans9"),LocalizedString(@"Ans10"), nil];

  
  synthesizer = [[AVSpeechSynthesizer alloc]init];

 // [self customView];
 
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)customView{
  
  //customise view for inlarge Image
  
  
  scrollImage.frame = CGRectMake(0,60,kScreenWidth , kScreenHeight - 60);
  scrollImage.pagingEnabled = YES;
  scrollImage.delegate = self;
  [self.view addSubview:scrollImage];
  UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImageOfScrollView:)];
  
  [scrollImage setUserInteractionEnabled:YES];
  [scrollImage addGestureRecognizer:singleTap];
  
}




-(void)createdForImage{
  
  scrollImage.contentSize = CGSizeMake(CGRectGetWidth(scrollImage.frame) * 10, CGRectGetHeight(scrollImage.frame));
  
  NSMutableArray* imageName = [[NSMutableArray alloc]init];
  
  if (kScreenHeight == 480) {
    [imageName addObjectsFromArray:imageDetailFor4];
  }else if (kScreenHeight == 568) {
    [imageName addObjectsFromArray:imageDetailFor5];
  } else if (kScreenHeight == 667) {
    [imageName addObjectsFromArray:imageDetailFor6];
  } else if (kScreenHeight == 736) {
    [imageName addObjectsFromArray:imageDetailFor6Plus];
  } else if (IS_IPAD) {
    [imageName addObjectsFromArray:imageDetailForIpad];
  }
  
  for (UIView *v  in scrollImage.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
  }
  
  for (int i = 0 ; i<10; i++) {
    
    UIView *viewForProductImage = [[UIView alloc]init];
    viewForProductImage.frame = CGRectMake(scrollImage.frame.size.width * i,0,scrollImage.frame.size.width , scrollImage.frame.size.height);
    UIImageView *imageOffer = [[UIImageView alloc]init];
    imageOffer.frame = CGRectMake(0, 0,viewForProductImage.frame.size.width  , viewForProductImage.frame.size.height);
    
    imageOffer.contentMode = UIViewContentModeScaleToFill;
    
    imageOffer.image = [UIImage imageNamed:[imageName objectAtIndex:i]];
    viewForProductImage.backgroundColor = [UIColor whiteColor];
    imageOffer.backgroundColor = [UIColor clearColor];
    [scrollImage addSubview:viewForProductImage];
    [viewForProductImage addSubview:imageOffer];
    
    
  }
}



#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollImage) {
    
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    if (scrollViews.contentOffset.x <= -1) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(0, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else if (scrollViews.contentOffset.x >= (kScreenWidth * 9)+1 ) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(kScreenWidth * 9, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else{
      self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
    }
  }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  
  //  CGFloat pageWidth = scrollViews.contentOffset.x;
  //  int i = pageWidth/scrollViewForImage.frame.size.width;
  CGFloat pageWidth = scrollViews.contentOffset.x;
  int i = pageWidth/kScreenWidth;
  
  
   NSString* tittle = [NSString stringWithFormat:@"%d. %@",i + 1,@"Global Presence"];
  [self designHeaderOfTheScreen:@"back_icon.png" :tittle :@"":@"":@""];


  
  
}

- (void)setOffsetRadio:(CGFloat)offsetRadios
{
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    
    [scrollImage setContentOffset:CGPointMake(scrollImage.frame.size.width + scrollImage.frame.size.width * offsetRadios, 0)];
  }
}








//-(void)customView{
//  
//  int lowerHeight;
//  int buttonHeight;
//  int btnlowerHeight;
//  int btnbuttonHeight;
//  int i = 0;
//  if (IS_IPAD) {
//    lowerHeight = (55 * 500)/100;
//    buttonHeight = (20 * lowerHeight)/100;
//    btnlowerHeight = (50 * 500)/100;
//    btnbuttonHeight = (25 * btnlowerHeight)/100;
//    i = 30;
//  }else{
//    lowerHeight = (55 * kScreenWidth)/100;
//    buttonHeight = (20 * lowerHeight)/100;
//    btnlowerHeight = (50 * kScreenWidth)/100;
//    btnbuttonHeight = (25 * btnlowerHeight)/100;
//  }
//
//  //viewOverImage.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
//  
//  
// /* NSAttributedString *attributedForQ = [[NSAttributedString alloc]initWithString:[questions objectAtIndex:self.indexPathRow] attributes:@
//                                        {
//                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:18 + i/6]
//                                        }];
//  CGRect rectForQ = [attributedForQ boundingRectWithSize:(CGSize){kScreenWidth - 20 - (2*i), CGFLOAT_MAX}
//                                             options:NSStringDrawingUsesLineFragmentOrigin
//                                             context:nil];
//  CGSize sizeForQ = rectForQ.size;
//  
//  CGFloat heightsForQ = sizeForQ.height;
//  
//  
//  NSAttributedString *attributedForA = [[NSAttributedString alloc]initWithString:[answers objectAtIndex:self.indexPathRow] attributes:@
//                                        {
//                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:18 + i/6]
//                                        }];
//  CGRect rectForA = [attributedForA boundingRectWithSize:(CGSize){kScreenWidth - 20 - (2*i), CGFLOAT_MAX}
//                                             options:NSStringDrawingUsesLineFragmentOrigin
//                                             context:nil];
//  CGSize sizeForA = rectForA.size;
//  
//  CGFloat heightsForA = sizeForA.height;
//
//
//  if (heightsForQ > (viewOverImage.frame.size.height/2 - 20 - (2*i))) {
//    labelForAbove.frame = CGRectMake(10 + i, 10 + i, kScreenWidth - 20 - (2*i), viewOverImage.frame.size.height/2 - 20 - (2*i));
//  }else{
//    labelForAbove.frame = CGRectMake(10 + i, 10 + i, kScreenWidth - 20 - (2*i), heightsForQ);
//  }
//  
//  int startingOfAnswer;
//  
//  if (labelForAbove.frame.size.height != heightsForQ) {
//    startingOfAnswer =  viewOverImage.frame.size.height/2 + 10 + i;
//  }else{
//    startingOfAnswer = labelForAbove.frame.origin.y + labelForAbove.frame.size.height + 50 + i;
//  }
//  
//  if (heightsForA > (viewOverImage.frame.size.height/2 - 20 - (2*i))) {
//     labelForBelow.frame = CGRectMake(10 + i, startingOfAnswer, kScreenWidth - 20 - (2*i), viewOverImage.frame.size.height/2 - 20 - (2*i));
//  }else{
//    labelForBelow.frame = CGRectMake(10 + i, startingOfAnswer , kScreenWidth - 20 - (2*i), heightsForA);
//  }
// 
//  
////  labelForAbove.text = [questions objectAtIndex:self.indexPathRow];
////  labelForBelow.text = [answers objectAtIndex:self.indexPathRow];
//
// labelForAbove.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + i/6];
// labelForBelow.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + i/6];
//   labelForAbove.adjustsFontSizeToFitWidth = YES;
//   labelForBelow.adjustsFontSizeToFitWidth = YES; */
//  
///*  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
//                 ^{
//                   [self animateLabelShowText:[questions objectAtIndex:self.indexPathRow] characterDelay:0.02];
//                 });
// */
//
//  
////  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0),
////                 ^{
////                   [self animateLabelShowText:[answers objectAtIndex:self.indexPathRow] characterDelay:0.5];
////                 });
//
//  
//}




-(void)backButtonTap{
  [synthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
  [self.navigationController popViewControllerAnimated:YES];
}


/*- (void)animateLabelShowText:(NSString*)newText characterDelay:(NSTimeInterval)delay
{
  
  
  if ([newText isEqualToString:[questions objectAtIndex:self.indexPathRow]]) {
    [labelForAbove setText:@""];
   
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:[questions objectAtIndex:self.indexPathRow] ];
    [utterance setRate:0.1f];
    utterance.pitchMultiplier = 1.25;
    utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-au"];
    
    [synthesizer speakUtterance:utterance];
       for (int i=0; i<newText.length; i++)
    {
      dispatch_async(dispatch_get_main_queue(),
                     ^{
                       [labelForAbove setText:[NSString stringWithFormat:@"%@%C", labelForAbove.text, [newText characterAtIndex:i]]];
                  
                     });
      
      [NSThread sleepForTimeInterval:delay];
    }
    
//    double delayInSeconds = 2.0;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    
      [self animateLabelShowText:[answers objectAtIndex:self.indexPathRow] characterDelay:0.02];
   // });

        }else{
  [labelForBelow setText:@""];
         //synthesizer = [[AVSpeechSynthesizer alloc]init];
        AVSpeechUtterance *utterance1 = [AVSpeechUtterance speechUtteranceWithString:[answers objectAtIndex:self.indexPathRow] ];
          utterance1.pitchMultiplier = 1.25;
        [utterance1 setRate:0.1f];
           utterance1.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-au"];
        [synthesizer speakUtterance:utterance1];
  for (int i=0; i<newText.length; i++)
  {
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                     [labelForBelow setText:[NSString stringWithFormat:@"%@%C", labelForBelow.text, [newText characterAtIndex:i]]];
                   });
    
    [NSThread sleepForTimeInterval:delay];
  }
   
  }
}*/


@end
