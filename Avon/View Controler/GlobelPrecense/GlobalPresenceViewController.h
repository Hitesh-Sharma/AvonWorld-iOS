//
//  GlobalPresenceViewController.h
//  Avon
//
//  Created by Shipra Singh on 31/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GlobalPresenceViewController : BaseViewController
@property (assign,nonatomic)  NSInteger indexPathRow;

@end
