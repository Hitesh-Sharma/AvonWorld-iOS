//
//  AllProductsViewController.m
//  Avon
//
//  Created by Shipra Singh on 26/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "AllProductsViewController.h"
#import "ProductBO.h"
#import "UIImageView+WebCache.h"

@interface AllProductsViewController ()<UIScrollViewDelegate,ConnectionManager_Delegate>
{
  IBOutlet UIView *viewUpper;
  IBOutlet UIView *viewLower;
  IBOutlet UIView *viewForProductName;
  IBOutlet UILabel *labelDescreption;
  IBOutlet UILabel *labelProductName;
  IBOutlet UILabel *labelSelectedProducts;
  IBOutlet UIScrollView *scrollForImage;
  IBOutlet UIImageView *imageDropDown;
  IBOutlet UITableView *tableForProductListing;
  IBOutlet UILabel *sepraterLine;
  IBOutlet UIButton *buttonDropDown;
  NSMutableArray* arrayAllProduct;
  BOOL isSelectedTable;
  ConnectionManager *requestManager;
  IBOutlet UILabel *labelTotalDescreption;
  IBOutlet UIScrollView *scrollDescription;
 }
@property (nonatomic) CGFloat offsetRadio;

@end

@implementation AllProductsViewController
- (void)viewDidLoad {
    [super viewDidLoad];
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
 
  if ([countryName isEqualToString:@"India"]) {
    [self designHeaderOfTheScreen:@"menu_icon.png" :@"The Beauty Store" :@"":@"":@""];

  }else{
    [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Products") :@"":@"":@""];

  }

  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  [self customView];
  arrayAllProduct = [[NSMutableArray alloc]init];
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
   NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
  if(remoteHostStatus == NotReachable) {
    
  //  if ([countryID isEqualToString:@"1"]) {
//      NSMutableArray* data = [[NSMutableArray alloc]init];
      arrayAllProduct = [[Database getSharedInstance]getProduct:countryID withcategoryId:@""];
     
//      for (CategoriesBO* obj in data) {
//        NSMutableArray* dataProduct = [[NSMutableArray alloc]init];
//        dataProduct = [[Database getSharedInstance]getProduct:countryID withcategoryId:obj.categoryId];
//        CategoriesBO* allData = [[CategoriesBO alloc]init];
//        allData = obj;
//        allData.productArray = dataProduct;
//        [arrayAllProduct addObject:allData];
//        
//      }
      [tableForProductListing reloadData];
      [self createViewForImage];
      [self changeText];

    //}
    }else{
       [self sendDataToServerOnPagging];
    }

 
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

-(void)customView{
  
  int imageHeight = [self heightValue];
  int i = 0;
  if (IS_IPAD) {
    i = 30;
  }
  
  //int height = (kScreenHeight - 60)/2;
  int heightForProductListing = (20 *((kScreenHeight - 60)/2))/100;
 
  
  self.view.frame = CGRectMake(0, 0,kScreenWidth, kScreenHeight);
  viewUpper.frame = CGRectMake(10, 70, kScreenWidth - 20, imageHeight/2 - 20);
  scrollForImage.frame = CGRectMake(10, viewUpper.frame.origin.y + imageHeight/2 - 15 , kScreenWidth - 20, imageHeight + imageHeight/9);
  viewLower.frame = CGRectMake(10, scrollForImage.frame.origin.y + scrollForImage.frame.size.height + 5, kScreenWidth - 20, kScreenHeight - (scrollForImage.frame.origin.y + scrollForImage.frame.size.height + 15) );
  labelSelectedProducts.frame = CGRectMake(10, 5, kScreenWidth - 60, viewUpper.frame.size.height/2 - 10);
  viewForProductName.frame = CGRectMake(5, viewUpper.frame.size.height/2 - 5, kScreenWidth - 30, viewUpper.frame.size.height/2 - 5);
  
  labelDescreption.frame = CGRectMake(10, 7, kScreenWidth - 60, 25);
  scrollDescription.frame = CGRectMake(10, labelDescreption.frame.size.height + 10, kScreenWidth - 40, viewLower.frame.size.height - (labelDescreption.frame.size.height + 15) );
  labelTotalDescreption.frame = CGRectMake(0, 0,kScreenWidth - 40 , scrollDescription.frame.size.height);
  labelProductName.frame = CGRectMake(7, 2, viewForProductName.frame.size.width - ((20 *viewForProductName.frame.size.width)/100 - 8), viewForProductName.frame.size.height);
  imageDropDown.frame = CGRectMake(viewForProductName.frame.size.width - ((20 *viewForProductName.frame.size.width)/100 - 20), 0, (20 *viewForProductName.frame.size.width)/100 - 20, viewForProductName.frame.size.height);
  sepraterLine.frame = CGRectMake(imageDropDown.frame.origin.x , 5, 1, viewForProductName.frame.size.height - 10);
  tableForProductListing.frame = CGRectMake(5,viewForProductName.frame.origin.y + viewUpper.frame.size.height/2 - 8 , viewForProductName.frame.size.width, (heightForProductListing - i/4) * 6);
  buttonDropDown.frame = CGRectMake(0, 0, viewForProductName.frame.size.width, viewForProductName.frame.size.height);
  [self roundCornersOfView:viewForProductName radius:viewForProductName.frame.size.height/6 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] borderWidth:1];
  [self roundCornersOfView:viewUpper radius:4 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:viewLower radius:4 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:scrollForImage radius:4 borderColor:[UIColor clearColor] borderWidth:0];
  
  labelSelectedProducts.font = [UIFont fontWithName:kFontHelavaticaBold size:16 + i/3];
  labelProductName.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:16 + i/3];
  labelDescreption.font = [UIFont fontWithName:kFontHelavaticaBold size:16 + i/3];
  labelTotalDescreption.font = [UIFont fontWithName:kFontTimesRomanItalic size:16 + i/3];
  
  
  
  
  tableForProductListing.hidden = YES;
  [self changeRadius];
  
  tableForProductListing.layer.cornerRadius = 4;
  tableForProductListing.layer.masksToBounds = YES;
  tableForProductListing.layer.shadowColor = [[UIColor blackColor] CGColor];
  tableForProductListing.layer.shadowOffset = CGSizeMake(0.0f,0.5f);
  tableForProductListing.layer.shadowOpacity = .5f;
  tableForProductListing.layer.shadowRadius = 0.5f;
  tableForProductListing.layer.borderWidth = 1.0;
  tableForProductListing.layer.borderColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
  scrollForImage.delegate = self;
  scrollForImage.pagingEnabled = YES;
  [self.view bringSubviewToFront:viewUpper];
  labelDescreption.text = LocalizedString(@"Description");
  labelSelectedProducts.text = LocalizedString(@"Select Products");
}

//create view for large image
-(void)createViewForImage{
  
  int x = 0;
  
  if (IS_IPAD) {
     x = 30;
  }
  scrollForImage.contentSize = CGSizeMake(CGRectGetWidth(scrollForImage.frame) * ( [arrayAllProduct count] + 1),0);

  for (int i = 0 ; i <= [arrayAllProduct count]; i++) {
   
    UIView *viewBackgroundForProductDetail = [[UIView alloc]init];
    viewBackgroundForProductDetail.frame = CGRectMake(scrollForImage.frame.size.width * i,0,scrollForImage.frame.size.width , scrollForImage.frame.size.height);
    UIImageView *imageInlarge = [[UIImageView alloc]init];
    imageInlarge.frame = CGRectMake(40 + x, 40 + x,viewBackgroundForProductDetail.frame.size.width - 80 - 2*x , viewBackgroundForProductDetail.frame.size.height - 80 - 2*x);
    
      imageInlarge.contentMode = UIViewContentModeScaleAspectFit;
    imageInlarge.backgroundColor = [UIColor yellowColor];
    if (i != [arrayAllProduct count]) {
       ProductBO*obj = [arrayAllProduct objectAtIndex:i];
      [imageInlarge sd_setImageWithURL:[NSURL URLWithString:obj.itemFullImage]
                      placeholderImage:nil
                               options:SDWebImageProgressiveDownload
                              progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                              }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                             }];
 // imageInlarge.image = [UIImage imageNamed:obj.itemFullImage];
//      if (i % 2 == 0) {
//        [imageInlarge setImage:[UIImage imageNamed:@"play_icon_ipad.png"]];
//      }else{
//        [imageInlarge setImage:[UIImage imageNamed:@"map_icon_ipad.png"]];
//      }
     
    }
    viewBackgroundForProductDetail.backgroundColor = [UIColor whiteColor];
    imageInlarge.backgroundColor = [UIColor clearColor];
    [scrollForImage addSubview:viewBackgroundForProductDetail];
    [viewBackgroundForProductDetail addSubview:imageInlarge];
  }
}

#pragma mark - UITableView Life Cycle
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
  return [arrayAllProduct count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableForProductListing.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int i = 0;
   int imageHeight = [self heightValue];
  if (IS_IPAD) {
    i = 30;
  }
  return (imageHeight/4 - 5 - i/4);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int page = scrollForImage.contentOffset.x / scrollForImage.frame.size.width;
  
  ProductBO* product = [arrayAllProduct objectAtIndex:indexPath.row];
  
  
  int i = 0;
   int imageHeight = [self heightValue];
  
  if (IS_IPAD) {
    i = 30;
  }
  
  static NSString *simpleTableIdentifier = @"SimpleTableItem";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  }
  
  //remove view to avoid duplicacy
  for (UIView *v in cell.contentView.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UILabel class]]) {
      [v removeFromSuperview];
    }
  }
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.contentView.frame = CGRectMake(0, 0, tableForProductListing.frame.size.width, (imageHeight/4 - 5 - i/4) );
  
  UILabel * lableProductNameListing = [[UILabel alloc]initWithFrame:CGRectMake(7,0 , tableForProductListing.frame.size.width - 14, (imageHeight/4 - 5 - i/4))];
  UILabel * lableLine = [[UILabel alloc]initWithFrame:CGRectMake(0,(imageHeight/4 - 5 - i/4)-1 ,tableForProductListing.frame.size.width  ,1 )];
  
  lableLine.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
  lableProductNameListing.text =  product.itemName;
   lableProductNameListing.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:15 + i/3];
  lableProductNameListing.numberOfLines = 2;
  
  if (indexPath.row == page) {
    cell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
  }else{
    cell.contentView.backgroundColor = [UIColor whiteColor];
     }
  
  
  [cell.contentView addSubview:lableProductNameListing];
  [cell.contentView addSubview:lableLine];
  
  
  return cell;
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];

  int imageHeight = [self heightValue];
  UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                              indexPath];
  oldCell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];

  [scrollForImage setContentOffset:CGPointMake((indexPath.row * CGRectGetWidth(scrollForImage.frame)) , 0) animated:YES];
  viewUpper.frame = CGRectMake(10, 70, kScreenWidth - 20, imageHeight/2 - 20);
  tableForProductListing.hidden = YES;
  isSelectedTable = NO;
 
  ProductBO* product = [arrayAllProduct objectAtIndex:indexPath.row];
  NSString* currency = LocalizedString(@"Currency");
  if ([countryName isEqualToString:@"Philippines"]){
    
    currency = @"₱ ";
  }
  [self textLabelHeight:[[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:currency]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:@"/-" ]];


  if ([countryName isEqualToString:@"Taiwan"]){
   labelTotalDescreption.text = [[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:LocalizedString(@"Currency")]stringByAppendingString:@"/-" ];
    
    
  }else{
    NSString* currency = LocalizedString(@"Currency");
    if ([countryName isEqualToString:@"Philippines"]){
      
      currency = @"₱ ";
    }

  labelTotalDescreption.text = [[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:currency]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:@"/-" ];
  }
  labelProductName.text = product.itemName;
  
  [self startSpin];
  [self changeRadius];
  
}



#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
 }


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  
  if (scrollViews.tag == 1) {
    CGFloat currentOffset = scrollViews.contentOffset.y;
    CGFloat maximumOffset = scrollViews.contentSize.height - scrollViews.frame.size.height;
    if (maximumOffset - currentOffset <= 10.0) {
      [self performSelector:@selector(sendDataToServerOnPagging) withObject:nil afterDelay:1];
    }
  }
  
   int i = scrollForImage.contentOffset.x/scrollForImage.frame.size.width;
 

  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
  if(i == [arrayAllProduct count]){
     if(remoteHostStatus == NotReachable) {
    [scrollForImage setContentOffset:CGPointMake((([arrayAllProduct count]-1) * CGRectGetWidth(scrollForImage.frame)) , 0) animated:YES];
     }else{
    [self sendDataToServerOnPagging];
     }
    
    }else{
      [self changeText];
  }
}


- (void)setOffsetRadio:(CGFloat)offsetRadio
{
 
  if (_offsetRadio != offsetRadio)
  {
    _offsetRadio = offsetRadio;
    
    [scrollForImage setContentOffset:CGPointMake(scrollForImage.frame.size.width + scrollForImage.frame.size.width * offsetRadio, 0)];
  }
}


-(void)changeText{
  int page = scrollForImage.contentOffset.x / scrollForImage.frame.size.width;
  if ([arrayAllProduct count] == page) {
      [scrollForImage setContentOffset:CGPointMake((([arrayAllProduct count]-1) * CGRectGetWidth(scrollForImage.frame)) , 0) animated:YES];
    return;
  }
  ProductBO* product = [arrayAllProduct objectAtIndex:page];
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  NSString* currency = LocalizedString(@"Currency");
  if ([countryName isEqualToString:@"Philippines"]){
    currency = @"₱ ";
  }

  [self textLabelHeight:[[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:currency]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:@"/-" ]];
  if ([countryName isEqualToString:@"Taiwan"]){
    labelTotalDescreption.text = [[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:LocalizedString(@"Currency")]stringByAppendingString:@"/-" ];

    
  }else{
  labelTotalDescreption.text = [[[[[product.itemDescription stringByAppendingString:@"\n"]stringByAppendingString:product.itemWeight]stringByAppendingString:currency]stringByAppendingString:product.itemAvaragePrice]stringByAppendingString:@"/-" ];
  }
  labelProductName.text = product.itemName;

}


- (IBAction)buttonDropDownClick:(id)sender {
 
  int imageHeight = [self heightValue];
  
   if (isSelectedTable == NO) {
    isSelectedTable = YES;
      [self startSpin];
      [tableForProductListing reloadData];
     [self roundCornersOfView:viewForProductName radius:0  borderColor:[UIColor clearColor] borderWidth:0];
     UIBezierPath *maskPath;
     maskPath = [UIBezierPath bezierPathWithRoundedRect:viewForProductName.bounds
                                      byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                            cornerRadii:CGSizeMake(viewForProductName.frame.size.height/6, viewForProductName.frame.size.height/6)];
     
     CAShapeLayer* shape = [[CAShapeLayer alloc] init];
     [shape setPath:maskPath.CGPath];
     viewForProductName.layer.mask = shape;
     CAShapeLayer*   frameLayer = [CAShapeLayer layer];
     frameLayer.frame =viewForProductName.bounds;
     frameLayer.path = maskPath.CGPath;
     frameLayer.borderWidth = 1.0f;
     frameLayer.borderColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
     frameLayer.strokeColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
     frameLayer.fillColor = nil;
     
     [viewForProductName.layer addSublayer:frameLayer];
     viewUpper.frame = CGRectMake(10, 70, kScreenWidth - 20, viewUpper.frame.size.height + tableForProductListing.frame.size.height - 3);
     tableForProductListing.hidden = NO;

    
  }else{
    isSelectedTable = NO;
    [self startSpin];
    viewUpper.frame = CGRectMake(10, 70, kScreenWidth - 20, imageHeight/2 - 20);
    tableForProductListing.hidden = YES;
    [self changeRadius];
  }
}

-(void)changeRadius{
  
  UIBezierPath *maskPath;
  maskPath = [UIBezierPath bezierPathWithRoundedRect:viewForProductName.bounds
                                   byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                         cornerRadii:CGSizeMake(0, 0)];
  
  CAShapeLayer* shape = [[CAShapeLayer alloc] init];
  [shape setPath:maskPath.CGPath];
  viewForProductName.layer.mask = shape;
  
  
  [self roundCornersOfView:viewForProductName radius:viewForProductName.frame.size.height/6  borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] borderWidth:1];
  //viewForProductName.layer.shouldRasterize = YES;
  [[viewForProductName layer] setRasterizationScale:[[UIScreen mainScreen] scale]];
  [viewForProductName.layer setShadowRadius:1.0f];
  viewForProductName.layer.shadowColor = [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] CGColor];
  viewForProductName.layer.shadowOffset = CGSizeMake(1, 1);
  viewForProductName.layer.shadowOpacity = 1.0f;
  viewForProductName.layer.shadowRadius = 0.0f;
}

- (void) spinWithOptions: (UIViewAnimationOptions) options {
  // this spin completes 360 degrees every 2 seconds
  [UIView animateWithDuration: 0.2f
                        delay: 0.0f
                      options: options
                   animations: ^{
                     imageDropDown.transform = CGAffineTransformRotate(imageDropDown.transform, M_PI/2  );
                   }
                   completion: ^(BOOL finished) {
                     if (finished) {
                       
                       if (options != UIViewAnimationOptionCurveEaseOut) {
                         
                         [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                         
                       }
                     }
                   }];
}


- (void) startSpin {
  [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
}


-(int)heightValue{
  int heights;
  int imageHeight;
  int i = 0;
  
  if (IS_IPAD) {
    heights = (70 * 780/100);
    imageHeight = (70 * heights)/100;
    i = 30;
  }else{
    heights = (80 * kScreenWidth/100);
    imageHeight = (80 * heights)/100;
  }

  return imageHeight;
}


-(void)textLabelHeight:(NSString*)text{
  int i = 0;
  if (IS_IPAD) {
    i = 30;
  }
  NSAttributedString *attributedForQ = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontTimesRomanItalic size:16 + i/3]}];
  CGRect rectForHedding = [attributedForQ boundingRectWithSize:(CGSize){kScreenWidth - 40, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin context:nil];
  CGSize sizeForHedding = rectForHedding.size;
  
  CGFloat heightsForHedding = sizeForHedding.height;
  labelTotalDescreption.frame = CGRectMake(0, 0,kScreenWidth - 40 , heightsForHedding + 3);
  scrollDescription.contentSize = CGSizeMake(0,CGRectGetHeight(labelTotalDescreption.frame));

  }




-(void)sendDataToServerOnPagging{
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
    ALERT(@"", LocalizedString(@"No Internet Connection"));
    
  }else{
    
  [self sendDataToServerWithTag:TASK_GET_ALLPRODUCT];
  }
}


-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_GET_ALLPRODUCT:
    {
      
       [self remove_Loader];
      [self show_Loader];
  NSString* countryId = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      
      [dictRoot setObject:countryId forKey:kCountryId];
      [dictRoot setObject:@"10" forKey:kLimit];
      [dictRoot setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arrayAllProduct count]] forKey:kOffset];
      [dictRoot setObject:@"asc" forKey:@"order"];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_ALLPRODUCT withURL:kUrlForAllProducts with:@"POST"];
      break;
    }
       default:
      break;
  }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  
  int heightForProductListing = (20 *((kScreenHeight - 60)/2))/100;
  int i = 0;
  if (IS_IPAD) {
    i = 30;
  }
  
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      
      case TASK_GET_ALLPRODUCT:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllProducts = [[NSMutableArray alloc]init];
          arrayAllProducts = [recievedDictionary objectForKey:kResponse];
          if ([arrayAllProducts count] > 0) {
          for (NSDictionary *obj in arrayAllProducts) {
            ProductBO* product = [[ProductBO alloc]initWithResponse:obj];
            int counting = 0;
            for (ProductBO *pro in arrayAllProduct) {
              if ([pro.itemName isEqualToString:product.itemName]) {
                counting++;
              }
            }
            if (counting == 0) {
               [arrayAllProduct addObject:product];
            }
           
          }
            [tableForProductListing reloadData];
            [self createViewForImage];
            [self changeText];
          }else {
    [scrollForImage setContentOffset:CGPointMake((([arrayAllProduct count]-1) * CGRectGetWidth(scrollForImage.frame)) , 0) animated:YES];
           
          }
          //[self customView];
     
         
        }else{
          ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        break;
      }
        
      default:
        break;
    }
  }
  else
  {
    ALERT(nil, @"");
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  NSString *response = [request responseString];
  [self remove_Loader];
}




@end
