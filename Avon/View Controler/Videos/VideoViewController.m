//
//  VideoViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "VideoViewController.h"
#import "VideoTableViewCell.h"
#import "UIScrollView+AH3DPullRefresh.h"
#import <MediaPlayer/MediaPlayer.h>
#import "VideoBO.h"
#import "UIImageView+WebCache.h"


@interface VideoViewController ()<ConnectionManager_Delegate>
{
  
  IBOutlet UITableView *tableVideo;
  NSMutableArray *arrayOfVideo;
   ConnectionManager *requestManager;
}
@end

@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Videos") :@"":@"":@""];
    // Do any additional setup after loading the view from its nib.
  tableVideo.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
  tableVideo.separatorColor = [UIColor clearColor];
  tableVideo.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
  arrayOfVideo = [[NSMutableArray alloc]init];
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
    ALERT(@"", LocalizedString(@"Video Connection"));
  }else{

  [self sendDataToServerWithTag:TASK_VIDEOALL];
  }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}


#pragma mark - UITableView Life Cycle
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
  return [arrayOfVideo count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableVideo.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//  VideoBO *objectAtIndex=[arrSearchVideoNews objectAtIndex:indexPath.row];
//  return [self getHeightOfLable:objectAtIndex.headline with:menuTextFont].height+210;
  int height = (41 * kScreenHeight)/100;
  return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   int height = (41 * kScreenHeight)/100;
  int seprate = 0;
  if (IS_IPAD) {
    seprate = 5;
  }
  NSString *simpleTableIdentifier = @"VideoTableViewCell";
  VideoTableViewCell *cell = (VideoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  NSArray *  nib = [[NSBundle mainBundle] loadNibNamed:@"VideoTableViewCell" owner:self options:nil];;
  cell = [[VideoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  cell = [nib objectAtIndex:0];
  
  VideoBO* obj = [arrayOfVideo objectAtIndex:indexPath.row];
  
 
//  float height =[self getHeightOfLable:objectAtIndex.headline with:menuTextFont].height+5;
  cell.contentView.frame = CGRectMake(0, 0, kScreenWidth, height);
   [cell.viewForMainBG setFrame:CGRectMake(5 + seprate, 5 + seprate, kScreenWidth-10 - 2*seprate, height - 5 - seprate)];
  [cell.imgvVideoThumb setFrame:CGRectMake(0, 0, cell.viewForMainBG.frame.size.width, cell.viewForMainBG.frame.size.height)];
  [cell.btnPlay setFrame:CGRectMake((kScreenWidth-59)/2, (height-55)/2, 59, 58)];
  [cell.btnPlay setTag:indexPath.row];
  [cell.btnPlay addTarget:self action:@selector(btnPlay:) forControlEvents:UIControlEventTouchUpInside];
  [cell.lblVideoDuration setFrame:CGRectMake(cell.imgvVideoThumb.frame.size.width-70, 13, 60, 21)];
  
  [cell.viewBG setFrame:CGRectMake(0, height - ((20 * height)/100) -5, kScreenWidth, (20 * height)/100)];
  //[cell.viewBG setBackgroundColor:[UIColor whiteColor]];
  [cell.lblVideoName setFrame:CGRectMake(15, 0, kScreenWidth-30, cell.viewBG.frame.size.height )];
  cell.lblVideoName.font = [UIFont fontWithName:kFontHelvaticaLight size:18];
  [self roundCornersOfView:cell.lblVideoDuration radius:2 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:cell.viewForMainBG radius:0 borderColor:[UIColor clearColor] borderWidth:0];
  [cell.lblVideoDuration setText:obj.video_time];
  [cell.lblVideoName setText:obj.video_name];
  [cell.imgvVideoThumb sd_setImageWithURL:[NSURL URLWithString:obj.video_thumb]
               placeholderImage:nil
                        options:SDWebImageProgressiveDownload
                       progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                       }
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                      }];
  return cell;
  
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];

  [self playTheVideo:indexPath.row];
}


-(void)btnPlay:(id)sender{
  [self playTheVideo:[sender tag]];
  
}


-(void)playTheVideo:(int)index{
 
  VideoBO *objectAtIndex=[arrayOfVideo objectAtIndex:index];
  
  NSURL *movieURL = [NSURL URLWithString:objectAtIndex.video_url];

  MPMoviePlayerViewController*  objTemp = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
  
  
  UIColor *transBgColor = [UIColor colorWithRed:30.0/255.2f green:155.0/255.2f blue:241.0/255.2f alpha:1];
  UIColor *black = [UIColor blackColor];
  CAGradientLayer *maskLayer = [CAGradientLayer layer];
  maskLayer.opacity = 1;
  maskLayer.colors = [NSArray arrayWithObjects:(id)black.CGColor,
                      (id)transBgColor.CGColor, (id)transBgColor.CGColor, (id)black.CGColor, nil];
  
  // Hoizontal - commenting these two lines will make the gradient veritcal
  maskLayer.startPoint = CGPointMake(0.0, 0.5);
  maskLayer.endPoint = CGPointMake(1.0, 0.5);
  
  NSNumber *gradTopStart = [NSNumber numberWithFloat:0.3];
  NSNumber *gradTopEnd = [NSNumber numberWithFloat:0.7];
  
  maskLayer.locations = @[gradTopStart, gradTopEnd];
  
  maskLayer.bounds = objTemp.navigationController.navigationBar.bounds;
  maskLayer.anchorPoint = CGPointZero;
  [objTemp.navigationController.navigationBar.layer addSublayer:maskLayer];
  
  //    objTemp.navigationController.navigationBar.tintColor=;
  [self presentMoviePlayerViewControllerAnimated:objTemp];
  [objTemp.moviePlayer play];
  
}



-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_VIDEOALL:
    {
       [self remove_Loader];
      [self show_Loader];
       NSString* countryId = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
       [dictRoot setObject:countryId forKey:kCountryId];
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_VIDEOALL withURL:kUrlForVideo with:@"POST"];
      break;
    }
      
    default:
      break;
  }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_VIDEOALL:
      {
         if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
        NSArray *arrayOfTemp= [recievedDictionary objectForKey:kResponse];
        if ([arrayOfTemp count]!=0) {
          for (NSDictionary *obj in arrayOfTemp) {
            
            VideoBO* video = [[VideoBO alloc]initWithResponse:obj];
            [arrayOfVideo addObject:video];
            
          }
          [tableVideo reloadData];
        }
         }else  {
           
            ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
         }
        break;
      }
        
      default:
        break;
    }
  }
  else
  {
    ALERT(nil, @"");
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  
}






@end
