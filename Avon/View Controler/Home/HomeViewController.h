//
//  HomeViewController.h
//  Avon
//
//  Created by Shipra Singh on 27/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <Foundation/Foundation.h>


@interface HomeViewController : BaseViewController

@end
