//
//  HomeViewController.m
//  Avon
//
//  Created by Shipra Singh on 27/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "ProductViewController.h"
#import "SliderDemo.h"
#import "CountryBO.h"
#import "LeftSlideViewController.h"

@interface HomeViewController ()<SlideNavigationControllerDelegate,UIScrollViewDelegate,ConnectionManager_Delegate>

{
  IBOutlet UIView *viewForCountryTable;
  IBOutlet UIView *viewWhiteForCountrySelection;
  IBOutlet UIButton *buttonNextForCountrySelection;
  IBOutlet UILabel *labelYourLocationForCountry;
  IBOutlet UIImageView *imageForLocationView;
  IBOutlet UIImageView *imageBackgrountForHome;
  IBOutlet UIView *viewForLocationSelector;
  IBOutlet UIView *viewLowerTransparent;
  IBOutlet UIView *viewWhiteForLocationSelector;
  IBOutlet UIView *transparentViewForLocationView;
  IBOutlet UILabel *labelText;
  IBOutlet UILabel *labelYourLocation;
  IBOutlet UIButton *buttonStartSaving;
  IBOutlet UIButton *buttonNext;
  IBOutlet UIView *viewForCountrySelection;
  IBOutlet UITableView *tableForCountrySelection;
  IBOutlet UILabel *labelEastMalaysia;
  IBOutlet UILabel *labelWestMalaysia;
  IBOutlet UIImageView *imageLogo;
  IBOutlet UILabel *sepraterLine;
  IBOutlet UIButton *buttonDropDown;
  IBOutlet UIView *viewForCountry;
  IBOutlet UILabel *labelCountryName;
  IBOutlet UIImageView *imageDropDown;
  ConnectionManager *requestManager;
  NSMutableArray *arryaForCountry;
  NSMutableArray *arrayShortedCountry;
}


@end

@implementation HomeViewController


-(void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
  buttonStartSaving.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:0.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
}

- (void)viewDidLoad {
  [super viewDidLoad];
 
  
  //connection manager
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  arryaForCountry = [[NSMutableArray alloc]init];
  tableForCountrySelection.hidden = YES;
 
  [self customView];
  [self countryLanguageSelection];
  [self textAccordingToLocalization];
  
  //Observer for Text Color Change
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveChangeNotificationMessage:) name:@"ChangeColorWest" object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveChangeNotificationMessage:) name:@"ChangeColorEast" object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDeviceTocken:) name:@"CallWebServiceForPush" object:nil];
  NSLog(@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:kIsLocationSelected]);
  //first use of app call for location access
  if ([[NSUserDefaults standardUserDefaults]integerForKey:kIsLocationSelected] == 0) {
    [self sendDataToServerWithTag:TASK_COUNTRY_LIST];
    [self.view addSubview:viewForCountrySelection];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^{
                       imageLogo.center = CGPointMake(imageLogo.center.x, imageLogo.center.y - 100);
                     }completion:nil];
    
    //animation for popup
    CGAffineTransform baseTransform = viewWhiteForCountrySelection.transform;
    viewWhiteForCountrySelection.transform = CGAffineTransformTranslate(baseTransform,0,viewWhiteForCountrySelection.bounds.size.height);
    [UIView animateWithDuration: 0.5 animations:^{
      viewWhiteForCountrySelection.transform = baseTransform;
    }];
  }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Customise View

-(void)customView{
  
  int lowerHeight;
  int buttonHeight;
  int btnlowerHeight;
  int btnbuttonHeight;
  int i = 0;
  
  if (IS_IPAD) {
     lowerHeight = (55 * 500)/100;
     buttonHeight = (20 * lowerHeight)/100;
    btnlowerHeight = (50 * 500)/100;
    btnbuttonHeight = (25 * btnlowerHeight)/100;
    i = 30;
     imageDropDown.image = [UIImage imageNamed:@"lc_icon@3x.png"];
   
  }else{
     lowerHeight = (55 * kScreenWidth)/100;
     buttonHeight = (20 * lowerHeight)/100;
    btnlowerHeight = (50 * kScreenWidth)/100;
    btnbuttonHeight = (25 * btnlowerHeight)/100;
    imageDropDown.image = [UIImage imageNamed:@"lc_icon.png"];

  }
 
  
  imageBackgrountForHome.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
  buttonStartSaving.frame = CGRectMake(25 + (4*i), kScreenHeight - btnbuttonHeight - 13 - i/6, kScreenWidth - 50 - (8*i), btnbuttonHeight);
  viewLowerTransparent.frame = CGRectMake(0,kScreenHeight - (lowerHeight - lowerHeight/5), kScreenWidth, (lowerHeight - lowerHeight/5));
  labelText.frame = CGRectMake(10 + i, 10, kScreenWidth - 20 - (2*i), viewLowerTransparent.frame.size.height/2);
  viewForLocationSelector.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
  transparentViewForLocationView.frame = CGRectMake(0, 0, kScreenWidth, viewForLocationSelector.frame.size.height);
  viewWhiteForLocationSelector.frame = CGRectMake(0,kScreenHeight - lowerHeight , kScreenWidth, lowerHeight);
  imageForLocationView.frame = CGRectMake(0, 90, kScreenWidth, kScreenHeight - 60);
  buttonNext.frame = CGRectMake(25 +( 4 * i) , viewWhiteForLocationSelector.frame.size.height - btnbuttonHeight - 23 - i/6, kScreenWidth - 50 - 8 * i, btnbuttonHeight);
  labelYourLocation.frame = CGRectMake(40, 17 + i/3, kScreenWidth - 80, 27);
  labelWestMalaysia.frame = CGRectMake(30 + ( 4 * i) + i/2,lowerHeight/3 + 12 + buttonHeight/5 + i/3, kScreenWidth/2 - 30, 27);
  labelEastMalaysia.frame = CGRectMake(kScreenWidth - (kScreenWidth/2 - 45)-30 - ( 4 * i) - + i/2 , lowerHeight/3 + 12 + buttonHeight/5 + i/3 , kScreenWidth/2 - 50, 27);

  viewForCountrySelection.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
  viewForCountryTable.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
  
  //slider view for malysia
  UIView *VctrDtl=[[UIView alloc]initWithFrame:CGRectMake(65 + ( 4 * i) + i/2,lowerHeight/5 , kScreenWidth - 130 - 8*i - i, buttonHeight/5 + i/6 + 50)];
  SliderDemo *sliderVctr=[[SliderDemo alloc] initWithFrame:CGRectMake(VctrDtl.bounds.origin.x,VctrDtl.bounds.origin.y,VctrDtl.bounds.size.width, VctrDtl.bounds.size.height)];
  sliderVctr.minimumValue =0;
  sliderVctr.selectedMinimumValue = 0;
  sliderVctr.maximumValue = 9;
  sliderVctr.selectedMaximumValue = 9;
  sliderVctr.minimumRange = 00;
  [VctrDtl addSubview:sliderVctr];
  [viewWhiteForLocationSelector addSubview:VctrDtl];

  viewWhiteForCountrySelection.frame = CGRectMake(0,kScreenHeight - (lowerHeight - lowerHeight/7), kScreenWidth, (lowerHeight - lowerHeight/7));
  labelYourLocationForCountry.frame = CGRectMake(40, 17 + i/3, kScreenWidth - 80, 27);
  buttonNextForCountrySelection.frame = CGRectMake(25 +( 4 * i) , viewWhiteForCountrySelection.frame.size.height - btnbuttonHeight - 23 - i/6, kScreenWidth - 50 - 8 * i, btnbuttonHeight);
  viewForCountry.frame = CGRectMake(25 +( 4 * i) , viewWhiteForCountrySelection.frame.size.height/3 , kScreenWidth - 50 - 8 * i, btnbuttonHeight - btnbuttonHeight/6);
  labelCountryName.frame = CGRectMake(10, 0, viewForCountry.frame.size.width - ((20 *viewForCountry.frame.size.width)/100 - 10), viewForCountry.frame.size.height);
  imageDropDown.frame = CGRectMake(viewForCountry.frame.size.width - ((20 *viewForCountry.frame.size.width)/100 - 20), 0, (20 *viewForCountry.frame.size.width)/100 - 20, viewForCountry.frame.size.height);
  sepraterLine.frame = CGRectMake(imageDropDown.frame.origin.x , 5, 1, viewForCountry.frame.size.height - 10);
   imageLogo.frame = CGRectMake(40, kScreenHeight/2 - (lowerHeight - lowerHeight/7)/2, kScreenWidth - 80, (lowerHeight - lowerHeight/7));
  buttonDropDown.frame = CGRectMake(0, 0, viewForCountry.frame.size.width, viewForCountry.frame.size.height);
  
  //customize for color font and border
  
  [self roundCornersOfView:buttonStartSaving radius:btnbuttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:buttonNext radius:btnbuttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:buttonNextForCountrySelection radius:btnbuttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  [self roundCornersOfView:viewForCountry radius:viewForCountry.frame.size.height/6 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] borderWidth:1];
   [self roundCornersOfView:tableForCountrySelection radius:4 borderColor:[UIColor clearColor] borderWidth:0];
  
  //custom Font size
  labelEastMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + i/6];
  labelYourLocation.font = [UIFont fontWithName:kFontHelvaticaLight size:20 + i/5];
  labelYourLocationForCountry.font = [UIFont fontWithName:kFontHelvaticaLight size:20 + i/5];
  labelWestMalaysia.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + i/6];
  labelText.font = [UIFont fontWithName:kFontHelvaticaLight size:18 + i/6];
  labelCountryName.font = [UIFont fontWithName:kFontTimesRomanItalic size:15 + i/6];
  buttonNext.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
  buttonStartSaving.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
  buttonNextForCountrySelection.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
  
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];

  if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
       [self imageDispalayOnHome:@"Malaysia"];
  }else  if ([countryName isEqualToString:@"Taiwan"]) {
    [self imageDispalayOnHome:@"Taiwan"];
      }else if ([countryName isEqualToString:@"India"]){
      [self imageDispalayOnHome:@"India"];
      
      }else if ([countryName isEqualToString:@"Philippines"]){
        [self imageDispalayOnHome:@"Philippines"];
        
      }

  
  
   labelText.textColor = [UIColor whiteColor];
  [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsEastMalaysia];
  [[NSUserDefaults standardUserDefaults]synchronize];
}


-(void)imageDispalayOnHome:(NSString*)countryName{
 
  NSString* imageName;
  if ([countryName isEqualToString:@"Taiwan"])
  {
    if (kScreenHeight == 568) {
      imageName = @"tw_img_iphone5.png";
    } else if (kScreenHeight == 667) {
      imageName = @"tw_img_iphone6.png";
    } else if (kScreenHeight == 736) {
      imageName = @"tw_img_iphone6_plus.png";
    } else if (IS_IPAD) {
      imageName = @"tw_img_ipad.png";
    }else{
      imageName = @"tw_img_iphone.png";
    }
  }
  else if ([countryName isEqualToString:@"India"])
  {
    if (kScreenHeight == 568) {
      imageName = @"india_img_iphone5.png";
    } else if (kScreenHeight == 667) {
      imageName = @"india_img_iphone6.png";
    } else if (kScreenHeight == 736) {
      imageName = @"india_img_iphone6_plus.png";
    } else if (IS_IPAD) {
      imageName = @"india_img_ipad.png";
    }else{
      imageName = @"india_img_iphone4.png";
    }

  }
  else if ([countryName isEqualToString:@"Philippines"])
  {
    if (kScreenHeight == 568) {
      imageName = @"ph_img_iphone5.png";
    } else if (kScreenHeight == 667) {
      imageName = @"ph_img_iphone6.png";
    } else if (kScreenHeight == 736) {
      imageName = @"ph_img_iphone6_plus.png";
    } else if (IS_IPAD) {
      imageName = @"ph_img_ipad.png";
    }else{
      imageName = @"ph_img_iphone.png";
    }

  }
  else if ([countryName isEqualToString:@"Malaysia"])
  {
    if (kScreenHeight == 568) {
      imageName = @"home_banner_iphone5.jpg";
    } else if (kScreenHeight == 667) {
      imageName = @"home_banner_iphone6.jpg";
    } else if (kScreenHeight == 736) {
      imageName = @"home_banner_iphone6_plus.jpg";
    } else if (IS_IPAD) {
      imageName = @"home_banner_ipad.jpg";
    }else{
      imageName = @"home_banner.jpg";
    }

  }
  
  imageBackgrountForHome.image = [UIImage imageNamed:imageName];
  imageForLocationView.image = [UIImage imageNamed:imageName];
}

-(void)textAccordingToLocalization{
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Home") :@"location_icon.png":@"":@""];
  
  [buttonNext setTitle:LocalizedString(@"Next") forState:UIControlStateNormal];
  [buttonStartSaving setTitle:LocalizedString(@"Start Saving") forState:UIControlStateNormal];

  labelText.text = LocalizedString(@"FirstScreenText");
  labelEastMalaysia.text = LocalizedString(@"East Malaysia");
  labelWestMalaysia.text = LocalizedString(@"West Malaysia");
  labelYourLocation.text = LocalizedString(@"Your Location");

}

-(void)countryLanguageSelection{
 
  labelText.textAlignment = NSTextAlignmentLeft;
  if ([labelCountryName.text isEqualToString:@"Malaysia"]) {
    LocalizationSetLanguage(@"ms-MY");
    [self imageDispalayOnHome:@"Malaysia"];
  }else  if ([labelCountryName.text isEqualToString:@"Taiwan"]) {
    [self imageDispalayOnHome:@"Taiwan"];
    labelText.textAlignment = NSTextAlignmentCenter;
  //  LocalizationSetLanguage(@"en");
      LocalizationSetLanguage(@"zh-Hans");
  }else if ([labelCountryName.text isEqualToString:@"India"] || [labelCountryName.text isEqualToString:@"Philippines"]){
    if ([labelCountryName.text isEqualToString:@"India"]){
      [self imageDispalayOnHome:@"India"];
    }else if ([labelCountryName.text isEqualToString:@"Philippines"]){
      [self imageDispalayOnHome:@"Philippines"];
      }
       LocalizationSetLanguage(@"en");
  }else if ([labelCountryName.text isEqualToString:@"Select Your Location"]){
    NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
    
    if ( [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
      countryName =@"Malaysia";
    }
    if ([countryName isEqualToString:@"Malaysia"]) {
      LocalizationSetLanguage(@"ms-MY");
      [self imageDispalayOnHome:@"Malaysia"];
    }else  if ([countryName isEqualToString:@"Taiwan"]) {
      labelText.textAlignment = NSTextAlignmentCenter;
      [self imageDispalayOnHome:@"Taiwan"];
      //  LocalizationSetLanguage(@"en");
      LocalizationSetLanguage(@"zh-Hans");
    }else{
      if ([countryName isEqualToString:@"India"]){
        [self imageDispalayOnHome:@"India"];
      }else if ([countryName isEqualToString:@"Philippines"]){
        [self imageDispalayOnHome:@"Philippines"];
      }
      LocalizationSetLanguage(@"en");
    }
  }
  
}

/*It takes back to menu screen*/

-(void)rightButtonTap{
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
  if ( [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
    countryName =@"Malaysia";
  }
  labelCountryName.text = countryName;
  imageLogo.hidden = YES;
  viewForCountrySelection.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5f];

  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
  
//    NSMutableArray* arrayAllCountry = [[NSMutableArray alloc]init];
    if ([arryaForCountry count] > 0) {
      [arryaForCountry removeAllObjects];
    }
    arryaForCountry = [[Database getSharedInstance]getCountries];
    
    
    int cellHeight = 40;
    int i = 0;
    if (IS_IPAD) {
      cellHeight = 60;
      i = 30;
    }
    [self shortCountryList];
    if (kScreenHeight > ([arryaForCountry count]*cellHeight)) {
      tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), (kScreenHeight)/2 - ( [arrayShortedCountry count]*cellHeight)/2, kScreenWidth - 50 - 8 * i,( [arrayShortedCountry count]*cellHeight));
    }else{
      tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), 0, kScreenWidth - 50 - 8 * i,kScreenHeight);
    }
    
    //tableForCountrySelection.hidden = NO;
    [tableForCountrySelection reloadData];
  
  }
  else {
     [self sendDataToServerWithTag:TASK_COUNTRY_LIST];
  }

//  [[Database getSharedInstance]storeMessage:obj];
 
  [self.view addSubview:viewForCountrySelection];
  [[NSUserDefaults standardUserDefaults]synchronize];
  
  //animation for popup
  CGAffineTransform baseTransform = viewWhiteForCountrySelection.transform;
  viewWhiteForCountrySelection.transform = CGAffineTransformTranslate(baseTransform,0,viewWhiteForCountrySelection.bounds.size.height);
  [UIView animateWithDuration: 0.5 animations:^{
    viewWhiteForCountrySelection.transform = baseTransform;
  }];

  }

-(void)backButtonTap{
 //  LeftSlideViewController* obj = [[LeftSlideViewController alloc]init];
//  [obj arryaAccordingToCountry];
  slideNavController = [SlideNavigationController sharedInstance];
  [slideNavController toggleLeftMenu];
}
- (IBAction)buttonCountryListClick:(id)sender {

  int cellHeight = 40;
  int i = 0;
  if (IS_IPAD) {
    cellHeight = 60;
    i = 30;
  }
 
  [self.view addSubview:viewForCountryTable];
 
  if (kScreenHeight > ([arryaForCountry count]*cellHeight)) {
    tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), (kScreenHeight)/2 - ( [arrayShortedCountry count]*cellHeight)/2, kScreenWidth - 50 - 8 * i,( [arrayShortedCountry count]*cellHeight));
  }else{
    tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), 0, kScreenWidth - 50 - 8 * i,kScreenHeight);
  }

  tableForCountrySelection.hidden = NO;
  
}

- (IBAction)buttonNextAfterCountrySelection:(id)sender {
  
  if (![labelCountryName.text isEqualToString:@"Select Your Location"]) {
     [self imageDispalayOnHome:labelCountryName.text];
    if ([labelCountryName.text isEqualToString:@"Malaysia"]) {
           }else{
      for (CountryBO* obj in arryaForCountry) {
        if ([obj.countryName isEqualToString:labelCountryName.text]) {
          [[NSUserDefaults standardUserDefaults]setObject:obj.countryName forKey:kCountrySelectionName];
          [[NSUserDefaults standardUserDefaults]setObject:obj.Ids forKey:kCountrySelectionId];
          [[NSUserDefaults standardUserDefaults] synchronize];
          //       if ([[NSUserDefaults standardUserDefaults]integerForKey:kIsLocationSelected] == 0) {
          [self sendDataToServerWithTag:TASK_PUSH_REGISTRATION];
          // }
        }
      }
    }

    [self countryLanguageSelection];
    [self textAccordingToLocalization];
    [viewForCountrySelection removeFromSuperview];
    if ([labelCountryName.text isEqualToString:@"Malaysia"]) {
      [self countryMalaysiaSelected];

    }else{
      [app setUser];
    }
   
  }
  
}

- (IBAction)saveButton:(id)sender {
  
  [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsClickOnHome];
  [[NSUserDefaults standardUserDefaults]synchronize];
 
  ProductViewController *obj = [[ProductViewController alloc]initWithNibName:@"ProductViewController" bundle:nil];
  [self.navigationController pushViewController:obj animated:NO];
  
}
- (IBAction)saveTouch:(id)sender {
  buttonStartSaving.titleLabel.textColor = [UIColor whiteColor];
  buttonStartSaving.backgroundColor = [UIColor colorWithRed:212.0f/255.0f green:0.0f/255.0f blue:123.0f/255.0f alpha:1.0f];
  
}

- (IBAction)nextButtonClick:(id)sender {
  for (CountryBO* country in arryaForCountry) {
    if ([country.countryName isEqualToString:app.eastOrWestMalaysia]) {
      [[NSUserDefaults standardUserDefaults]setObject:country.countryName forKey:kCountrySelectionName];
      [[NSUserDefaults standardUserDefaults]setObject:country.Ids forKey:kCountrySelectionId];
      [[NSUserDefaults standardUserDefaults] synchronize];
//       if ([[NSUserDefaults standardUserDefaults]integerForKey:kIsLocationSelected] == 0) {
      [self sendDataToServerWithTag:TASK_PUSH_REGISTRATION];
      // }
    }
  }
   [viewForLocationSelector removeFromSuperview];
  [app setUser];
}

- (IBAction)nextTouch:(id)sender {
  buttonNext.titleLabel.textColor = [UIColor whiteColor];
  buttonNext.backgroundColor = [UIColor colorWithRed:212.0f/255.0f green:0.0f/255.0f blue:123.0f/255.0f alpha:1.0f];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(void) receiveDeviceTocken :(NSNotification *) notification {
  if ([[notification name] isEqualToString:@"CallWebServiceForPush"])
  {
    NSLog (@"Successfully received the test notification!");
 }
}

//change Position of slider
- (void) receiveChangeNotificationMessage:(NSNotification *) notification{
    NSLog(@"%@",labelEastMalaysia.text);
  if ([[notification name] isEqualToString:@"ChangeColorEast"]) {
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kIsEastMalaysia];
    [[NSUserDefaults standardUserDefaults]synchronize];
    labelEastMalaysia.textColor = [UIColor colorWithRed:(237.0f/255.0f) green:(0.0f/255.0f) blue:(140.0f/255.0f) alpha:1.0f];
    labelWestMalaysia.textColor = [UIColor blackColor];
   
    app.eastOrWestMalaysia = @"East Malaysia";
  }else{
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsEastMalaysia];
    [[NSUserDefaults standardUserDefaults]synchronize];
    labelWestMalaysia.textColor = [UIColor colorWithRed:(237.0f/255.0f) green:(0.0f/255.0f) blue:(140.0f/255.0f) alpha:1.0f];
    labelEastMalaysia.textColor = [UIColor blackColor];
    app.eastOrWestMalaysia = @"West Malaysia";
  }
  
  int lowerHeight;
  int buttonHeight;
  int i = 0;
  if (IS_IPAD) {
    lowerHeight = (55 * 500)/100;
    buttonHeight = (20 * lowerHeight)/100;
    i = 30;
  }else{
     lowerHeight = (55 * kScreenWidth)/100;
     buttonHeight = (20 * lowerHeight)/100;
  }
  
  for (UIView * v in viewWhiteForLocationSelector.subviews) {
    if ([v isKindOfClass:[SliderDemo class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]] && ![v isKindOfClass:[UILabel class]] && ![v isKindOfClass:[UIButton class]]) {
      [v removeFromSuperview];
    }
  }

  UIView *VctrDtl=[[UIView alloc]initWithFrame:CGRectMake(65 + ( 4 * i) + i/2,lowerHeight/5 , kScreenWidth - 130 - 8*i - i, buttonHeight/5 + i/6 + 50)];
 
  SliderDemo *sliderVctr=[[SliderDemo alloc] initWithFrame:CGRectMake(VctrDtl.bounds.origin.x,VctrDtl.bounds.origin.y,VctrDtl.bounds.size.width, VctrDtl.bounds.size.height)];
  sliderVctr.minimumValue =0;
  sliderVctr.selectedMinimumValue = 0;
  sliderVctr.maximumValue = 9;
  sliderVctr.selectedMaximumValue = 9;
  sliderVctr.minimumRange = 00;
  [VctrDtl addSubview:sliderVctr];
  
  [viewWhiteForLocationSelector addSubview:VctrDtl];
}


//view controller add if country malaysia selected
-(void)countryMalaysiaSelected{
  [self.view addSubview:viewForLocationSelector];
  CGAffineTransform baseTransform = viewWhiteForLocationSelector.transform;
  viewWhiteForLocationSelector.transform = CGAffineTransformTranslate(baseTransform,0,viewWhiteForLocationSelector.bounds.size.height);
  
  [UIView animateWithDuration: 0.5 animations:^{
    viewWhiteForLocationSelector.transform = baseTransform;
  }];
  viewWhiteForLocationSelector.userInteractionEnabled = YES;
  viewForLocationSelector.userInteractionEnabled = YES;
  app.eastOrWestMalaysia = @"West Malaysia";
  }

//country shortList
-(void)shortCountryList{
  arrayShortedCountry = [[NSMutableArray alloc]init];
  for (CountryBO* country in arryaForCountry) {
    if ([country.countryName isEqualToString:@"West Malaysia"] || [country.countryName isEqualToString:@"East Malaysia"]) {
    if (![arrayShortedCountry containsObject:@"Malaysia"]) {
        [arrayShortedCountry addObject:@"Malaysia"];
      }
    }else{
      [arrayShortedCountry addObject:country.countryName];
    }
  }
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayShortedCountry count];
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableForCountrySelection.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int height = 40;
    if (IS_IPAD) {
      height = 60;
    }
    return height;
 }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int height = 40;
  int i = 0;
    if (IS_IPAD) {
      height = 60;
      i = 30;
    }
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil )
    {
      cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.frame = CGRectMake(25 +( 4 * i), 0, kScreenWidth - 50 - (8 *i) , height);
    for (UIView *v in cell.contentView.subviews ) {
      if ([v isKindOfClass:[UILabel class]]) {
        [v removeFromSuperview];
      }
    }
    
    UILabel *tittleLabel = [[UILabel alloc]init];
    UILabel *labelLine = [[UILabel alloc]init];
    
    tittleLabel.frame = CGRectMake(0, 0, kScreenWidth - 50 - (8 *i) , height);
    tittleLabel.textColor = [UIColor blackColor];
    tittleLabel.textAlignment = NSTextAlignmentCenter;
    tittleLabel.text = [arrayShortedCountry objectAtIndex:indexPath.row];
  
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
  if ( [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
    countryName =@"Malaysia";
  }

  if ([countryName isEqualToString:[arrayShortedCountry objectAtIndex:indexPath.row]]) {
    cell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
  }
    tittleLabel.font = [UIFont fontWithName:kFontTimesRomanItalic size:14 + height/4];
    labelLine.frame = CGRectMake(0, height - 1, kScreenWidth - 60, 1);
    labelLine.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    
    [cell.contentView addSubview:tittleLabel];
    [cell.contentView addSubview:labelLine];
    return cell;
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSString* countryName = [arrayShortedCountry objectAtIndex:indexPath.row];
  labelCountryName.text = countryName;
  tableForCountrySelection.hidden = YES;
  [viewForCountryTable removeFromSuperview];
  
}


-(void)sendDataToServerWithTag:(int)tag
{
//   NSString *deviceTocken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DevToken"];
  requestManager.delegate=self;
  switch (tag) {
    case TASK_PUSH_REGISTRATION:
    {
      
     [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:kIsLocationSelected];
      [[NSUserDefaults standardUserDefaults]synchronize];
       [self remove_Loader];
      [self show_Loader];
      NSString *deviceTocken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DevToken"];
       NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
     
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
     
      [dictRoot setObject:deviceTocken forKey:@"deviceId"];
      [dictRoot setObject:@"" forKey:@"gcmId"];
      [dictRoot setObject:countryName forKey:kCountryId];
      [dictRoot setObject:@"IOS" forKey:@"platform"];
      [dictRoot setObject:@"PRO" forKey:@"type"];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_PUSH_REGISTRATION withURL:kURLFORPUSH with:@"POST"];
      break;
    }
    case TASK_COUNTRY_LIST:
    {
    [self remove_Loader];
    [self show_Loader];
    NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
    [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_COUNTRY_LIST withURL:kUrlForCountryExis with:@"GET"];
      break;
    }
    default:
      break;
  }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_PUSH_REGISTRATION:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
//          ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
              break;
      }
      case TASK_COUNTRY_LIST:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayAllCountry = [[NSMutableArray alloc]init];
          arrayAllCountry = [recievedDictionary objectForKey:kResponse];
          NSMutableArray* arrayAllCountryDatabase = [[NSMutableArray alloc]init];
          arrayAllCountryDatabase = [[Database getSharedInstance]getCountries];

          if ([arryaForCountry count] > 0) {
            [arryaForCountry removeAllObjects];
          }
          for (NSDictionary *obj in arrayAllCountry) {
            CountryBO* country = [[CountryBO alloc]initWithResponse:obj];
            if ([arrayAllCountryDatabase count] == 0) {
              [[Database getSharedInstance]storeCountry:country];
            }else{
              int count = 0;
            for (CountryBO *obj in arrayAllCountryDatabase) {
              if ([country.Ids isEqualToString:obj.Ids]) {
                count++;
              }
            }
              if (count == 0) {
               [[Database getSharedInstance]storeCountry:country];
              }
            }
            [arryaForCountry addObject:country];
          }
          
          int cellHeight = 40;
          int i = 0;
          if (IS_IPAD) {
            cellHeight = 60;
            i = 30;
          }
           [self shortCountryList];
          if (kScreenHeight > ([arryaForCountry count]*cellHeight)) {
            tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), (kScreenHeight)/2 - ( [arrayShortedCountry count]*cellHeight)/2, kScreenWidth - 50 - 8 * i,( [arrayShortedCountry count]*cellHeight));
          }else{
            tableForCountrySelection.frame = CGRectMake(25 +( 4 * i), 0, kScreenWidth - 50 - 8 * i,kScreenHeight);
          }

                   //tableForCountrySelection.hidden = NO;
          [tableForCountrySelection reloadData];
        }else{
        ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        break;
      }

      default:
        break;
    }
  }
  else
  {
    ALERT(nil, @"");
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  NSString *response = [request responseString];
  [self remove_Loader];
}





@end
