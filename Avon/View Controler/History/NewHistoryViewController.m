//
//  NewHistoryViewController.m
//  Avon
//
//  Created by Shipra Singh on 08/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NewHistoryViewController.h"

@interface NewHistoryViewController ()<UIScrollViewDelegate>
{
  IBOutlet UIScrollView *scrollImage;
  NSMutableArray *upperText;
  NSMutableArray *image1Text;
  NSMutableArray *image2Text;
  NSMutableArray *heading;
  NSMutableArray *headingYear;
  NSMutableArray *colorForR;
  NSMutableArray *colorForG;
  NSMutableArray *colorForB;
  NSMutableArray *imageFor1x;
  NSMutableArray *imageFor3x;
  NSMutableArray *imageForIpad;
  NSMutableArray *image2For1x;
  NSMutableArray *image2For3x;
  NSMutableArray *image2ForIpad;
  
  CGFloat offsetRadio;
}

@end

@implementation NewHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Our History") :@"":@"":@""];

  //Array of texts
  
  headingYear = [[NSMutableArray alloc]initWithObjects:@"1886",@"1900 - 1960",@"1961 - 1980",@"1981 - 1995",[@"1996 - " stringByAppendingString:LocalizedString(@"Todays")] , nil];
  upperText = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"History1"),LocalizedString(@"History2"),LocalizedString(@"History3"),LocalizedString(@"History4"),LocalizedString(@"History5"), nil];
  image1Text = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"HistoryImageName11"),LocalizedString(@"HistoryImageName21"),LocalizedString(@"HistoryImageName31"),LocalizedString(@"HistoryImageName41"),LocalizedString(@"HistoryImageName51"), nil];
  image2Text = [[NSMutableArray alloc]initWithObjects:LocalizedString(@"HistoryImageName12"),LocalizedString(@"HistoryImageName22"),LocalizedString(@"HistoryImageName32"),LocalizedString(@"HistoryImageName42"),LocalizedString(@"HistoryImageName52"), nil];

  
  //Array or RGB Color
  colorForR = [[NSMutableArray alloc]initWithObjects:@"6",@"0",@"202",@"250",@"25", nil];
  colorForG = [[NSMutableArray alloc]initWithObjects:@"92",@"175",@"83",@"7",@"61", nil];
  colorForB = [[NSMutableArray alloc]initWithObjects:@"134",@"125",@"157",@"137",@"133", nil];
  
  //Array Of Image
  imageFor1x = [[NSMutableArray alloc]initWithObjects:@"our_history_a_1@2x.png",@"our_history_b_1@2x.png",@"our_history_c_1@2x.png",@"our_history_d_1@2x.png",@"our_history_e_1@2x.png", nil];
  imageFor3x = [[NSMutableArray alloc]initWithObjects:@"our_history_a_iphone6_1.png",@"our_history_b_iphone6_1.png",@"our_history_c_iphone6_1.png",@"our_history_d_iphone6_1.png",@"our_history_e_iphone6_1.png", nil];
  imageForIpad = [[NSMutableArray alloc]initWithObjects:@"our_history_a_ipad_1.png",@"our_history_b_ipad_1.png",@"our_history_c_ipad_1.png",@"our_history_d_ipad_1.png",@"our_history_e_ipad_1.png", nil];
  image2For1x = [[NSMutableArray alloc]initWithObjects:@"our_history_a_2@2x.png",@"our_history_b_2@2x.png",@"our_history_c_2@2x.png",@"our_history_d_2@2x.png",@"our_history_e_2@2x.png", nil];
  image2For3x = [[NSMutableArray alloc]initWithObjects:@"our_history_a_iphone6_2.png",@"our_history_b_iphone6_2.png",@"our_history_c_iphone6_2.png",@"our_history_d_iphone6_2.png",@"our_history_e_iphone6_2.png", nil];
  image2ForIpad = [[NSMutableArray alloc]initWithObjects:@"our_history_a_ipad_2.png",@"our_history_b_ipad_2.png",@"our_history_c_ipad_2.png",@"our_history_d_ipad_2.png",@"our_history_e_ipad_2.png", nil];

  
    [self createingSlider];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - customizing View

-(void)createingSlider{
  scrollImage.frame = CGRectMake(0,60,kScreenWidth , kScreenHeight - 60);
  scrollImage.contentSize = CGSizeMake(CGRectGetWidth(scrollImage.frame) * 5,0);
  
  int height;
  int imageHeight;
  int x = 0;
  
  int size = 25;
  if (kScreenHeight > 667) {
    size = 35;
  }else if(kScreenHeight == 568){
    size = 30;
  }else if(kScreenHeight == 667){
    size = 30;
  }
  
  if (IS_IPAD) {
    height = (75 * 780/100);
    imageHeight = (75 * height)/100;
    x = 30;
  }else{
    height = (75 * kScreenWidth/100);
    imageHeight = (75 * height)/100;
  }
  
  
  for (UIView *v  in scrollImage.subviews) {
    if ([v isKindOfClass:[UIScrollView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
  }
  
  
  for (int i = 0 ; i<5; i++) {
    UIScrollView *viewBackgroundForKey = [[UIScrollView alloc]init];
    UIView *viewBackgroundForScroll = [[UIView alloc]init];
    //UIView *viewBackgroundLowerLogo = [[UIView alloc]init];
    UILabel* labelText = [[UILabel alloc]init];
    UIImageView* imageAvon = [[UIImageView alloc]init];
    UIImageView* imageTaiwan = [[UIImageView alloc]init];

    UILabel *labelForHeading = [[UILabel alloc]init];
    UILabel *labelForHeadingYear = [[UILabel alloc]init];
    UILabel *labelForUpperText = [[UILabel alloc]init];
    UILabel *labelFor1ImageText = [[UILabel alloc]init];
    UILabel *labelFor2ImageText = [[UILabel alloc]init];
    UIImageView *image1Middle = [[UIImageView alloc]init];
    UIImageView *image2Middle = [[UIImageView alloc]init];
    UIImageView *imageLower = [[UIImageView alloc]init];
    
    viewBackgroundForScroll.frame = CGRectMake(scrollImage.frame.size.width * i,0,scrollImage.frame.size.width , scrollImage.frame.size.height );
    viewBackgroundForKey.frame = CGRectMake(0, 0, kScreenWidth, viewBackgroundForScroll.frame.size.height);
    
    viewBackgroundForKey.delegate = self;
    viewBackgroundForKey.backgroundColor = [UIColor colorWithRed:([[colorForR objectAtIndex:i] floatValue]/255.0f) green:([[colorForG objectAtIndex:i] floatValue]/255.0f) blue:([[colorForB objectAtIndex:i] floatValue]/255.0f) alpha:1.0f];
    NSAttributedString *attributedForHedding = [[NSAttributedString alloc]initWithString:[upperText objectAtIndex:i] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaBoldItalic size:15 + x/6]}];
    CGRect rectForHedding = [attributedForHedding boundingRectWithSize:(CGSize){kScreenWidth - 40 - (2*x), CGFLOAT_MAX}
                                                               options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize sizeForHedding = rectForHedding.size;
    
    CGFloat heightsForHedding = sizeForHedding.height;
    
    
    
    
    labelForHeading.frame = CGRectMake(20 + x, 5 + x/2 , kScreenWidth - 40 - (2*x), 100 + x/2);
   
    labelForHeadingYear.frame = CGRectMake(20 + x, labelForHeading.frame.origin.y + 65  , kScreenWidth - 40 - (2*x), 40 + x/2);
    labelForUpperText.frame = CGRectMake(20 + x, labelForHeadingYear.frame.origin.y + 50 + x/2 , kScreenWidth - 40 - (2*x), heightsForHedding);
    
    if (i == 0) {
      image1Middle.frame = CGRectMake(35 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 10, (kScreenWidth - 70 - (2*x))/2, imageHeight);
      image2Middle.frame = CGRectMake(kScreenWidth/2 + 15,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 10, (kScreenWidth - 70 - (2*x))/2, imageHeight);
      
      labelFor1ImageText.frame = CGRectMake(35 + x, image1Middle.frame.origin.y + imageHeight  , (kScreenWidth - 70 - (2*x))/2, 50);
      
      labelFor2ImageText.frame = CGRectMake(kScreenWidth/2 + 15, image2Middle.frame.origin.y + imageHeight  ,(kScreenWidth - 70 - (2*x))/2, 50);
    }else if ( i == 1 || i == 2){
      
      image1Middle.frame = CGRectMake(20 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 20, (kScreenWidth - 40 - (2*x))/3, imageHeight - imageHeight/4);
      image2Middle.frame = CGRectMake(kScreenWidth/3 + 15 + x/2,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 20, kScreenWidth - (kScreenWidth/3 + 35 + x + x/2), imageHeight - imageHeight/4);
      
      labelFor1ImageText.frame = CGRectMake(20 + x, image1Middle.frame.origin.y + imageHeight - imageHeight/4  , (kScreenWidth - 40 - (2*x))/3, 50);
      
      labelFor2ImageText.frame = CGRectMake(kScreenWidth/3 + 15 + x/2, image2Middle.frame.origin.y + imageHeight - imageHeight/4 ,kScreenWidth - (kScreenWidth/3 + 35 + x + x/2), 50);
    }else if ( i == 3){
      
      image1Middle.frame = CGRectMake(20 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 30, (kScreenWidth - 40 - (2*x))/2 - 20 - x/2, imageHeight - imageHeight/2);
      image2Middle.frame = CGRectMake(kScreenWidth/2 - 15 - x/2 ,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8 - 30, kScreenWidth - (kScreenWidth/2 + 5 + x/2), imageHeight - imageHeight/2);
      
      labelFor1ImageText.frame = CGRectMake(20 + x, image1Middle.frame.origin.y + imageHeight - imageHeight/2  , (kScreenWidth - 40 - (2*x))/2 - 20, 50);
      
      labelFor2ImageText.frame = CGRectMake(kScreenWidth/2 - 15 - x/2, image2Middle.frame.origin.y + imageHeight - imageHeight/2 ,kScreenWidth - (kScreenWidth/2 + 5 + x/2), 50);
      
    }else if ( i == 4){
      
      image1Middle.frame = CGRectMake(20 + x,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8, (kScreenWidth - 40 - (2*x))/3, imageHeight - imageHeight/4);
      image2Middle.frame = CGRectMake(kScreenWidth/3 + 25,  labelForUpperText.frame.origin.y + heightsForHedding + size + x/8, kScreenWidth - (kScreenWidth/3 + 45 + x), imageHeight - imageHeight/4);
      
      labelFor1ImageText.frame = CGRectMake(20 + x, image1Middle.frame.origin.y + imageHeight - imageHeight/4  , (kScreenWidth - 40 - (2*x))/3, 50);
      
      labelFor2ImageText.frame = CGRectMake(kScreenWidth/3 + 25, image2Middle.frame.origin.y + imageHeight - imageHeight/4 ,kScreenWidth - (kScreenWidth/3 + 45 + x), 50);
    }
    
    NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
    NSString* logoText;
    if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
      logoText = @"Beauty for a purpose \n\n\n\n my.avon.com";
    }else  if ([countryName isEqualToString:@"Taiwan"]) {
      logoText = @"\n\n\n\n\n avon.com.tw";
    }else if ([countryName isEqualToString:@"India"]){
      logoText = @"Beauty for a purpose \n\n\n\n NEW YORK";
      
    }else if ([countryName isEqualToString:@"Philippines"]){
      logoText = @"Beauty for a purpose \n\n\n\n avon.com.ph";
      
    }
    imageTaiwan.backgroundColor = [UIColor clearColor];
    imageTaiwan.contentMode = UIViewContentModeCenter;
    imageAvon.contentMode = UIViewContentModeCenter;
    imageAvon.backgroundColor = [UIColor clearColor];
    labelText.backgroundColor = [UIColor clearColor];
    labelText.textAlignment = NSTextAlignmentCenter;
    labelText.text = logoText;
    
    imageAvon.contentMode = UIViewContentModeCenter;
    NSString* compareText;
    if ([countryName isEqualToString:@"Taiwan"]) {
      compareText = @"雅芳美麗好給力";
      
    }else{
      compareText = @"Beauty for a purpose";
      
    }
    
    NSString* compareText1 = @"\n AVON";
    labelText.numberOfLines = 0;
    
    NSMutableAttributedString *attributedString1;
    attributedString1 = [[NSMutableAttributedString alloc] initWithString:labelText.text attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaLight size:14 + x/2]}];
    
    NSError *error4 = nil;
    NSRegularExpression *    regex4 = [NSRegularExpression regularExpressionWithPattern:compareText options:0 error:&error4];
    
    NSArray *matches2 = [regex4 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    NSError *error3 = nil;
    NSRegularExpression *    regex3 = [NSRegularExpression regularExpressionWithPattern:@"AVON" options:0 error:&error3];
    
    NSArray *matches1 = [regex3 matchesInString:labelText.text
                                        options:0
                                          range:NSMakeRange(0, labelText.text.length)];
    
    for (NSTextCheckingResult *match in matches2)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontCoalhandLukeTRIAL size:30 + x]
                                range:matchRange];
      
    }
    
    for (NSTextCheckingResult *match in matches1)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString1 addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:kFontHelvaticaLight size:30 + x]
                                range:matchRange];
      NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
      [paragraphStyle setLineSpacing:15];
      paragraphStyle.alignment = NSTextAlignmentCenter;
      [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [compareText length])];
      
      
    }
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange([compareText length], [compareText1 length])];
    
    
    labelText.attributedText = attributedString1;
    labelText.textColor = [UIColor whiteColor];

     labelText.frame = CGRectMake(20 + x, labelFor1ImageText.frame.origin.y  +   50, kScreenWidth - 40 - (2*x), imageHeight);
    imageAvon.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/2 - (imageHeight/3)/2 + 10 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
    imageTaiwan.frame = CGRectMake(20 + x,labelText.frame.origin.y + imageHeight/6 ,kScreenWidth - 40 - (2*x) ,imageHeight/3 );
//    imageLower.frame = CGRectMake(20 + x, labelFor1ImageText.frame.origin.y  +   50, kScreenWidth - 40 - (2*x), imageHeight);
    
    //  labelForAbove.text = [questions objectAtIndex:self.indexPathRow];
    //  labelForBelow.text = [answers objectAtIndex:self.indexPathRow];
    
    
    labelForHeading.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:45 + x/6];
    labelForHeadingYear.font = [UIFont fontWithName:kFontCoalhandLukeTRIAL size:25 + x/6];
    labelForUpperText.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + x/6];
    labelFor1ImageText.font = [UIFont fontWithName:kFontTimesRomanItalic size:13 + x/6];
    labelFor2ImageText.font = [UIFont fontWithName:kFontTimesRomanItalic size:13 + x/6];
    labelForHeading.numberOfLines = 0;
    labelForUpperText.numberOfLines = 0;
    labelFor1ImageText.numberOfLines = 0;
    labelFor2ImageText.numberOfLines = 0;
    labelForHeading.textAlignment = NSTextAlignmentCenter;
     labelForHeadingYear.textAlignment = NSTextAlignmentCenter;
    labelFor1ImageText.textAlignment = NSTextAlignmentCenter;
    labelFor2ImageText.textAlignment = NSTextAlignmentCenter;
    if (i == 0) {
      labelForUpperText.textAlignment = NSTextAlignmentCenter;
     
     
    }else{
    labelForUpperText.textAlignment = NSTextAlignmentLeft;
      
    }
    [scrollImage addSubview:viewBackgroundForScroll];
    [viewBackgroundForScroll addSubview:viewBackgroundForKey];
    [viewBackgroundForKey addSubview:labelForHeading];
    [viewBackgroundForKey addSubview:labelForHeadingYear];
    [viewBackgroundForKey addSubview:labelForUpperText];
    [viewBackgroundForKey addSubview:image1Middle];
    [viewBackgroundForKey addSubview:image2Middle];
    [viewBackgroundForKey addSubview:labelFor1ImageText];
    [viewBackgroundForKey addSubview:labelFor2ImageText];
    [viewBackgroundForKey addSubview:labelText];
     [viewBackgroundForKey addSubview:imageAvon];
     if ([countryName isEqualToString:@"Taiwan"]) {
       [viewBackgroundForKey addSubview:imageTaiwan];
       
     }

    labelForHeading.text = LocalizedString(@"Our History");
    labelForHeadingYear.text = [headingYear objectAtIndex:i];
    labelForUpperText.text =  [upperText objectAtIndex:i];
    labelFor1ImageText.text =  [image1Text objectAtIndex:i];
    labelFor2ImageText.text =  [image2Text objectAtIndex:i];
    if (IS_IPAD){
      image1Middle.image = [UIImage imageNamed:[imageForIpad objectAtIndex:i]];
      image2Middle.image = [UIImage imageNamed:[image2ForIpad objectAtIndex:i]];
       imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purposeipad@1x.png"];
       imageAvon.image = [UIImage imageNamed:@"logo_ipad.png"];
//      imageLower.image = [UIImage imageNamed:@"logo_ipad@2x.png"];
    }else if (kScreenHeight > 600) {
      image1Middle.image = [UIImage imageNamed:[imageFor3x objectAtIndex:i]];
      image2Middle.image = [UIImage imageNamed:[image2For3x objectAtIndex:i]];
       imageAvon.image = [UIImage imageNamed:@"logo@3x.png"];
      imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@3x.png"];
//      imageLower.image = [UIImage imageNamed:@"logo@3x.png"];
    }else{
      image1Middle.image = [UIImage imageNamed:[imageFor1x objectAtIndex:i]];
      image2Middle.image = [UIImage imageNamed:[image2For1x objectAtIndex:i]];
       imageAvon.image = [UIImage imageNamed:@"logo@2x.png"];
        imageTaiwan.image = [UIImage imageNamed:@"tv_bueaty_purpose@2x.png"];
//      imageLower.image = [UIImage imageNamed:@"logo@2x.png"];
    }
    
    labelForHeading.textColor = [UIColor whiteColor];
    labelForHeadingYear.textColor = [UIColor whiteColor];
    labelForUpperText.textColor = [UIColor whiteColor];
    labelFor1ImageText.textColor = [UIColor whiteColor];
    labelFor2ImageText.textColor = [UIColor whiteColor];
    image1Middle.contentMode = UIViewContentModeRedraw;
    image2Middle.contentMode = UIViewContentModeRedraw;
    imageLower.contentMode = UIViewContentModeCenter;
    viewBackgroundForKey.contentSize = CGSizeMake(0, labelText.frame.origin.y + imageHeight - 35 - 2*x);
    NSMutableAttributedString *attributedString;
    attributedString = [[NSMutableAttributedString alloc] initWithString:[upperText objectAtIndex:i] attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontHelvaticaLight size:15 + x/6]}];
    
    NSError *error2 = nil;
    NSRegularExpression *    regex2 = [NSRegularExpression regularExpressionWithPattern:LocalizedString(@"BoldHistory") options:0 error:&error2];
    
    NSArray *matches = [regex2 matchesInString:[upperText objectAtIndex:i]
                                       options:0
                                         range:NSMakeRange(0, labelForUpperText.text.length)];
    
    for (NSTextCheckingResult *match in matches)
    {
      NSRange matchRange = [match rangeAtIndex:0];
      [attributedString addAttribute:NSFontAttributeName
                               value:[UIFont fontWithName:kFontHelavaticaBold size:15 + x/6]
                               range:matchRange];
      
    }
    labelForUpperText.attributedText = attributedString;

  }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollImage) {
    
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    if (scrollViews.contentOffset.x <= -1) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(0, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else if (scrollViews.contentOffset.x >= (kScreenWidth * 4)+1 ) {
      [scrollViews setScrollEnabled:NO];
      [scrollImage setContentOffset:CGPointMake(kScreenWidth * 4, 0) animated:YES];
      [scrollViews setScrollEnabled:YES];
    }else{
      self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
    }
  }
}

- (void)setOffsetRadio:(CGFloat)offsetRadios
{
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    
    [scrollImage setContentOffset:CGPointMake(scrollImage.frame.size.width + scrollImage.frame.size.width * offsetRadios, 0)];
  }
}

-(void)backButtonTap{
  
  [slideNavController toggleLeftMenu];
}


@end
