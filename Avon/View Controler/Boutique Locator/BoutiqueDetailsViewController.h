//
//  BoutiqueDetailsViewController.h
//  Avon
//
//  Created by Shipra Singh on 24/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface BoutiqueDetailsViewController : BaseViewController
@property (assign,nonatomic)  NSInteger indexPathRow;
@property (strong,nonatomic)  NSMutableArray* arrayBLocationDetails;
@end
