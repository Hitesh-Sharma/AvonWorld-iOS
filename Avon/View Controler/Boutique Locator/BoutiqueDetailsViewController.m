//
//  BoutiqueDetailsViewController.m
//  Avon
//
//  Created by Shipra Singh on 24/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//
#define METERS_PER_MILE 1609.344
#import "BoutiqueDetailsViewController.h"
#import <MapKit/MapKit.h>
#import "BoutiqueLocatorBO.h"
#import <GoogleMaps/GoogleMaps.h>

@interface BoutiqueDetailsViewController ()<CLLocationManagerDelegate,MKMapViewDelegate,GMSMapViewDelegate,UIScrollViewDelegate>
{
  
  IBOutlet UIView *viewForGoogleMap;
  IBOutlet UIScrollView *scrollViewForSwap;
  IBOutlet UIButton *buttonGuideMe;
  IBOutlet MKMapView *mapView;
  IBOutlet UIImageView *imageCallIcon;
  
  NSString* latForGuidMe;
  NSString* longForGuidMe;
  int paging;
  
}

@end

@implementation BoutiqueDetailsViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self customView];
  [self createdScrollForImage];
  [self designHeaderOfTheScreen:@"back_icon.png" :LocalizedString(@"Butiran Butik") :@"":@"":@""];
  [scrollViewForSwap setContentOffset:CGPointMake((self.indexPathRow * CGRectGetWidth(scrollViewForSwap.frame)) , 0) animated:YES];
  [self showMapofStore:self.indexPathRow];
}

-(void)customView{
  
  int lowerHeight;
  int buttonHeight;
  int i = 0;
  if (IS_IPAD) {
    lowerHeight = (50 * 500)/100;
    buttonHeight = (25 * lowerHeight)/100;
    i = 30;
    
    imageCallIcon.image = [UIImage imageNamed:@"call_icon@3x.png"];
    imageCallIcon.contentMode = UIViewContentModeScaleAspectFit;
  }else{
    lowerHeight =  (50 * kScreenWidth)/100;
    buttonHeight = (25 * lowerHeight)/100;
    imageCallIcon.contentMode = UIViewContentModeCenter;
  }
  
  mapView.frame = CGRectMake(0, 60, kScreenWidth, (49 * (kScreenHeight - 60)/100));
  viewForGoogleMap.frame = CGRectMake(0, 60, kScreenWidth, (49 * (kScreenHeight - 60)/100));
  
  if (kScreenHeight == 480) {
    buttonGuideMe.frame = CGRectMake(25 + (4 *i), kScreenHeight - buttonHeight - 10 - i/6, kScreenWidth - 50 - 8*i, buttonHeight);
    scrollViewForSwap.frame = CGRectMake(0,mapView.frame.size.height + 90 + 2*i,kScreenWidth , kScreenHeight - (buttonHeight + 10 + i/6 + mapView.frame.size.height + 90 + 2*i));
  }else{
    buttonGuideMe.frame = CGRectMake(25 + (4 *i), kScreenHeight - buttonHeight - 30 - i/6, kScreenWidth - 50 - 8*i, buttonHeight);
    scrollViewForSwap.frame = CGRectMake(0,mapView.frame.size.height + 90 + 2*i,kScreenWidth , kScreenHeight - (buttonHeight + 30 + i/6 + mapView.frame.size.height + 90 + 2*i));
  }
  
  imageCallIcon.frame = CGRectMake(kScreenWidth - 60 - 3*i/2, ( 60 + mapView.frame.size.height) - 25 - i/6, 50 + i/6, 50 + i/6);
  
  scrollViewForSwap.pagingEnabled = YES;
  scrollViewForSwap.delegate = self;
  [self roundCornersOfView:buttonGuideMe radius:buttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  buttonGuideMe.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];
  UITapGestureRecognizer *onClickCall = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callClick:)];
  
  imageCallIcon.userInteractionEnabled=YES;
  [imageCallIcon addGestureRecognizer:onClickCall];
  [buttonGuideMe setTitle:LocalizedString(@"Guide Me") forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)createdScrollForImage{
  int count = [self.arrayBLocationDetails count];
  scrollViewForSwap.contentSize = CGSizeMake(CGRectGetWidth(scrollViewForSwap.frame) * count, 1);
  
  for (UIView *v  in scrollViewForSwap.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UILabel class]]) {
      [v removeFromSuperview];
    }
  }
  
  int i = 0;
  if (IS_IPAD) {
    i = 30;
  }
  
  for (int j = 0 ; j<count; j++) {
    
    BoutiqueLocatorBO* obj =  [self.arrayBLocationDetails objectAtIndex:j];
    
    
    UIView *viewForProductImage = [[UIView alloc]init];
    viewForProductImage.frame = CGRectMake(scrollViewForSwap.frame.size.width * j,0,scrollViewForSwap.frame.size.width , scrollViewForSwap.frame.size.height);
    
    NSAttributedString *attributedText1 = [[NSAttributedString alloc]initWithString:obj.blStoreName attributes:@
                                           {
                                           NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:23 + 2*i/2]
                                           }];
    CGRect rect1 = [attributedText1 boundingRectWithSize:(CGSize){kScreenWidth - 70 - 2 * i, CGFLOAT_MAX}
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                 context:nil];
    CGSize size1 = rect1.size;
    CGFloat heights1 = size1.height;
    UILabel *labelStoreName = [[UILabel alloc]initWithFrame:CGRectMake(10 + i/3,0, kScreenWidth - 40, heights1)];
    
    labelStoreName.numberOfLines = 0;
    labelStoreName.font = [UIFont fontWithName:kFontHelvaticaLight size:23 + 2*i/2];
    
    
    
    NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:obj.blStoreAddress attributes:@
                                          {
                                          NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:15 + 3*i/6]
                                          }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - 70 - 2 * i, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    
    CGFloat heights = size.height;
    UILabel *labelStoreAddress;
    UIImageView *imageLoctionIcon;
    if (kScreenHeight == 480) {
      labelStoreAddress = [[UILabel alloc]initWithFrame:CGRectMake(35 + i + i/2, heights1  , kScreenWidth - 70 - 2 * i, heights)];
      imageLoctionIcon = [[UIImageView alloc]initWithFrame:CGRectMake(5 + i/3, heights1  , 30 + i, 25 + i)];
    }else{
      labelStoreAddress = [[UILabel alloc]initWithFrame:CGRectMake(35 + i + i/2, heights1 + 10 , kScreenWidth - 70 - 2 * i, heights)];
      imageLoctionIcon = [[UIImageView alloc]initWithFrame:CGRectMake(5 + i/3, heights1 + 10 , 30 + i, 25 + i)];
    }
    
    imageLoctionIcon.contentMode = UIViewContentModeCenter;
    labelStoreAddress.numberOfLines = 0;
    labelStoreAddress.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + 3*i/6];
    
    viewForProductImage.backgroundColor = [UIColor whiteColor];
    imageLoctionIcon.backgroundColor = [UIColor clearColor];
    [scrollViewForSwap addSubview:viewForProductImage];
    [viewForProductImage addSubview:labelStoreAddress];
    [viewForProductImage addSubview:labelStoreName];
    [viewForProductImage addSubview:imageLoctionIcon];
    if (IS_IPAD) {
      imageLoctionIcon.image = [UIImage imageNamed:@"location_pin_ipad.png"];
    }else{
      imageLoctionIcon.image = [UIImage imageNamed:@"location_pin.png"];
    }
    labelStoreAddress.text = obj.blStoreAddress;
    labelStoreName.text = obj.blStoreName;
    
  }
}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollViews {
  if (scrollViews == scrollViewForSwap) {
    if (scrollViews.contentOffset.y != 0) {
      CGPoint offset = scrollViews.contentOffset;
      offset.y = 0;
      scrollViews.contentOffset = offset;
    }
    
    self.offsetRadio = scrollViews.contentOffset.x/CGRectGetWidth(scrollViews.frame) - 1;
  }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  CGFloat pageWidth = scrollViews.contentOffset.x;
  int i = pageWidth/scrollViewForSwap.frame.size.width;
  paging = i;
  [self showMapofStore:i];
  [self startSpin];
}

- (void)setOffsetRadio:(CGFloat)offsetRadios{
  CGFloat offsetRadio;
  if (offsetRadio != offsetRadios)
  {
    offsetRadio = offsetRadios;
    [scrollViewForSwap setContentOffset:CGPointMake(scrollViewForSwap.frame.size.width + scrollViewForSwap.frame.size.width * offsetRadios, 0)];
  }
}


- (void) spinWithOptions: (UIViewAnimationOptions) options {
  // this spin completes 360 degrees every 2 seconds
  [UIView animateWithDuration: 0.2f
                        delay: 0.0f
                      options: options
                   animations: ^{
                     imageCallIcon.transform = CGAffineTransformRotate(imageCallIcon.transform, M_PI  );
                   }
                   completion: ^(BOOL finished) {
                     if (finished) {
                       if (options != UIViewAnimationOptionCurveEaseOut) {
                         // one last spin, with deceleration
                         [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                       }
                     }
                   }];
}


- (void) startSpin {
  [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
}


- (IBAction)clickOnGuideMe:(id)sender {
  NSString* url;
  if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: @"comgooglemaps://"]]) {
    
    url = [NSString stringWithFormat:@"comgooglemaps://?daddr=%f,%f", [latForGuidMe floatValue], [longForGuidMe floatValue]];
    
    
  }else{
    url = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%f,%f", [latForGuidMe floatValue], [longForGuidMe floatValue]];
  }
  [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
  
}

-(void)backButtonTap{
  [self.navigationController popViewControllerAnimated:YES];
}


-(void)showMapofStore:(int)pageNo{
  BoutiqueLocatorBO* obj = [self.arrayBLocationDetails objectAtIndex:pageNo];
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[obj.blLatitude floatValue]
                                                          longitude:[obj.blLongitude floatValue]
                                                               zoom:10];
  GMSMapView* mapViews = [GMSMapView mapWithFrame:mapView.bounds camera:camera];
  CLLocationCoordinate2D position = CLLocationCoordinate2DMake([obj.blLatitude floatValue], [obj.blLongitude floatValue]);
  GMSMarker *marker = [GMSMarker markerWithPosition:position];
  //marker.title = @"Hello World";
  marker.map = mapViews;
  mapViews.myLocationEnabled = YES;
  mapViews.settings.myLocationButton = YES;
  mapViews.padding = UIEdgeInsetsMake(0, 0, 20, 0);
  [viewForGoogleMap addSubview:mapViews];
  
  latForGuidMe = obj.blLatitude;
  longForGuidMe = obj.blLongitude;
  //  tittle = obj.blLocality;
}


-(void)callClick:(UITapGestureRecognizer*)recognize{
  BoutiqueLocatorBO* obj = [self.arrayBLocationDetails objectAtIndex:paging];
  NSString *phoneNumber = [@"tel://" stringByAppendingString:obj.blContactNo];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
  
}


@end
