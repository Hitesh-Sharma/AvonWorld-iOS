//
//  BoutiqueLocatorViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//
double M_PI = 3.141592653589793;
#define d2r (M_PI / 180.0)

#import "BoutiqueLocatorViewController.h"
#import "BoutquesTableViewCell.h"
#import "BoutiqueDetailsViewController.h"
#import "UIViewController+KeyboardAnimation.h"
#import "BoutiqueLocatorBO.h"
#import "StateBO.h"
#import <GoogleMaps/GoogleMaps.h>

@interface BoutiqueLocatorViewController ()<ConnectionManager_Delegate,GMSMapViewDelegate>
{
  
  IBOutlet UITextField *textSelectCity;
  IBOutlet UIButton *buttonSelectState;
  IBOutlet UITableView *tableForSelectState;
  IBOutlet UILabel *sepraterLine;
  IBOutlet UIImageView *imageDropDown;
  IBOutlet UITableView *tableBoutique;
  IBOutlet UIView *viewForSelectingState;
  
  NSMutableArray * arrayBLDetails;
  NSMutableArray *shortedCity;
  NSMutableArray * arrayStateName;
  int buttonHeight;
  int heightOfKeyPad;
  BOOL isSelectedTable;
  BOOL changePositionOfArrow;
  ConnectionManager *requestManager;
  int selectState;
  int buttonTapForState;
  CGFloat longitude;
  CGFloat latitude;
  GMSMapView*  mapView_;
}

@end

@implementation BoutiqueLocatorViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
  textSelectCity.text = LocalizedString(@"Select State");
  
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                          longitude:151.2086
                                                               zoom:12];
  
  mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
  
  [mapView_ addObserver:self
             forKeyPath:@"myLocation"
                options:NSKeyValueObservingOptionNew
                context:NULL];
  dispatch_async(dispatch_get_main_queue(), ^{
    mapView_.myLocationEnabled = YES;
  });
  
  [self subscribeToKeyboard];
  selectState = 0;
  buttonTapForState = 0;
  shortedCity = [[NSMutableArray alloc]init];
  arrayStateName = [[NSMutableArray alloc]init];
  arrayBLDetails = [[NSMutableArray alloc]init];
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
    ALERT(@"", LocalizedString(@"Boutique Connection"));
  }else{
  [self sendDataToServerWithTag:TASK_GET_STATE];
  }
  [self customView];
  
  if ([[NSUserDefaults standardUserDefaults]boolForKey:kisBoutiqueByButton] == YES) {
    [self designHeaderOfTheScreen:@"back_icon.png" :LocalizedString(@"Boutiques") :@"":@"":@""];
    
  }else{
    [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Boutiques"):@"":@"":@""];
  }
  
}

-(void)customView{
  int lowerHeight;
  
  int i = 0;
  if (IS_IPAD) {
    lowerHeight = (55 * 500)/100;
    buttonHeight = (20 * lowerHeight)/100;
    i = 30;
    imageDropDown.image = [UIImage imageNamed:@"state_drop_down_ipad.png"];
  }else{
    lowerHeight = (55 * kScreenWidth)/100;
    buttonHeight = (20 * lowerHeight)/100;
  }
  
  viewForSelectingState.frame = CGRectMake(10, 80, kScreenWidth - 20, buttonHeight);
  textSelectCity.frame = CGRectMake(20, 0, kScreenWidth - (20 *viewForSelectingState.frame.size.width)/100 - 25, viewForSelectingState.frame.size.height);
  imageDropDown.frame = CGRectMake(kScreenWidth - (20 *viewForSelectingState.frame.size.width)/100 , 0, (20 *viewForSelectingState.frame.size.width)/100 - 20, viewForSelectingState.frame.size.height);
  sepraterLine.frame = CGRectMake(imageDropDown.frame.origin.x - 3, 5, 1, viewForSelectingState.frame.size.height - 10);
  tableBoutique.frame = CGRectMake(0, buttonHeight + 90, kScreenWidth, kScreenHeight - (buttonHeight + 90));
  [tableBoutique setContentOffset:CGPointMake(0,tableBoutique.frame.size.height + 10)];
  buttonSelectState.frame = CGRectMake(0 , 0, kScreenWidth - 20, viewForSelectingState.frame.size.height);
  tableForSelectState.frame = CGRectMake(10, 75 + buttonHeight, kScreenWidth - 20, 3 *buttonHeight);
  
  tableBoutique.separatorColor = [UIColor clearColor];
  tableForSelectState.separatorColor = [UIColor clearColor];
  textSelectCity.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:16 + i/6];
  tableBoutique.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
  tableForSelectState.hidden = YES;
  [self changeRadius];
  
  tableForSelectState.layer.cornerRadius = 4;
  tableForSelectState.layer.masksToBounds = YES;
  tableForSelectState.layer.shadowColor = [[UIColor blackColor] CGColor];
  tableForSelectState.layer.shadowOffset = CGSizeMake(0.0f,0.5f);
  tableForSelectState.layer.shadowOpacity = .5f;
  tableForSelectState.layer.shadowRadius = 0.5f;
  tableForSelectState.layer.borderWidth = 1.0;
  tableForSelectState.layer.borderColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
  tableForSelectState.layer.borderColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  [mapView_ removeObserver:self
                forKeyPath:@"myLocation"
                   context:NULL];
}


/*It takes back to menu screen*/
-(void)backButtonTap{
  [textSelectCity resignFirstResponder];
  
  if ([[NSUserDefaults standardUserDefaults]boolForKey:kisBoutiqueByButton] == YES) {
    [self.navigationController popViewControllerAnimated:NO];
  }else{
    [slideNavController toggleLeftMenu];
  }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  if (scrollViews.tag == 0) {
    CGFloat currentOffset = scrollViews.contentOffset.y;
    CGFloat maximumOffset = scrollViews.contentSize.height - scrollViews.frame.size.height;
    if (maximumOffset - currentOffset <= 10.0) {
      [self performSelector:@selector(sendDataToServerOnPagging) withObject:nil afterDelay:1];
    }
  }
}

-(void)sendDataToServerOnPagging{
  selectState = 0;
  [self sendDataToServerWithTag:TASK_GET_BOUTIQUES];
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  if (tableView.tag == 0) {
    return [arrayBLDetails count];
  }else{
    return [shortedCity count] + 1;
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableBoutique.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  if (tableView.tag == 0) {
    
    int height = 120;
    if (IS_IPAD) {
      height = 185;
    }
    return height;
    
  }else{
    int height = 40;
    if (IS_IPAD) {
      height = 60;
    }
    return height;
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  if (tableView.tag == 0) {
    
    int height = 120;
    int i = 0;
    if (IS_IPAD) {
      height = 185;
      i = 65;
    }
    
    NSString *simpleTableIdentifier = @"BoutquesTableViewCell";
    BoutquesTableViewCell *cell = (BoutquesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *  nib = [[NSBundle mainBundle] loadNibNamed:@"BoutquesTableViewCell" owner:self options:nil];;
    cell = [[BoutquesTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    cell = [nib objectAtIndex:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    BoutiqueLocatorBO* obj = [arrayBLDetails objectAtIndex:indexPath.row];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:obj.blStoreAddress attributes:@
                                          {
                                          NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:15 + i/6]
                                          }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - (15 + 45 + i/3 +  50 + i +(height - 40 - i/3)), CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    CGFloat heights = size.height;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.frame = CGRectMake(0, 0, kScreenWidth, height );
    
    cell.imageLocation.frame = CGRectMake(10 + i/6, 17 + i/6, height - 40 - i/3 , height - 40 - i/3);
    cell.labelDistance.frame = CGRectMake(10 + i/6, height - 20 - i/16, height - 40 - i/3 , 20 + i/16);
    cell.labelSepraterLine.frame = CGRectMake(0, height - 1, kScreenWidth, 1);
    cell.labelStoreName.frame = CGRectMake(cell.imageLocation.frame.size.width + 20 + i/3, height/2 - height/3 + 5 , kScreenWidth - (cell.imageLocation.frame.size.width + 20 + i/3 +  50 + i ), height/3);
    cell.imageLocationLogo.frame = CGRectMake(cell.imageLocation.frame.size.width + 20 + i/3, height/2   , 15, height/4);
    
    if (heights > height/4) {
      cell.labelLocationOfStore.frame = CGRectMake(cell.imageLocation.frame.size.width + 40 + i/2, height/2 , kScreenWidth - (cell.imageLocation.frame.size.width + 45 + i/3 +  50 + i ), height/2 - 10);
    }else{
      cell.labelLocationOfStore.frame = CGRectMake(cell.imageLocation.frame.size.width + 40 + i/2, height/2 - 2 , kScreenWidth - (cell.imageLocation.frame.size.width + 45 + i/3 +  50 + i ), height/4);
    }
    cell.imageCall.frame = CGRectMake(kScreenWidth - 50 - i/2, height/2 - 20 - i/6, 40 + i/3, 40 + i/3);
    cell.labelLocationOfStore.font = [UIFont fontWithName:kFontHelvaticaLight size:15 + i/6];
    cell.labelDistance.font = [UIFont fontWithName:kFontHelvaticaLight size:12 + i/6];
    cell.labelStoreName.font = [UIFont fontWithName:kFontHelvaticaLight size:20 + i/6];
    if (IS_IPAD) {
      cell.imageCall.image = [UIImage imageNamed:@"small_call_icon_ipad.png"];
      cell.imageLocation.image = [UIImage imageNamed:@"map_icon_ipad.png"];
      cell.imageLocationLogo.image = [UIImage imageNamed:@"small_location_icon_ipad.png"];
    }
    UITapGestureRecognizer *onClickCall = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callClick:)];
    
    cell.imageCall.userInteractionEnabled=YES;
    [cell.imageCall addGestureRecognizer:onClickCall];
    cell.imageCall.tag = indexPath.row + 1000;
    
    cell.labelStoreName.text = obj.blStoreName;
    cell.labelLocationOfStore.text = obj.blStoreAddress;
    NSLog(@"%lu",(unsigned long)[obj.blDistance length]);
    if ([obj.blDistance length] > 0) {
      int distanceInMeter = [obj.blDistance intValue] * 1000;
      if (distanceInMeter >= 1000) {
        //        int distance = [obj.blDistance intValue];
        cell.labelDistance.text = [[NSString stringWithFormat:@"%d",distanceInMeter/1000] stringByAppendingString:@" km"];
      }else{
        cell.labelDistance.text = [[NSString stringWithFormat:@"%d",distanceInMeter] stringByAppendingString:@" m"];
      }
      
    }
    
    return cell;
  }else{
    int height = 40;
    if (IS_IPAD) {
      height = 60;
    }
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil )
    {
      cell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.frame = CGRectMake(0, 0, kScreenWidth - 20 , height);
    for (UIView *v in cell.contentView.subviews ) {
      if ([v isKindOfClass:[UILabel class]]) {
        [v removeFromSuperview];
      }
    }
    
    UILabel *tittleLabel = [[UILabel alloc]init];
    UILabel *labelLine = [[UILabel alloc]init];
    tittleLabel.frame = CGRectMake(20, 0, kScreenWidth - 50 , height);
    tittleLabel.textColor = [UIColor blackColor];
    tittleLabel.textAlignment = NSTextAlignmentLeft;
    if (indexPath.row == 0) {
      tittleLabel.text = LocalizedString(@"Semua");
    }else{
      StateBO* state = [shortedCity objectAtIndex:indexPath.row - 1];
      tittleLabel.text = state.stateName;
    }
    tittleLabel.font = [UIFont fontWithName:kFontTimesRomanItalic size:14 + height/4];
    labelLine.frame = CGRectMake(0, height - 1, kScreenWidth - 20, 1);
    labelLine.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    [cell.contentView addSubview:tittleLabel];
    [cell.contentView addSubview:labelLine];
    return cell;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [textSelectCity endEditing:YES];
  if (tableView.tag == 1) {
    if (indexPath.row == 0) {
      textSelectCity.text = LocalizedString(@"Semua");
      selectState = 0;
    }else{
      StateBO* state = [shortedCity objectAtIndex:indexPath.row - 1];
      textSelectCity.text = state.stateName;
      selectState = 1;
    }
    isSelectedTable = NO;
    tableForSelectState.hidden = YES;
    [self startSpin];
    [self changeRadius];
    [arrayBLDetails removeAllObjects];
    if ([textSelectCity.text isEqualToString:LocalizedString(@"Semua")]) {
      [self callingAllData];
    }else{
     [self sendDataToServerWithTag:TASK_GET_BOUTIQUES];
    }
  }else if (tableView.tag == 0){
    
    BoutiqueDetailsViewController *obj = [[BoutiqueDetailsViewController alloc]initWithNibName:@"BoutiqueDetailsViewController" bundle:nil];
    obj.indexPathRow = indexPath.row;
    obj.arrayBLocationDetails = arrayBLDetails;
    [self.navigationController pushViewController:obj animated:NO];
    
    
  }
}

- (IBAction)selectingState:(id)sender {
  int height = 40;
  if (IS_IPAD) {
    height = 60;
  }
  [textSelectCity resignFirstResponder];
  if (isSelectedTable == NO) {
    isSelectedTable = YES;
    changePositionOfArrow = NO;
    buttonTapForState = 1;
    [self selectingStateTable:textSelectCity.text];
    
  }else{
    buttonTapForState = 0;
    changePositionOfArrow = YES;
    isSelectedTable = NO;
    [self startSpin];
    tableForSelectState.hidden = YES;
    [self changeRadius];
    
  }
}

-(void)changeRadius{
  
  UIBezierPath *maskPath;
  maskPath = [UIBezierPath bezierPathWithRoundedRect:viewForSelectingState.bounds
                                   byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                         cornerRadii:CGSizeMake(0, 0)];
  
  CAShapeLayer* shape = [[CAShapeLayer alloc] init];
  [shape setPath:maskPath.CGPath];
  viewForSelectingState.layer.mask = shape;
  
  
  [self roundCornersOfView:viewForSelectingState radius:buttonHeight/6  borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] borderWidth:1];
  viewForSelectingState.layer.shouldRasterize = YES;
  [viewForSelectingState.layer setShadowRadius:1.0f];
  viewForSelectingState.layer.shadowColor = [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] CGColor];
  viewForSelectingState.layer.shadowOffset = CGSizeMake(1, 1);
  viewForSelectingState.layer.shadowOpacity = 1.0f;
  viewForSelectingState.layer.shadowRadius = 0.0f;
}

#pragma mark - TextField Deligate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
  
  
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
}

// This method enables or disables the processing of return key
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
  [textField resignFirstResponder];
  return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
  buttonTapForState = 0;
  NSString *newString = [textSelectCity.text stringByReplacingCharactersInRange:range withString:string];
  
  [self selectingStateTable:newString];
  
  return YES;
  
}


-(void)selectingStateTable:(NSString *)text{
  //   if ([text length] != 0 || [textSelectCity.text length] == 0) {
  [self searchTableView:@""];
  [self roundCornersOfView:viewForSelectingState radius:0  borderColor:[UIColor clearColor] borderWidth:0];
  UIBezierPath *maskPath;
  maskPath = [UIBezierPath bezierPathWithRoundedRect:viewForSelectingState.bounds
                                   byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                         cornerRadii:CGSizeMake(buttonHeight/6, buttonHeight/6)];
  
  CAShapeLayer* shape = [[CAShapeLayer alloc] init];
  [shape setPath:maskPath.CGPath];
  viewForSelectingState.layer.mask = shape;
  CAShapeLayer*   frameLayer = [CAShapeLayer layer];
  frameLayer.frame =viewForSelectingState.bounds;
  frameLayer.path = maskPath.CGPath;
  frameLayer.borderWidth = 1.0f;
  frameLayer.borderColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
  frameLayer.strokeColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
  frameLayer.fillColor = nil;
  
  [viewForSelectingState.layer addSublayer:frameLayer];
  tableForSelectState.hidden = NO;
  //  }else{
  //     [self searchTableView:text];
  //    isSelectedTable = NO;
  //    tableForSelectState.hidden = YES;
  //    [self changeRadius];
  //
  //    if ([text isEqualToString:@""]) {
  //      selectState = 1;
  //      [self sendDataToServerOnPagging:@""];
  //    }
  // }
}



- (void) spinWithOptions: (UIViewAnimationOptions) options {
  // this spin completes 360 degrees every 2 seconds
  [UIView animateWithDuration: 0.2f
                        delay: 0.0f
                      options: options
                   animations: ^{
                     imageDropDown.transform = CGAffineTransformRotate(imageDropDown.transform, M_PI/2  );
                   }
                   completion: ^(BOOL finished) {
                     if (finished) {
                       
                       if (options != UIViewAnimationOptionCurveEaseOut) {
                         
                         [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                         
                       }
                     }
                   }];
}


- (void) startSpin {
  [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
}

- (void) searchTableView:(NSString *)text
{
  int height = 40;
  if (IS_IPAD) {
    height = 60;
  }
  
  if (shortedCity!=nil) {
    [shortedCity removeAllObjects];
  }
  
  if ([text length]!=0 && ![text isEqualToString:@""])
  {
    [shortedCity addObjectsFromArray:arrayStateName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"stateName contains[cd]%@",text];
    NSArray *results = [shortedCity filteredArrayUsingPredicate:predicate];
    
    if (shortedCity!=nil) {
      [shortedCity removeAllObjects];
    }
    [shortedCity addObjectsFromArray:results];
    if (buttonTapForState == 1) {
      changePositionOfArrow = NO;
      [self startSpin];
    }else
      if (isSelectedTable == YES && shortedCity.count > 0 && [text length] == 1 &&  changePositionOfArrow == YES) {
        changePositionOfArrow = NO;
      }
      else if ((shortedCity.count > 0 && [text length] == 1 &&
                
                [textSelectCity.text length] == 0) || (changePositionOfArrow == YES && shortedCity.count > 0)) {
        
        changePositionOfArrow = NO;
        [self startSpin];
        
        
      }else if (shortedCity.count == 0 && changePositionOfArrow == NO){
        changePositionOfArrow = YES;
        [self startSpin];
      }
    
  }else{
    [shortedCity addObjectsFromArray:arrayStateName];
    if ( changePositionOfArrow == NO  && isSelectedTable == YES){
      changePositionOfArrow = YES;
      [self startSpin];
    }else if ( changePositionOfArrow == NO  && isSelectedTable == NO){
      changePositionOfArrow = NO;
      [self startSpin];
    }
    
    
  }
  
  
  int heightOfTable = kScreenHeight - heightOfKeyPad - (75 + buttonHeight) - 10;
  if (heightOfTable > (([shortedCity count] + 1)*height)) {
    tableForSelectState.frame = CGRectMake(10, 75 + buttonHeight, kScreenWidth - 20,( ([shortedCity count] + 1)*height));
  }else{
    tableForSelectState.frame = CGRectMake(10, 75 + buttonHeight, kScreenWidth - 20,heightOfTable);
  }
  
  [ tableForSelectState reloadData];
}


//key pad animation
- (void)subscribeToKeyboard {
  [self an_subscribeKeyboardWithAnimations:^(CGRect keyboardRect, NSTimeInterval duration, BOOL isShowing) {
    if (isShowing) {
      CGRectGetHeight(keyboardRect);
      heightOfKeyPad = keyboardRect.size.height;
    }else{
      heightOfKeyPad = 0;
    }
    [self.view layoutIfNeeded];
  } completion:nil];
}



-(void)callClick:(UITapGestureRecognizer*)recognize{
  NSInteger section = (recognize.view.tag)/1000;
  NSInteger row = (recognize.view.tag)%1000;
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
  BoutiqueLocatorBO* obj = [arrayBLDetails objectAtIndex:indexPath.row ];
  
  NSString *phoneNumber = [@"tel://" stringByAppendingString:obj.blContactNo];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
  
}



-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_GET_STATE:
    {
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
      
      [dictRoot setObject:countryID forKey:kCountryId];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_STATE withURL:kUrlForState with:@"POST"];
      break;
    }
    case TASK_GET_BOUTIQUES:
    {
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
    NSString *state = [textSelectCity.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
      NSString *stateId;
      if ([textSelectCity.text isEqualToString:LocalizedString(@"Semua")]||[textSelectCity.text isEqualToString:@""] || [textSelectCity.text length] == 1 || [textSelectCity.text isEqualToString:LocalizedString(@"Select State")] ) {
        stateId =  @"";
      }else{
        for (StateBO* obj in arrayStateName) {
          if ([obj.stateName isEqualToString:textSelectCity.text]) {
            stateId = obj.Ids;
          }
        }
      }
      [dictRoot setObject:countryID forKey:kCountryId];
      [dictRoot setObject:stateId forKey:kStateId];
      if (latitude == 0 && longitude == 0) {
        [dictRoot setObject:@"" forKey:kLatitude];
        [dictRoot setObject:@"" forKey:kLongitude];
      }else{
        [dictRoot setObject:[NSString stringWithFormat:@"%f",latitude] forKey:kLatitude];
        [dictRoot setObject:[NSString stringWithFormat:@"%f",longitude] forKey:kLongitude];
      }
      
      [dictRoot setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arrayBLDetails count]] forKey:kOffset];
      [dictRoot setObject:@"10" forKey:kLimit];
      
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_GET_BOUTIQUES withURL:kUrlForBoutiqueLocation with:@"POST"];
      break;
    }
      
    default:
      break;
      
      
  }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_GET_STATE:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayOfState = [[NSMutableArray alloc]init];
          arrayOfState = [recievedDictionary objectForKey:kResponse];
          if ([arrayOfState count] > 0) {
            for (NSDictionary *obj in arrayOfState) {
              StateBO* state = [[StateBO alloc]initWithResponse:obj];
              
              [arrayStateName addObject:state];
              
            }
            if (shortedCity!=nil) {
              [shortedCity removeAllObjects];
            }
            [shortedCity addObjectsFromArray:arrayStateName];
            [tableForSelectState reloadData];
            [self sendDataToServerWithTag:TASK_GET_BOUTIQUES];
            
          }
        }else{
          ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
        }
        break;
      }
      case TASK_GET_BOUTIQUES:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          NSMutableArray* arrayBoutique = [[NSMutableArray alloc]init];
          arrayBoutique = [recievedDictionary objectForKey:kResponse];
          
          if (selectState == 1) {
            [arrayBLDetails removeAllObjects];
          }
          
           //if ([arrayBoutique count] != 0) {
          
          if ([arrayBoutique count] > 0) {
           
          for (NSDictionary *obj in arrayBoutique) {
        BoutiqueLocatorBO* boutiques = [[BoutiqueLocatorBO alloc]initWithResponse:obj];
             [arrayBLDetails addObject:boutiques];            }
            
          }
          // }
          [tableBoutique reloadData];
        }
        
    break;
      }
        default:
        break;
    }
  
  }
  else
  {
    ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  [self remove_Loader];
}



- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
  
  
  CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
  latitude = location.coordinate.latitude;
  longitude = location.coordinate.longitude;
  
}

-(void)callingAllData{
  if ([CLLocationManager locationServicesEnabled]) {
    switch ([CLLocationManager authorizationStatus]) {
      case kCLAuthorizationStatusAuthorized:
       [self sendDataToServerWithTag:TASK_GET_BOUTIQUES];
        break;
        
      default:{
       [self sendDataToServerWithTag:TASK_GET_BOUTIQUES];
        //[tableBoutique reloadData];
        
      }
        break;
    }
  }
}


//double haversine_km(double lat1, double long1, double lat2, double long2) {
//  double dlong = (long2 - long1) * d2r;
//  double dlat = (lat2 - lat1) * d2r;
//  double a = pow(sin(dlat/2.0), 2) + cos(lat1*d2r) * cos(lat2*d2r) * pow(sin(dlong/2.0), 2);
//  double c = 2 * atan2(sqrt(a), sqrt(1-a));
//  double distance = 6367 * c;
//  
//  return distance;
//}



@end
