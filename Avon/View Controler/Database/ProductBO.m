//
//  ProductBO.m
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "ProductBO.h"
#import "BaseBO.h"

@implementation ProductBO

-(ProductBO *)init{
  
  self = [super init];
  if (self) {
    self.itemId = @"";
    self.itemName = @"";
    self.itemFullImage = @"";
    self.itemThumbImage = @"";
    self.itemAvaragePrice = @"";
    self.itemDescription = @"";
    self.itemWeight = @"";
    self.quantity = @"";
  }
  return self;
}

-(ProductBO *)initWithResponse:(id)response{
   app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.itemId = [self checkNil:[response objectForKey:kID]];
    self.itemName = [self checkNil:[response objectForKey:kName]];
    self.itemThumbImage = [self checkNil:[response objectForKey:kTumb]];
    self.itemFullImage = [self checkNil:[response objectForKey:kImage]];
    self.itemAvaragePrice = [self checkNil:[response objectForKey:kPrice]];
    self.itemDescription = [self checkNil:[response objectForKey:kDescription]];
    self.itemWeight = [self checkNil:[response objectForKey:kWeight]];
    self.quantity = [self checkNil:[response objectForKey:kQuantity]];
    
  }
  
  return self;
}


@end
