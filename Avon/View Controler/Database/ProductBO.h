//
//  ProductBO.h
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"
#import "AppDelegate.h"

@interface ProductBO : BaseBO
{
  AppDelegate *app;
}
@property (nonatomic,strong)  NSString *itemId;
@property (nonatomic,strong)  NSString *itemName;
@property (nonatomic,strong)  NSString *itemFullImage;
@property (nonatomic,strong)  NSString *itemThumbImage;
@property (nonatomic,strong)  NSString *itemAvaragePrice;
@property (nonatomic,strong)  NSString *itemDescription;
@property (nonatomic,strong)  NSString *itemWeight;
@property (nonatomic,strong)  NSString *quantity;


-(ProductBO *)init;
-(ProductBO *)initWithResponse:(id)response;
@end
