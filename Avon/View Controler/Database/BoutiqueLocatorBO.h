//
//  BoutiqueLocatorBO.h
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface BoutiqueLocatorBO : BaseBO

@property (nonatomic,strong)  NSString *blId;
@property (nonatomic,strong)  NSString *blStoreName;
@property (nonatomic,strong)  NSString *blEmail;
@property (nonatomic,strong)  NSString *blStoreAddress;
@property (nonatomic,strong)  NSString *blStoreCity;
@property (nonatomic,strong)  NSString *blStoreState;
@property (nonatomic,strong)  NSString *blStoreCountry;
@property (nonatomic,strong)  NSString *blContactNo;
@property (nonatomic,strong)  NSString *blDistance;
@property (nonatomic,strong)  NSString *blLatitude;
@property (nonatomic,strong)  NSString *blLongitude;




-(BoutiqueLocatorBO *)init;
-(BoutiqueLocatorBO *)initWithResponse:(id)response;

@end
