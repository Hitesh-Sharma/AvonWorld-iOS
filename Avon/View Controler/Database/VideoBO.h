//
//  VideoBO.h
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface VideoBO : BaseBO

@property (nonatomic,strong) NSString *video_id;
@property (nonatomic,strong) NSString *video_url;
@property (nonatomic,strong) NSString *video_time;
@property (nonatomic,strong) NSString *video_thumb;
@property (nonatomic,strong) NSString *video_name;


-(VideoBO *)init;
-(VideoBO *)initWithResponse:(id)response;

@end
