//
//  PersentageBO.h
//  Avon
//
//  Created by Shipra Singh on 23/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface PersentageBO : BaseBO

@property (nonatomic,strong)  NSString *maximumPersentage;
@property (nonatomic,strong)  NSString *minimumPersentage;

-(PersentageBO *)init;
-(PersentageBO *)initWithResponse:(id)response;

@end
