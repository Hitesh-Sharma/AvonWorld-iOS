//
//  CurrentOfferBO.m
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "CurrentOfferBO.h"

@implementation CurrentOfferBO

-(CurrentOfferBO *)init{
  
  self = [super init];
  if (self) {
    self.offerId = @"";
    self.offerImage = @"";
    self.offerTittle = @"";
    self.offerDescreption = @"";
    self.offerStartDate = @"";
    self.offerEndDate = @"";

    
  }
  return self;
}

-(CurrentOfferBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.offerId = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kID]]];
    self.offerImage = [self checkNil:[response objectForKey:kImage]];
    self.offerTittle = [self checkNil:[response objectForKey:kTittle]];
    self.offerDescreption = [self checkNil:[response objectForKey:kDescription]];
    self.offerStartDate = [self checkNil:[response objectForKey:kOfferStartDate]];
    self.offerEndDate = [self checkNil:[response objectForKey:kOfferEndDate]];
    
  }
  
  return self;
}

@end
