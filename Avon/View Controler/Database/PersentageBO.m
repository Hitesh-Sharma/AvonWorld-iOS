//
//  PersentageBO.m
//  Avon
//
//  Created by Shipra Singh on 23/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "PersentageBO.h"

@implementation PersentageBO

-(PersentageBO *)init{
  
  self = [super init];
  if (self) {
    self.maximumPersentage = @"";
    self.minimumPersentage = @"";
    
  }
  return self;
}


-(PersentageBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.maximumPersentage = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kMaximumPersentage]]];
    self.minimumPersentage = [self checkNil:[response objectForKey:kMinimumPersentage]];
   
  }
  return self;
}

@end
