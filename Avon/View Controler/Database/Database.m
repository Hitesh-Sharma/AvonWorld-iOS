//
//  Database.m
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "Database.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@interface Database ()
{
  AppDelegate * app;
}
@property (strong, nonatomic) NSString *dataBasePath;

@end


static Database *sharedInstance = nil;
static sqlite3 *database = nil;


@implementation Database
static Database *instance;
@synthesize dataBasePath;


+(Database*)getSharedInstance{
  if (!sharedInstance) {
    sharedInstance = [[super allocWithZone:NULL]init];
    [sharedInstance createDB];
  }
  return sharedInstance;
}

-(BOOL)createDB{
  NSString *docsDir;
  NSArray *dirPaths;
  // Get the documents directory
  dirPaths = NSSearchPathForDirectoriesInDomains
  (NSDocumentDirectory, NSUserDomainMask, YES);
  docsDir = dirPaths[0];
  // Build the path to the database file
  databasePath = [[NSString alloc] initWithString:
                  [docsDir stringByAppendingPathComponent: @"avonMalaysia.db"]];
  BOOL isSuccess = YES;
  NSFileManager *filemgr = [NSFileManager defaultManager];
  if ([filemgr fileExistsAtPath: databasePath ] == NO)
  {
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
      
      char *errMsg;
      
     const char *sql_stmt = "create table if not exists messagetable (id INTEGER PRIMARY KEY AUTOINCREMENT, heading TEXT, message TEXT, time TEXT)";
      const char *sql_stmt_Country = "create table if not exists countrytable (id INTEGER PRIMARY KEY AUTOINCREMENT,countryId TEXT, countryName TEXT)";
      const char *sql_stmt_Category = "create table if not exists categorytable (id INTEGER PRIMARY KEY AUTOINCREMENT,categoryId TEXT, categoryName TEXT, totalSaving TEXT, countryId TEXT)";
      const char *sql_stmt_Product = "create table if not exists producttable (id INTEGER PRIMARY KEY AUTOINCREMENT,ids TEXT, name TEXT, description TEXT, weight TEXT,image TEXT, thumb TEXT, price TEXT, categoryId TEXT, countryId TEXT)";
       const char *sql_stmt_MinMax = "create table if not exists minmaxtable (id INTEGER PRIMARY KEY AUTOINCREMENT,maxpersent TEXT, minpersent TEXT, countryId TEXT)";
      
      if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
          != SQLITE_OK)
      {
        isSuccess = NO;
        NSLog(@"%s",sqlite3_errmsg(database));
        NSLog(@"Failed to create table");
      }
      if (sqlite3_exec(database, sql_stmt_Country, NULL, NULL, &errMsg)
          != SQLITE_OK)
      {
        isSuccess = NO;
        NSLog(@"%s",sqlite3_errmsg(database));
        NSLog(@"Failed to create table");
      }
      if (sqlite3_exec(database, sql_stmt_Category, NULL, NULL, &errMsg)
          != SQLITE_OK)
      {
        isSuccess = NO;
        NSLog(@"%s",sqlite3_errmsg(database));
        NSLog(@"Failed to create table");
      }
      if (sqlite3_exec(database, sql_stmt_Product, NULL, NULL, &errMsg)
          != SQLITE_OK)
      {
        isSuccess = NO;
        NSLog(@"%s",sqlite3_errmsg(database));
        NSLog(@"Failed to create table");
      }
      if (sqlite3_exec(database, sql_stmt_MinMax, NULL, NULL, &errMsg)
          != SQLITE_OK)
      {
        isSuccess = NO;
        NSLog(@"%s",sqlite3_errmsg(database));
        NSLog(@"Failed to create table");
      }

      
      sqlite3_close(database);
      return  isSuccess;
    }
    else {
      isSuccess = NO;
      NSLog(@"Failed to open/create database");
    }
  }
  return isSuccess;
}


-(void) closeDB:(sqlite3 *) db{
  sqlite3_close(db);
}

-(BOOL) storeMessage:(NotificationBO *) obj{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  BOOL result = YES;
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  //[NSString stringWithFormat:@"INSERT INTO report(patitentId, doctorName, reportName, viewReport) VALUES(? , ? , ? ,?)"];
  
  NSString *queryString = [NSString stringWithFormat:@"INSERT INTO messagetable(heading,message,time)VALUES(?,?,?)"];
//  NSString *time = [NSDateFormatter localizedStringFromDate:[NSDate date]
//                                                  dateStyle:NSDateFormatterShortStyle
//                                                  timeStyle:NSDateFormatterFullStyle];
  
  if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
  }
  if(sqlite3_open([databasePath UTF8String], &db) == SQLITE_OK){
    const char* query = [queryString UTF8String];
    
    
    if(sqlite3_prepare_v2(db, query, -1, &sql_statement, NULL) == SQLITE_OK){
      
      sqlite3_bind_text(sql_statement, 1, [obj.notificationTittle UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 2, [obj.notificationMessage UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 3, [obj.notificationTime UTF8String], -1, SQLITE_TRANSIENT);
      
    }
    
    
    
    if(SQLITE_DONE != sqlite3_step(sql_statement)){
      [self closeDB:db];
      result = NO;
    }
    
    sqlite3_reset(sql_statement);
    if (sql_statement) {
      sqlite3_finalize(sql_statement);
      sql_statement = nil;
    }
  }
  
  [self closeDB:db];
  return result;
}


-(BOOL) storeCountry:(CountryBO *) obj{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  BOOL result = YES;
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  
  NSString *queryString = [NSString stringWithFormat:@"INSERT INTO countrytable(countryId,countryName)VALUES(?,?)"];
  
  if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
  }
  if(sqlite3_open([databasePath UTF8String], &db) == SQLITE_OK){
    const char* query = [queryString UTF8String];
    
    
    if(sqlite3_prepare_v2(db, query, -1, &sql_statement, NULL) == SQLITE_OK){
      
      sqlite3_bind_text(sql_statement, 1, [obj.Ids UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 2, [obj.countryName UTF8String], -1, SQLITE_TRANSIENT);
         }
    
    
    
    if(SQLITE_DONE != sqlite3_step(sql_statement)){
      [self closeDB:db];
      result = NO;
    }
    
    sqlite3_reset(sql_statement);
    if (sql_statement) {
      sqlite3_finalize(sql_statement);
      sql_statement = nil;
    }
  }
  
  [self closeDB:db];
  return result;
}


-(BOOL) storeCategory:(CategoriesBO *) obj{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  BOOL result = YES;
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  
  NSString *queryString = [NSString stringWithFormat:@"INSERT INTO categorytable(categoryId,categoryName,totalSaving,countryId)VALUES(?,?,?,?)"];
  
  if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
  }
  if(sqlite3_open([databasePath UTF8String], &db) == SQLITE_OK){
    const char* query = [queryString UTF8String];
    
    
    if(sqlite3_prepare_v2(db, query, -1, &sql_statement, NULL) == SQLITE_OK){
      
      sqlite3_bind_text(sql_statement, 1, [obj.categoryId UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 2, [obj.categoryName UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 3, [obj.categoryNameTotalSaving UTF8String], -1, SQLITE_TRANSIENT);
       sqlite3_bind_text(sql_statement, 4, [obj.countryId UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    
    
    if(SQLITE_DONE != sqlite3_step(sql_statement)){
      [self closeDB:db];
      result = NO;
    }
    
    sqlite3_reset(sql_statement);
    if (sql_statement) {
      sqlite3_finalize(sql_statement);
      sql_statement = nil;
    }
  }
  
  [self closeDB:db];
  return result;
}


-(BOOL) storeProduct:(ProductBO *) obj andCountryId:(NSString*)countryId withCategoryId:(NSString*)categoryId{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  BOOL result = YES;
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  
  NSString *queryString = [NSString stringWithFormat:@"INSERT INTO producttable(ids,name,description,weight,image,thumb,price,categoryId,countryId)VALUES(?,?,?,?,?,?,?,?,?)"];
  
  if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
  }
  if(sqlite3_open([databasePath UTF8String], &db) == SQLITE_OK){
    const char* query = [queryString UTF8String];
    
    
    if(sqlite3_prepare_v2(db, query, -1, &sql_statement, NULL) == SQLITE_OK){
      
      sqlite3_bind_text(sql_statement, 1, [obj.itemId UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 2, [obj.itemName UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 3, [obj.itemDescription UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 4, [obj.itemWeight UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 5, [obj.itemFullImage UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 6, [obj.itemThumbImage UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 7, [obj.itemAvaragePrice UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 8, [categoryId UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 9, [countryId UTF8String], -1, SQLITE_TRANSIENT);


    }
    
    
    
    if(SQLITE_DONE != sqlite3_step(sql_statement)){
      [self closeDB:db];
      result = NO;
    }
    
    sqlite3_reset(sql_statement);
    if (sql_statement) {
      sqlite3_finalize(sql_statement);
      sql_statement = nil;
    }
  }
  
  [self closeDB:db];
  return result;
}


-(BOOL) storeMinMax:(PersentageBO *) obj withCountryId:(NSString*)countryId{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  BOOL result = YES;
  app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
  
  NSString *queryString = [NSString stringWithFormat:@"INSERT INTO minmaxtable(maxpersent,minpersent,countryId )VALUES(?,?,?)"];
  
  if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
  }
  if(sqlite3_open([databasePath UTF8String], &db) == SQLITE_OK){
    const char* query = [queryString UTF8String];
    
    
    if(sqlite3_prepare_v2(db, query, -1, &sql_statement, NULL) == SQLITE_OK){
      
      sqlite3_bind_text(sql_statement, 1, [obj.maximumPersentage UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 2, [obj.minimumPersentage UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(sql_statement, 3, [countryId UTF8String], -1, SQLITE_TRANSIENT);
    }
    
    
    
    if(SQLITE_DONE != sqlite3_step(sql_statement)){
      [self closeDB:db];
      result = NO;
    }
    
    sqlite3_reset(sql_statement);
    if (sql_statement) {
      sqlite3_finalize(sql_statement);
      sql_statement = nil;
    }
  }
  
  [self closeDB:db];
  return result;
}



-(NSMutableArray*) getMessage
{
 
    NSMutableArray *identities = [[NSMutableArray alloc] init];
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  // sqlite3_open([databasePath UTF8String], &db);
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  
  NSString *query = [NSString stringWithFormat:@" SELECT * FROM messagetable ORDER BY time DESC"];
  const char *query_stmt = [query UTF8String];
  
  if(sqlite3_prepare_v2(db, query_stmt, -1, &sql_statement, NULL) != SQLITE_OK){
  [self closeDB:db];
    return NO;
  }
  NotificationBO* notify;
  while(sqlite3_step(sql_statement) == SQLITE_ROW){
    notify = [[NotificationBO alloc]init];
    notify.notificationTittle = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 1)];
     notify.notificationMessage = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 2)];
     notify.notificationTime = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 3)];
    
    [identities addObject:notify];
    
  }
  sqlite3_reset(sql_statement);
  if (sql_statement) {
    sqlite3_finalize(sql_statement);
    sql_statement = nil;
  }
  
  [self closeDB:db];
  return identities;
}


-(NSMutableArray*) getCountries
{
  
  NSMutableArray *identities = [[NSMutableArray alloc] init];
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  // sqlite3_open([databasePath UTF8String], &db);
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  
  NSString *query = [NSString stringWithFormat:@" SELECT * FROM countrytable"];
  const char *query_stmt = [query UTF8String];
  
  if(sqlite3_prepare_v2(db, query_stmt, -1, &sql_statement, NULL) != SQLITE_OK){
    [self closeDB:db];
    return NO;
  }
  CountryBO* country;
  while(sqlite3_step(sql_statement) == SQLITE_ROW){
    country = [[CountryBO alloc]init];
    country.Ids = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 1)];
    country.countryName = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 2)];
    
    [identities addObject:country];
    
  }
  sqlite3_reset(sql_statement);
  if (sql_statement) {
    sqlite3_finalize(sql_statement);
    sql_statement = nil;
  }
  
  [self closeDB:db];
  return identities;
}


-(NSMutableArray*) getCategories:(NSString*)countryId
{
  
  NSMutableArray *identities = [[NSMutableArray alloc] init];
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  // sqlite3_open([databasePath UTF8String], &db);
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  NSString *query;
  
 query = [NSString stringWithFormat:@" SELECT * FROM categorytable where countryId ='%@'",countryId];
  
  const char *query_stmt = [query UTF8String];
  
  if(sqlite3_prepare_v2(db, query_stmt, -1, &sql_statement, NULL) != SQLITE_OK){
    [self closeDB:db];
    return NO;
  }
  CategoriesBO* categories;
  while(sqlite3_step(sql_statement) == SQLITE_ROW){
    categories = [[CategoriesBO alloc]init];
    categories.categoryId = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 1)];
    categories.categoryName = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 2)];
    categories.categoryNameTotalSaving = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 3)];
    categories.countryId = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 4)];
    
    [identities addObject:categories];
    
  }
  sqlite3_reset(sql_statement);
  if (sql_statement) {
    sqlite3_finalize(sql_statement);
    sql_statement = nil;
  }
  
  [self closeDB:db];
  return identities;
}


-(NSMutableArray*) getProduct:(NSString*)countryId withcategoryId:(NSString*)categoryId
{
  
  NSMutableArray *identities = [[NSMutableArray alloc] init];
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  // sqlite3_open([databasePath UTF8String], &db);
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  NSString *query;
  if ( [categoryId isEqualToString:@""]) {
    query = [NSString stringWithFormat:@" SELECT * FROM producttable where countryId ='%@'",countryId];
  }else{
 query = [NSString stringWithFormat:@" SELECT * FROM producttable where countryId ='%@' AND categoryId ='%@'",countryId,categoryId];
  }
  const char *query_stmt = [query UTF8String];
  
  if(sqlite3_prepare_v2(db, query_stmt, -1, &sql_statement, NULL) != SQLITE_OK){
    [self closeDB:db];
    return NO;
  }
  ProductBO* product;
  while(sqlite3_step(sql_statement) == SQLITE_ROW){
    product = [[ProductBO alloc]init];
    product.itemId = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 1)];
    product.itemName = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 2)];
    product.itemDescription = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 3)];
    product.itemWeight = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 4)];
    product.itemFullImage = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 5)];
    product.itemThumbImage = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 6)];
    product.itemAvaragePrice = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 7)];

    
    [identities addObject:product];
    
  }
  sqlite3_reset(sql_statement);
  if (sql_statement) {
    sqlite3_finalize(sql_statement);
    sql_statement = nil;
  }
  
  [self closeDB:db];
  return identities;
}

-(NSMutableArray*) getMinMax:(NSString*)countryId
{
  
  NSMutableArray *identities = [[NSMutableArray alloc] init];
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  // sqlite3_open([databasePath UTF8String], &db);
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  
  NSString *query = [NSString stringWithFormat:@" SELECT * FROM minmaxtable where countryId ='%@'",countryId];
  const char *query_stmt = [query UTF8String];
  
  if(sqlite3_prepare_v2(db, query_stmt, -1, &sql_statement, NULL) != SQLITE_OK){
    [self closeDB:db];
    return NO;
  }
  PersentageBO* persentage;
  while(sqlite3_step(sql_statement) == SQLITE_ROW){
    persentage = [[PersentageBO alloc]init];
    persentage.maximumPersentage = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 1)];
    persentage.minimumPersentage = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(sql_statement, 2)];
    
    [identities addObject:persentage];
    
  }
  sqlite3_reset(sql_statement);
  if (sql_statement) {
    sqlite3_finalize(sql_statement);
    sql_statement = nil;
  }
  
  [self closeDB:db];
  return identities;
}


-(void)deleteMessage:(NSString *)time{
  sqlite3 *db;
  sqlite3_stmt *sql_statement = nil;
  if(sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK){
    NSLog(@"%@",@"databasse open");
    
  }
  
  if (sqlite3_open([databasePath UTF8String], & db) == SQLITE_OK)
  {
    
  }
  
  NSString *deleteStatementNS = [NSString stringWithFormat:
                                 @"delete from messagetable where time = '%@'",
                                 time];
  
  const char *sql = [deleteStatementNS UTF8String];
  
  //const char *sql ="delete from tbl_todo where type = yourDbfildeRecored";
  if(sqlite3_prepare_v2(db, sql, -1, &sql_statement, NULL) != SQLITE_OK)
    /// NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
    
    //When binding parameters, index starts from 1 and not zero.
    sqlite3_bind_text(sql_statement, 1, [time UTF8String], -1, SQLITE_TRANSIENT);
  if (SQLITE_DONE != sqlite3_step(sql_statement))
    NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
  
  sqlite3_reset(sql_statement);
  [self closeDB:db];
}

@end
