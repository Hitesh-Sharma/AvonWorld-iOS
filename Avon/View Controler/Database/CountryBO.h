//
//  CountryBO.h
//  Avon
//
//  Created by Shipra Singh on 16/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface CountryBO : BaseBO

@property (nonatomic,strong) NSString *Ids;
@property (nonatomic,strong) NSString *countryName;

-(CountryBO *)init;
-(CountryBO *)initWithResponse:(id)response;

@end
