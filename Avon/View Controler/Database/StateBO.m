//
//  StateBO.m
//  Avon
//
//  Created by Shipra Singh on 04/09/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "StateBO.h"

@implementation StateBO

-(StateBO *)init{
  
  self = [super init];
  if (self) {
    self.Ids = @"";
    self.stateName = @"";
    
  }
  return self;
}


-(StateBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.Ids = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kID]]];
    self.stateName = [self checkNil:[response objectForKey:kName]];
    
    }
    return self;
}

@end
