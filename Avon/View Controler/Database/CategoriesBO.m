//
//  CategoriesBO.m
//  Avon
//
//  Created by Shipra Singh on 19/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "CategoriesBO.h"
#import "ProductBO.h"
#import "Database.h"
#import "UIImageView+WebCache.h"
@implementation CategoriesBO

-(CategoriesBO *)init{
  
  self = [super init];
  if (self) {
    self.categoryId = @"";
    self.categoryName = @"";
    self.countryId = @"";
    self.categoryNameTotalSaving = @"";
    self.isLodingFinish = @"";
  }
  return self;
}


-(CategoriesBO *)initWithResponse:(id)response product:(NSDictionary*)productResponse{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.categoryId = [self checkNil:[response objectForKey:kID]];
    self.categoryName = [self checkNil:[response objectForKey:kName]];
    self.countryId = [self checkNil:[response objectForKey:kCountry]];
    self.categoryNameTotalSaving = [self checkNil:[response objectForKey:kTottalSavings]];
    self.totalProductPriceCount = [self checkNil:[response objectForKey:kTotalQuantity]];
    self.isLodingFinish = [self checkNil:[response objectForKey:kTotalQuantity]];
    if ([productResponse valueForKey:self.categoryId]>0) {
      [self doParsingParseContent:[productResponse valueForKey:self.categoryId] withCAtegoryId:self.categoryId];
    }
  }
  return self;
}

-(void)doParsingParseContent:(NSArray *)arrayTemp withCAtegoryId:(NSString*)categoryId{
  if (self.productArray == nil) {
    self.productArray=[[NSMutableArray alloc] init];
  }
  else{
    [self.productArray removeAllObjects];
  }
  for (int i=0; i<[arrayTemp count]; i++) {
    ProductBO *objTemp=[[ProductBO alloc] initWithResponse:[arrayTemp objectAtIndex:i]];
   
    NSString* countryID = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
    NSMutableArray* arrayAllCategoriesDatabase = [[NSMutableArray alloc]init];
    arrayAllCategoriesDatabase = [[Database getSharedInstance]getProduct:countryID withcategoryId:categoryId];
   
  //  if ([countryID isEqualToString:@"1"]) {
      
      
      UIImageView* im = [[UIImageView alloc]init];
      [im sd_setImageWithURL:[NSURL URLWithString:objTemp.itemThumbImage]];
      
      UIImageView* ims = [[UIImageView alloc]init];
      [ims sd_setImageWithURL:[NSURL URLWithString:objTemp.itemFullImage]];
 
      
    if ([arrayAllCategoriesDatabase count] == 0) {
       [[Database getSharedInstance]storeProduct:objTemp andCountryId:countryID withCategoryId:categoryId];
    }else{
      int count = 0;
    for ( ProductBO *obj in  arrayAllCategoriesDatabase) {
      if ([objTemp.itemId isEqualToString:obj.itemId]) {
        count++;
      }
    }
      if (count == 0) {
        [[Database getSharedInstance]storeProduct:objTemp andCountryId:countryID withCategoryId:categoryId];
      }
    }
   // }
    
    [self.productArray addObject:objTemp];
  }
}

@end
