//
//  CategoriesBO.h
//  Avon
//
//  Created by Shipra Singh on 19/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface CategoriesBO : BaseBO

@property (nonatomic,strong)  NSString *categoryId;
@property (nonatomic,strong)  NSString *categoryName;
@property (nonatomic,strong)  NSString *countryId;
@property (nonatomic,strong)  NSString *categoryNameTotalSaving;
@property (nonatomic,strong)  NSString *totalProductPriceCount;
@property (nonatomic,strong)  NSMutableArray *productArray;
@property (nonatomic,strong)  NSString *isLodingFinish;

-(CategoriesBO *)init;
-(CategoriesBO *)initWithResponse:(id)response product:(NSDictionary*)productResponse;
-(void)doParsingParseContent:(NSArray *)arrayTemp withCAtegoryId:(NSString*)categoryId;
@end
