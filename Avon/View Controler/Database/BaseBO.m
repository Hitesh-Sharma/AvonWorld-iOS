//
//  BaseBO.m
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "BaseBO.h"

@implementation BaseBO

-(id)init
{
  self = [super init];
  return self;
}

-(NSString *)checkNil:(NSString *)string
{
  if (![string isKindOfClass:[NSNull class]]  ) {
    return string;
  }else{
    return @"0";
  }
}


@end
