//
//  BaseBO.h
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseBO : NSObject

-(NSString *)checkNil:(NSString *)string;
-(id)init;

@end
