//
//  Database.h
//  Avon
//
//  Created by Shipra Singh on 11/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotificationBO.h"
#import "ProductBO.h"
#import "CountryBO.h"
#import "CategoriesBO.h"
#import "PersentageBO.h"


@interface Database : NSObject
{
  NSString *databasePath;
}
+(Database *)getSharedInstance;
-(BOOL)createDB;
-(BOOL) storeMessage:(NotificationBO *) obj;
-(BOOL) storeCountry:(CountryBO *) obj;
-(BOOL) storeProduct:(ProductBO *) obj andCountryId:(NSString*)countryId withCategoryId:(NSString*)categoryId;
-(BOOL) storeCategory:(CategoriesBO *) obj;
-(BOOL) storeMinMax:(PersentageBO *) obj withCountryId:(NSString*)countryId;
-(NSMutableArray*) getMessage;
-(NSMutableArray*) getCountries;
-(NSMutableArray*) getCategories:(NSString*)countryId;
-(NSMutableArray*) getProduct:(NSString*)countryId withcategoryId:(NSString*)categoryId;
-(NSMutableArray*) getMinMax:(NSString*)countryId;
-(void)deleteMessage:(NSString *)time;
@end
