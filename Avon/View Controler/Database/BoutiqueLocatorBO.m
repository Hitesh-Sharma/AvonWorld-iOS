//
//  BoutiqueLocatorBO.m
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "BoutiqueLocatorBO.h"

@implementation BoutiqueLocatorBO

-(BoutiqueLocatorBO *)init{
  
  self = [super init];
  if (self) {
    self.blId = @"";
    self.blStoreName = @"";
    self.blStoreState = @"";
    self.blStoreCity = @"";
    self.blStoreAddress = @"";
    self.blContactNo = @"";
    self.blLatitude = @"";
    self.blLongitude = @"";
    self.blEmail = @"";
    self.blDistance = @"";
  }
  return self;
}

-(BoutiqueLocatorBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.blId = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kID]]];
    self.blStoreName = [self checkNil:[response objectForKey:kName]];
    self.blStoreState = [self checkNil:[response objectForKey:kState]];
    self.blStoreCity = [self checkNil:[response objectForKey:kCity]];
    self.blStoreAddress = [self checkNil:[response objectForKey:kAddress]];
    self.blContactNo = [self checkNil:[response objectForKey:kContactNo]];
    self.blLatitude = [self checkNil:[response objectForKey:kBLLatitude]];
    self.blLongitude = [self checkNil:[response objectForKey:kBLLongitude]];
    self.blEmail = [self checkNil:[response objectForKey:kBLEmail]];
    self.blDistance = [self checkNil:[response objectForKey:kDistance]];
    
  }
  
  return self;
}


@end
