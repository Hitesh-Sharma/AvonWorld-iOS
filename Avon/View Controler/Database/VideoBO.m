//
//  VideoBO.m
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "VideoBO.h"

@implementation VideoBO

-(VideoBO *)init{
  
  self = [super init];
  if (self) {
    self.video_id = @"";
    self.video_url = @"";
    self.video_time = @"";
    self.video_thumb = @"";
    self.video_name = @"";
    }
  return self;
}



-(VideoBO *)initWithResponse:(id)response{
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.video_id = [self checkNil:[NSString stringWithFormat:@"%@",[response valueForKey:kID]]];
      self.video_url = [self checkNil:[response valueForKey:kVideoUrl]];
    self.video_time = [self checkNil:[response valueForKey:kVideoTime]];
    self.video_thumb = [self checkNil:[response valueForKey:kTumb]];
    self.video_name = [self checkNil:[response valueForKey:kTittle]];
   
    
  }
  return self;
}

@end
