//
//  CurrentOfferBO.h
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface CurrentOfferBO : BaseBO

@property (nonatomic,strong) NSString *offerId;
@property (nonatomic,strong) NSString *offerImage;
@property (nonatomic,strong) NSString *offerTittle;
@property (nonatomic,strong) NSString *offerDescreption;
@property (nonatomic,strong) NSString *offerStartDate;
@property (nonatomic,strong) NSString *offerEndDate;

-(CurrentOfferBO *)init;
-(CurrentOfferBO *)initWithResponse:(id)response;

@end
