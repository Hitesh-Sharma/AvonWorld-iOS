//
//  CountryBO.m
//  Avon
//
//  Created by Shipra Singh on 16/10/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "CountryBO.h"

@implementation CountryBO
-(CountryBO *)init{
  
  self = [super init];
  if (self) {
    self.Ids = @"";
    self.countryName = @"";
   
    
  }
  return self;
}


-(CountryBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.Ids = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kID]]];
    self.countryName = [self checkNil:[response objectForKey:kName]];
    
  }
  return self;
}

@end
