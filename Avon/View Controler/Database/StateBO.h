//
//  StateBO.h
//  Avon
//
//  Created by Shipra Singh on 04/09/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBO.h"

@interface StateBO : BaseBO

@property (nonatomic,strong) NSString *Ids;
@property (nonatomic,strong) NSString *stateName;


-(StateBO *)init;
-(StateBO *)initWithResponse:(id)response;

@end
