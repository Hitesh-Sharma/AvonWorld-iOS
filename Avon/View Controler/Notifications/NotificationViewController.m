//
//  NotificationViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "Database.h"

@interface NotificationViewController ()
{
  
  IBOutlet UIView *backGroundViewForUndo;
  IBOutlet UITableView *notificationTable;
  NSMutableArray *text;
   NSMutableArray *textHeding;
  NSMutableArray* messageExist;
  IBOutlet UILabel *undoLabel;
  IBOutlet UIImageView *imageUndo;
  IBOutlet UIView *secondViewForUndo;
  IBOutlet UIButton *buttonUndo;
  IBOutlet UILabel *labelHedlineForUndo;
  IBOutlet UILabel *labelLineSeprater;
  BOOL isDelete;
  NSIndexPath *indexPaths;
  NSString* dateCheck;
  NotificationBO* object;
  NSTimer *timerUndo;
}
@property (nonatomic) CGFloat offsetRadio;
@end

@implementation NotificationViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self customView];
  
  // get all notification from database
  messageExist = [[NSMutableArray alloc]init];
  messageExist =  [[Database getSharedInstance]getMessage];
  undoLabel.text = LocalizedString(@"Undo");
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"Notifications") :@"":@"":@""];

  }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

#pragma mark - Customize screen

-(void)customView{
  int i = 0;
  int lowerHeight;
  int buttonHeight;
  if (IS_IPAD) {
    lowerHeight = (55 * 500)/100;
    buttonHeight = (20 * lowerHeight)/100;
    i = 30;
   
  }else{
    lowerHeight = (55 * kScreenWidth)/100;
    buttonHeight = (20 * lowerHeight)/100;
  }

  notificationTable.frame = CGRectMake(0, 60, kScreenWidth, kScreenHeight - 60);
  notificationTable.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
  notificationTable.separatorColor = [UIColor clearColor];
  
  
  secondViewForUndo.frame = CGRectMake(10 + i, kScreenHeight - buttonHeight - 40, kScreenWidth - 20 - 2*i, buttonHeight + 10);
  backGroundViewForUndo.frame = CGRectMake(0, 0, kScreenWidth - 20 - 2*i, buttonHeight + 10);
  labelHedlineForUndo.frame = CGRectMake(5, 3, kScreenWidth - 110 - 2*i, secondViewForUndo.frame.size.height - 6);
  buttonUndo.frame = CGRectMake(kScreenWidth - 90 - 2*i , 0, 90, secondViewForUndo.frame.size.height);
  imageUndo.frame = CGRectMake(kScreenWidth - 88 - 2*i , 0, 20 , secondViewForUndo.frame.size.height);
   undoLabel.frame = CGRectMake(kScreenWidth - 70 - 2*i, 0,  50 , secondViewForUndo.frame.size.height);
  labelLineSeprater.frame = CGRectMake(kScreenWidth - 96 - 2*i, 9, 1, buttonUndo.frame.size.height - 18);
  
  [self roundCornersOfView:secondViewForUndo radius:(buttonHeight + 10)/7 borderColor:[UIColor clearColor] borderWidth:0];
  
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  
    return [messageExist count];
 
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, notificationTable.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  int i = 0;
  float heightForTime = 37;
  if (IS_IPAD) {
    i = 30;
    heightForTime = 45;
  }

  if (indexPath.row == 0) {
    dateCheck = @"";
  }
  NotificationBO* obj = [messageExist objectAtIndex:indexPath.row];
  
  NSDateFormatter *formatOld = [[NSDateFormatter alloc] init];
  [formatOld setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
  NSDate *date = [formatOld dateFromString:obj.notificationTime];
  NSDateFormatter *newFormate = [[NSDateFormatter alloc]init];
  [newFormate setDateFormat:@"MMM-dd-yyyy"];
  NSString *newdate =[newFormate stringFromDate:date];
  
  NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:obj.notificationMessage attributes:@
                                        {
                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:16 + i/5]
                                        }];
  CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - 20 - i/4 , CGFLOAT_MAX}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil];
  CGSize size = rect.size;
  CGFloat height = size.height;
  
  if ([newdate isEqualToString:dateCheck]) {
    return (height + heightForTime  + i/3 + 45);
  }else{
        dateCheck = newdate;
    return (height + heightForTime  + i/3 + 65);
  }
  
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int i = 0;
  float heightForTime = 37;
  if (IS_IPAD) {
    i = 30;
    heightForTime = 45;
  }
  
  if (indexPath.row == 0) {
    dateCheck = @"";
  }
  
  
  NotificationBO* obj = [messageExist objectAtIndex:indexPath.row];
  
  NSDateFormatter *formatOld = [[NSDateFormatter alloc] init];
  [formatOld setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   NSDate *date = [formatOld dateFromString:obj.notificationTime];
  NSDateFormatter *newFormate = [[NSDateFormatter alloc]init];
  [newFormate setDateFormat:@"MMM-dd-yyyy"];
  NSString *newdate =[newFormate stringFromDate:date];
  NSDateFormatter *newFormate1 = [[NSDateFormatter alloc]init];
  [newFormate1 setDateFormat:@"hh:mm a"];
  NSString *newtime =[newFormate1 stringFromDate:date];
  
  
  NSDate *today = [NSDate date];
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"MMM-dd-yyyy"];
   NSString *todayDate = [dateFormatter stringFromDate:today];
  
  NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
  NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
 [offsetComponents setDay:-1];
   NSDate *datetime = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
  
  NSString *yesterDate = [dateFormatter stringFromDate:datetime];
  
  NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:obj.notificationMessage attributes:@
                                        {
                                        NSFontAttributeName: [UIFont fontWithName:kFontHelvaticaLight size:16 + i/5]
                                        }];
  CGRect rect = [attributedText boundingRectWithSize:(CGSize){kScreenWidth - 20 - i/4 , CGFLOAT_MAX}
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                             context:nil];
  CGSize size = rect.size;
  CGFloat height = size.height;
  CGFloat height1 = height;
  CGFloat aHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
  if ( aHeight == (height + heightForTime  + i/3 + 45)) {
  height = height + heightForTime  + i/3 + 45;
  }else{
    dateCheck = newdate;
    height = height + heightForTime  + i/3 + 65;
  }

  
    NSString *simpleTableIdentifier = @"NotificationTableViewCell";
    NotificationTableViewCell *cell = (NotificationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *  nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationTableViewCell" owner:self options:nil];;
    cell = [[NotificationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    cell = [nib objectAtIndex:0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
      
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.frame = CGRectMake(0, 0, kScreenWidth, height );
    cell.baseViewOfScroll.frame = CGRectMake(10, 0, kScreenWidth - 20, height );
    cell.scrollView.frame = CGRectMake(0, 0, kScreenWidth - 20, height );
    cell.scrollView.tag = indexPath.row;
    cell.labelHeding.tag = indexPath.row + 1000;
    cell.labelNotification.tag = indexPath.row + 2000;
    cell.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
 

    cell.scrollView.contentSize = CGSizeMake(CGRectGetWidth(cell.scrollView.frame) * 2, 0);
  
  if (height ==  (height1 + heightForTime  + i/3 + 65)) {
    
    cell.labelDate.hidden = NO;
    cell.labelDate.frame = CGRectMake(0, 10, kScreenWidth - 20, 25 + i/3);
    cell.labelHeding.frame = CGRectMake(0, 37 + i/3, kScreenWidth - 20, 25 + i/3);
    cell.labelNotification.frame = CGRectMake(0, 62 + 2*i/3, kScreenWidth - 20, size.height + 10);
  
  }else{
   
    cell.labelDate.hidden = YES;
    cell.labelHeding.frame = CGRectMake(0, 10, kScreenWidth - 20, 25 + i/3);
    cell.labelNotification.frame = CGRectMake(0, 35 + i/3, kScreenWidth - 20, size.height + 10);
 
  }
  
  cell.labelTime.frame = CGRectMake(kScreenWidth - 95 - i/2,height - (25 + i/3) - 10 , 70 + i/2, 25 + i/3);
  cell.lableLine.frame = CGRectMake(0, height - 10, kScreenWidth + 10, 10);
  cell.clockImage.frame = CGRectMake(kScreenWidth - 106 - i/2,height - (25 + i/3) - 9 , 11 , 25 + i/3);
  cell.lableLine.backgroundColor = [UIColor clearColor];
  cell.lableLine.text = @"- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
  cell.lableLine.textColor = [UIColor blackColor];
  cell.lableLine.font = [UIFont fontWithName:kFontHelvaticaLight size:5 + i/12];
  cell.labelNotification.text = obj.notificationMessage;
  cell.labelHeding.text = obj.notificationTittle;

  if ([todayDate isEqualToString:newdate]) {
    
    cell.labelDate.text = LocalizedString(@"Today");
  
  }else if ([yesterDate isEqualToString:newdate]){
   
    cell.labelDate.text = LocalizedString(@"Yesterday");
  
  }else{
  
    cell.labelDate.text = newdate;

  }
  
  cell.labelTime.text = newtime;
  cell.labelHeding.font = [UIFont fontWithName:kFontHelvaticaBoldItalic size:18 + i/5];
  cell.labelNotification.font = [UIFont fontWithName:kFontHelvaticaLight size:16 + i/5];
   cell.labelTime.font = [UIFont fontWithName:kFontHelvaticaLight size:14 + i/7];
  cell.labelDate.font = [UIFont fontWithName:kFontHelavaticaBold size:15 + i/7];
 
    return cell;
  
}

#pragma mark - scrollview methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
  CGFloat pageWidth = scrollView.contentOffset.x;
  NSInteger section = (scrollView.tag)/1000;
  NSInteger row = (scrollView.tag)%1000;
  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
  UITableViewCell *table = [notificationTable cellForRowAtIndexPath:indexPath];
  UILabel *label = (UILabel *)[table viewWithTag:row + 1000];
  UILabel *label2 = (UILabel *)[table viewWithTag:row + 2000];
  if (pageWidth > kScreenWidth/2) {
    isDelete = YES;
   
    label.textColor = [UIColor grayColor];
    label2.textColor = [UIColor grayColor];
  }else{
    isDelete = NO;
    label.textColor = [UIColor blackColor];
    label2.textColor = [UIColor blackColor];
  }
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollViews {
  if (scrollViews.tag != 33) {
  
  CGFloat pageWidth = scrollViews.contentOffset.x;
  NSLog(@"%f",pageWidth);
  NSInteger section = (scrollViews.tag)/1000;
  NSInteger row = (scrollViews.tag)%1000;
  indexPaths = [NSIndexPath indexPathForRow:row inSection:section];
 object = [[NotificationBO alloc]init];
  
  object = [messageExist objectAtIndex:indexPaths.row];
  if (scrollViews.tag != -1) {
    if (isDelete == YES) {
  [messageExist removeObjectAtIndex:indexPaths.row];
  [notificationTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPaths] withRowAnimation:UITableViewRowAnimationFade];
 // [notificationTable reloadData];
  labelHedlineForUndo.text = object.notificationTittle ;
  [self.view addSubview:secondViewForUndo];
      isDelete = NO;
 timerUndo = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(handleTimer:) userInfo:nil repeats:NO];
    }
    
  }}
}

#pragma mark - Button action for undo

- (IBAction)buttonUndoAction:(id)sender {
  [timerUndo invalidate];
  timerUndo = nil;
  messageExist = [[NSMutableArray alloc]init];
  messageExist =  [[Database getSharedInstance]getMessage];
  [notificationTable reloadData];
  [secondViewForUndo removeFromSuperview];

 }


//handle Timer
- (void) handleTimer:(NSTimer *)timer {
 
  [[Database getSharedInstance]deleteMessage:object.notificationTime];
  [secondViewForUndo removeFromSuperview];
 }

@end
