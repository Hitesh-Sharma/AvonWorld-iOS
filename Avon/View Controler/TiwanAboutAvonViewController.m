//
//  TiwanAboutAvonViewController.m
//  Avon
//
//  Created by Shipra Singh on 16/11/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "TiwanAboutAvonViewController.h"

@interface TiwanAboutAvonViewController ()

@end

@implementation TiwanAboutAvonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  [self designHeaderOfTheScreen:@"menu_icon.png" :LocalizedString(@"About Avon") :@"":@"":@""];
    // Do any additional setup after loading the view from its nib.
}

/*It takes back to menu screen*/
-(void)backButtonTap{
  [slideNavController toggleLeftMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
