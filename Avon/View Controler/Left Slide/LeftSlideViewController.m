//
//  LeftSlideViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "LeftSlideViewController.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "Constants.pch"
#import "HomeViewController.h"
#import "ProductViewController.h"
#import "BoutiqueLocatorViewController.h"
#import "VideoViewController.h"
#import "KeyStrengthsViewController.h"
#import "FeedBackViewController.h"
#import "NotificationViewController.h"
#import "CurrentOffersViewController.h"
#import "AppDelegate.h"
#import "NewKeyStrengthDetailViewController.h"
#import "NewMissionViewController.h"
#import "NewHistoryViewController.h"
#import "AllProductsViewController.h"
#import "TiwanAboutAvonViewController.h"

@interface LeftSlideViewController ()
{
    IBOutlet UITableView *leftTableView;
  NSMutableArray * arrayOfTitle;
  NSMutableArray * arrayExpendOfTitle;
  NSMutableArray * arrayOfTitleInEnglish;
  NSMutableArray * arrayExpendOfTitleInEnglish;
  SlideNavigationController *slideNavController;
  BOOL isExpendable;
   BOOL isMinimise;
  AppDelegate *app;
  IBOutlet UIImageView *imageLogo;
  NSIndexPath *lastIndexPath;
}
@end

@implementation LeftSlideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
  int i = 0;
  if (IS_IPAD) {
     i = 80;
    imageLogo.image = [UIImage imageNamed:@"avon_logo_ipad.png"];
  }
  if (kScreenHeight == 480) {
      leftTableView.frame = CGRectMake(0, 110 + 3* i/2, kScreenWidth, kScreenHeight - (20 + i));
  }else{
      leftTableView.frame = CGRectMake(0, 110 + 3* i/2, kScreenWidth, kScreenHeight - (110 + i));
  }

  imageLogo.frame = CGRectMake(0 , 30, kScreenWidth - kScreenWidth/4, 70 + i);
  leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//  self.view.backgroundColor=navigationcolorCode;
  leftTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  
  [self arryaAccordingToCountry];
   NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
  if (indexPath) {
    [leftTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
  }
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
  [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return [arrayOfTitle count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  // This will create a "invisible" footer
  return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftTableView.frame.size.width, 0)];
  view.backgroundColor = [UIColor clearColor];
  return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  int height = 40;
  if (IS_IPAD) {
    height = 65;
  }
  return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  int row = 3;
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
//  if ([countryName isEqualToString:@"Taiwan"] ) {
//    row = 2;
//  }

  int height = 40;
  int i = 0;
  if (IS_IPAD) {
    height = 65;
    i = 65;
  }
  static NSString *simpleTableIdentifier = @"SimpleTableItem";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
  
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
  }
  
  //remove view to avoid duplicacy
  for (UIView *v in cell.contentView.subviews) {
    if ([v isKindOfClass:[UIImageView class]]) {
      [v removeFromSuperview];
    }
    if ([v isKindOfClass:[UILabel class]]) {
      [v removeFromSuperview];
    }
  }
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  cell.contentView.frame = CGRectMake(0, 0, kScreenWidth - kScreenWidth/4, height );

  UILabel *tittleLabel = [[UILabel alloc]init];
  UILabel *lineLabel = [[UILabel alloc]init];
  lineLabel.frame = CGRectMake(0, cell.contentView.frame.size.height - 1, kScreenWidth, 1);
  tittleLabel.frame = CGRectMake(20, 8 , cell.contentView.frame.size.width/2 + cell.contentView.frame.size.width/4  , cell.contentView.frame.size.height - 16);
 tittleLabel.textColor=[UIColor blackColor];
  tittleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:18 + i/8];
  
  lineLabel.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
  tittleLabel.text =[arrayOfTitle objectAtIndex:indexPath.row];
  cell.backgroundColor=[UIColor clearColor];
  UIImageView *arrow=[[UIImageView alloc] initWithFrame:CGRectMake(cell.contentView.frame.size.width - 50, cell.contentView.frame.size.height/2 - 30/2, 30, 30)];
  arrow.image = [UIImage imageNamed:@"black_side_arrow.png"];
  arrow.contentMode = UIViewContentModeCenter;
  if (isExpendable == YES) {
    
    
    if (indexPath.row > row && indexPath.row < (row + 4)) {

      tittleLabel.frame = CGRectMake(35 + i/3, 8 , cell.contentView.frame.size.width/2 , cell.contentView.frame.size.height - 16);
      tittleLabel.font = [UIFont fontWithName:kFontTimesRomanItalic size:18 + i/8];
        arrow.hidden = YES;
      lineLabel.frame = CGRectMake(0, cell.contentView.frame.size.height - 7, kScreenWidth, 10);
        lineLabel.backgroundColor = [UIColor clearColor];
      lineLabel.text = @"............................................................................................................................................................................................................................................................................................................";
      lineLabel.font = [UIFont fontWithName:kFontHelvaticaLight size:5 + i/12];
      lineLabel.textColor = [UIColor blackColor];
     // lineLabel.textColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
      arrow.hidden = YES;
    }
    lineLabel.tag = indexPath.row;
    if (indexPath.row == row + 3) {
       lineLabel.frame = CGRectMake(0, cell.contentView.frame.size.height - 1, kScreenWidth, 1);
       lineLabel.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    }
    if (isMinimise == YES) {

    if (indexPath.row == row) {
      arrow.image = [UIImage imageNamed:@"black_side_arrow.png"];
      lineLabel.hidden = NO;
      arrow.frame = CGRectMake(cell.contentView.frame.size.width - 50, cell.contentView.frame.size.height/2 - 30/2, 30, 30);
    }
    }else{
      if (indexPath.row == row) {
        arrow.image = [UIImage imageNamed:@"black_down_arrow.png"];
        lineLabel.hidden = YES;
         arrow.frame = CGRectMake(cell.contentView.frame.size.width - 50, cell.contentView.frame.size.height/2 - 30/2, 30, 30);
      }
    }
  }
  [cell.contentView addSubview:lineLabel];
  [cell.contentView addSubview:tittleLabel];
  [cell.contentView addSubview:arrow];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSInteger newRow = [indexPath row];
  NSInteger oldRow = [lastIndexPath row];
  if (lastIndexPath == nil) {
    oldRow = -1;
  }
  NSString* selectedRowName = [arrayOfTitleInEnglish objectAtIndex:indexPath.row];
  NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  if (newRow != oldRow)
  {
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                                lastIndexPath];
      oldCell.contentView.backgroundColor = [UIColor whiteColor];
    
    }
  
  slideNavController = [SlideNavigationController sharedInstance];
  UIViewController *rootVC;
  
  if ([selectedRowName isEqualToString:@"About Avon"]) {
//    if ([countryName isEqualToString:@"Taiwan"]){
//      rootVC = [[TiwanAboutAvonViewController alloc]initWithNibName:@"TiwanAboutAvonViewController" bundle:nil];
//      
//    }else{
    if (isExpendable == YES) {
      
      [self miniMizeThisRows];
      return;
      
    }else{
      
      isExpendable = YES;
      isMinimise = NO;
      [leftTableView reloadData];
      NSUInteger count=indexPath.row;
      NSUInteger indexing = 0;
      NSUInteger countIndex;
      NSMutableArray *arCells=[NSMutableArray array];
      for(NSString * name in arrayExpendOfTitleInEnglish ) {
        countIndex = ++count;
        [arrayOfTitleInEnglish insertObject:name atIndex:countIndex];
       // indexing = indexing+1;
      }
      count=indexPath.row;
      for(NSString * name in arrayExpendOfTitle ) {
        countIndex = ++count;
        [arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
        [arrayOfTitle insertObject:name atIndex:countIndex];
        indexing = indexing+1;
      }
      [tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationLeft];
      return;
    }
   // }
  }
  else if ([selectedRowName isEqualToString:@"Products"]) {
    
    if ([countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
      [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kIsClickOnHome];
      [[NSUserDefaults standardUserDefaults]synchronize];
      rootVC = [[ProductViewController alloc]initWithNibName:@"ProductViewController" bundle:nil];
    }else{
      rootVC = [[AllProductsViewController alloc]initWithNibName:@"AllProductsViewController" bundle:nil];
    }
        
  }else if ([selectedRowName isEqualToString:@"Current Offers"]) {
    
     rootVC = [[CurrentOffersViewController alloc]initWithNibName:@"CurrentOffersViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Home"]) {
      
      rootVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
      
    }
  else if ([selectedRowName isEqualToString:@"Videos"]) {
    
    rootVC = [[VideoViewController alloc]initWithNibName:@"VideoViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Boutique Locator"]) {
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kisBoutiqueByButton];
    [[NSUserDefaults standardUserDefaults]synchronize];
    rootVC = [[BoutiqueLocatorViewController alloc]initWithNibName:@"BoutiqueLocatorViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Feedback"]) {
    if ([countryName isEqualToString:@"Taiwan"]){
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mshop.avon.com.tw/"]];
      return;
    }else{
    rootVC = [[FeedBackViewController alloc]initWithNibName:@"FeedBackViewController" bundle:nil];
    }
  }else if ([selectedRowName isEqualToString:@"Notifications"]) {
    
    rootVC = [[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Our History"]) {
    
    rootVC = [[NewHistoryViewController alloc]initWithNibName:@"NewHistoryViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Our Mission"]) {
    
    rootVC = [[NewMissionViewController alloc]initWithNibName:@"NewMissionViewController" bundle:nil];
    
  }else if ([selectedRowName isEqualToString:@"Key Strengths"]) {
    
    rootVC = [[NewKeyStrengthDetailViewController alloc]initWithNibName:@"NewKeyStrengthDetailViewController" bundle:nil];
    
  }
  

  UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
  for (UIView *v in newCell.contentView.subviews) {
    if ([v isKindOfClass:[UILabel class]]) {
     
    }
  }
  if (newRow != oldRow)
  {
    
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:
                                lastIndexPath];
    newCell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
    oldCell.contentView.backgroundColor = [UIColor whiteColor];
    
    lastIndexPath = indexPath;
  }else{
    newCell.contentView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
  }
    [slideNavController popAllAndSwitchToViewController:rootVC withSlideOutAnimation:NO andCompletion:nil];
  
}

-(void)miniMizeThisRows{
   NSUInteger indexing = 0;
  isMinimise = YES;
 [leftTableView reloadData];
  isExpendable = NO;
  
  for(NSString * name in arrayExpendOfTitle ) {
    NSUInteger indexToRemove=[arrayOfTitle indexOfObjectIdenticalTo:name];
    
    if([arrayOfTitle indexOfObjectIdenticalTo:name]!=NSNotFound) {
      [arrayOfTitle removeObjectIdenticalTo:name];
      
      [leftTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
                                             [NSIndexPath indexPathForRow:indexToRemove inSection:0]]withRowAnimation:UITableViewRowAnimationRight];
      indexing = indexing+1;
    }
  }
  
  for(NSString * name in arrayExpendOfTitleInEnglish ) {
    if([arrayOfTitleInEnglish indexOfObjectIdenticalTo:name]!=NSNotFound) {
      [arrayOfTitleInEnglish removeObjectIdenticalTo:name];
      indexing = indexing+1;
    }
  }
  
  
 
}


-(void)arryaAccordingToCountry{
   NSString* countryName = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionName];
  
  if ([countryName isEqualToString:@"India"] ) {
    
    arrayOfTitle=[[NSMutableArray alloc] initWithObjects:LocalizedString(@"Home"),@"The Beauty Store",LocalizedString(@"Current Offers"),LocalizedString(@"About Avon"),LocalizedString(@"Videos"),LocalizedString(@"Feedback"),LocalizedString(@"Notifications"), nil];
    
    arrayOfTitleInEnglish = [[NSMutableArray alloc] initWithObjects:@"Home",@"Products",@"Current Offers",@"About Avon",@"Videos",@"Feedback",@"Notifications",nil];
  
  }else  if ([countryName isEqualToString:@"Philippines"] || [countryName isEqualToString:@"Malaysia"] || [countryName isEqualToString:@"East Malaysia"] || [countryName isEqualToString:@"West Malaysia"]) {
    
  arrayOfTitle=[[NSMutableArray alloc] initWithObjects:LocalizedString(@"Home"),LocalizedString(@"Products"),LocalizedString(@"Current Offers"),LocalizedString(@"About Avon"),LocalizedString(@"Videos"),LocalizedString(@"Boutique Locator"),LocalizedString(@"Feedback"),LocalizedString(@"Notifications"), nil];
    
  arrayOfTitleInEnglish = [[NSMutableArray alloc] initWithObjects:@"Home",@"Products",@"Current Offers",@"About Avon",@"Videos",@"Boutique Locator",@"Feedback",@"Notifications",nil];
 
  
  }else if ([countryName isEqualToString:@"Taiwan"]){
    
    arrayOfTitle=[[NSMutableArray alloc] initWithObjects:LocalizedString(@"Home"),LocalizedString(@"Products"),LocalizedString(@"Current Offers"),LocalizedString(@"About Avon"),LocalizedString(@"Videos"),LocalizedString(@"Boutique Locator") ,LocalizedString(@"Feedback"),LocalizedString(@"Notifications"), nil];
    
    arrayOfTitleInEnglish = [[NSMutableArray alloc] initWithObjects:@"Home",@"Products",@"Current Offers",@"About Avon",@"Videos",@"Boutique Locator",@"Feedback",@"Notifications",nil];
  }
   arrayExpendOfTitle=[[NSMutableArray alloc] initWithObjects:LocalizedString(@"Our History"),LocalizedString(@"Our Mission"), LocalizedString(@"Our Strengths"), nil];
  
  arrayExpendOfTitleInEnglish = [[NSMutableArray alloc] initWithObjects:@"Our History",@"Our Mission",@"Key Strengths", nil];
  
  [leftTableView reloadData];
//  NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//  if (indexPath) {
//    [leftTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//  }


}

@end
