//
//  FeedBackViewController.m
//  Avon
//
//  Created by Shipra Singh on 29/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "FeedBackViewController.h"
#import "UIViewController+KeyboardAnimation.h"


@interface FeedBackViewController ()<ConnectionManager_Delegate>
{
  IBOutlet UILabel *labelMessage;
  IBOutlet UILabel *labelPhoneNo;
  IBOutlet UILabel *labelEmail;
  IBOutlet UITextView *textPhoneNumber;
  IBOutlet UITextView *textEmail;
  IBOutlet UITextView *textViewMessage;
  IBOutlet UIButton *buttonSubmit;
  int lowerHeight;
  int buttonHeight;
  int i;
  int heightOfKeyPad;
  NSString *strAlertMessage;
  ConnectionManager *requestManager;
}
@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  //connection Manager
  requestManager=[ConnectionManager sharedManager];
  requestManager.delegate=self;
 
  [self designHeaderOfTheScreen:@"menu_icon.png" : LocalizedString(@"Feedback") :@"":@"":@""];
  i = 0;
  
  [self subscribeToKeyboard];
  [self customView];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - customizeView

-(void)customView{
 
  if (IS_IPAD) {
    lowerHeight = (55 * 500)/100;
    buttonHeight = (20 * lowerHeight)/100;
    i = 30;
    textEmail.textContainerInset = UIEdgeInsetsMake(17, 10, 0, 0);
    textPhoneNumber.textContainerInset = UIEdgeInsetsMake(17, 10, 0, 0);
    textViewMessage.textContainerInset = UIEdgeInsetsMake(11, 10, 0, 0);

  }else{
    lowerHeight = (55 * kScreenWidth)/100;
    buttonHeight = (20 * lowerHeight)/100;
    if (kScreenHeight > 568) {
      textEmail.textContainerInset = UIEdgeInsetsMake(13, 10, 0, 0);
      textPhoneNumber.textContainerInset = UIEdgeInsetsMake(13, 10, 0, 0);

    }else{
      textEmail.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 0);
      textPhoneNumber.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 0);

    }
       textViewMessage.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 0);

  }
 
  textEmail.frame = CGRectMake(10 + i, 70, kScreenWidth - 2*(10 + i), buttonHeight);
  textPhoneNumber.frame = CGRectMake(10 +  i, 80 + buttonHeight, kScreenWidth - 2*(10 + i), buttonHeight);
  textViewMessage.frame = CGRectMake(10 + i, 100 + 2 * buttonHeight, kScreenWidth - 2*(10 + i), kScreenHeight - buttonHeight - 60 - i/6 - (100 + 2 * buttonHeight) );
  buttonSubmit.frame = CGRectMake(25 + i, textViewMessage.frame.origin.y + textViewMessage.frame.size.height + 20, kScreenWidth - 50 - 2*i, buttonHeight);
  labelMessage.frame = CGRectMake(25 + i, 100 + 2 * buttonHeight, 200, 35 + i/4);
  labelEmail.frame = CGRectMake(25 + i, 70, 200, buttonHeight);
  labelPhoneNo.frame = CGRectMake(25 + i, 80 + buttonHeight, 200, buttonHeight);
  
  [self roundCornersOfView:textEmail radius:buttonHeight/5 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0f] borderWidth:1];
  [self roundCornersOfView:textPhoneNumber radius:buttonHeight/5 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0f] borderWidth:1];
  [self roundCornersOfView:textViewMessage radius:buttonHeight/5 borderColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0f] borderWidth:1];
  [self roundCornersOfView:buttonSubmit radius:buttonHeight/8 borderColor:[UIColor clearColor] borderWidth:0];
  
  textEmail.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  textPhoneNumber.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  textViewMessage.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  labelMessage.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  labelEmail.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  labelPhoneNo.font = [UIFont fontWithName:kFontHelvaticaLight size:buttonHeight/3];
  buttonSubmit.titleLabel.font = [UIFont fontWithName:kFontTimesNewRomanBoldItalic size:buttonHeight/2];

  labelMessage.text = LocalizedString(@"Message");
  labelEmail.text = LocalizedString(@"Email");
  labelPhoneNo.text = LocalizedString(@"Phone Number");
  [buttonSubmit setTitle:LocalizedString(@"Submit") forState:UIControlStateNormal];
   
}


/*It takes back to menu screen*/
-(void)backButtonTap{
  [textEmail endEditing:YES];
  [textPhoneNumber endEditing:YES];
  [textViewMessage endEditing:YES];
  [slideNavController toggleLeftMenu];
}

#pragma mark - button click
- (IBAction)submitClick:(id)sender {
  
  textEmail.text = [textEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  textPhoneNumber.text = [textPhoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  textViewMessage.text = [textViewMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  
  if (![self checkBlankField]) {
    ALERT(nil, strAlertMessage);
    return;
  }
  if ([self validateText:textEmail.text withPattern:patternForEmail]==0) {
    ALERT(nil, mInvalidEmail);
    return;	}
  if (textPhoneNumber.text.length <= 6 || [self validateText:textPhoneNumber.text withPattern:patternForPhoneNumber]==0 ) {
    ALERT(nil, mContact);
    return;
  }
  Reachability* reachability = [Reachability reachabilityForInternetConnection];
  
  
  NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
  
  if(remoteHostStatus == NotReachable) {
   ALERT(@"", LocalizedString(@"Feedback Connection"));
  }else{
  [self sendDataToServerWithTag:TASK_FEEDBACK];
  }
}


#pragma mark - validation

-(int)validateText:(NSString*)string withPattern:(NSString*)pattern
{
  //case insensitive pattern matching
  NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
  int numberOfMatches = (int)[regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
  return numberOfMatches;
}


#pragma mark - Check Blank Field

-(BOOL)checkBlankField
{
  strAlertMessage = @"";
  BOOL result;
  NSInteger email,phone,message;
  
  email = textEmail.text.length;
  phone = textPhoneNumber.text.length;
  message = textViewMessage.text.length;
  
  
  if(email == 0 || phone == 0 || message == 0 ){
       if (email == 0){
      strAlertMessage = [strAlertMessage stringByAppendingString:@"\n- Email"];
    }
    if (phone == 0){
      strAlertMessage = [strAlertMessage stringByAppendingString:@"\n- Phone Number"];
    }
    
    if (message == 0) {
      strAlertMessage = [strAlertMessage stringByAppendingString:@"\n- Messaage"];
    }
    }
  
  if(email == 0 || phone == 0 || message == 0 ){
    result=NO;
  }else{
    result=YES;
  }
  return result;
}




//key pad animation
- (void)subscribeToKeyboard {
  [self an_subscribeKeyboardWithAnimations:^(CGRect keyboardRect, NSTimeInterval duration, BOOL isShowing) {
    if (isShowing) {
    CGRectGetHeight(keyboardRect);
    heightOfKeyPad = keyboardRect.size.height;
    }
    [self.view layoutIfNeeded];
  } completion:nil];
}




#pragma mark - <UITextViewDelegate>

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  
  if (range.location > 0 || (text.length > 0 && ![text isEqualToString:@"\n" ])) {
    if (textView.tag == 0) {
      labelEmail.hidden = YES;
    }else if (textView.tag == 1){
      labelPhoneNo.hidden = YES;
    }else{
      labelMessage.hidden = YES;
    }
    
  }else if ([textViewMessage.text length] == 1) {
    if (textView.tag == 2) {
      labelMessage.hidden = NO;
    }
  
  }else{
    if ([textViewMessage.text length] == 0) {
      if (textView.tag == 0) {
        labelEmail.hidden = NO;
      }else if (textView.tag == 1){
        labelPhoneNo.hidden = NO;
      }else{
        labelMessage.hidden = NO;
      }
      
    }
  }

 
  if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
    
    return YES;
  }
  [textView resignFirstResponder];
  return NO;
  
  return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
  
  [self subscribeToKeyboard];

  if (kScreenHeight == 480){
   
    textViewMessage.frame = CGRectMake(10 + i, 100 + 2 * buttonHeight, kScreenWidth - 2*(10 + i) , buttonHeight );
  }else{
    textViewMessage.frame = CGRectMake(10 + i, 100 + 2 * buttonHeight, kScreenWidth - 2*(10 + i) ,  ( kScreenHeight  - buttonHeight - 50 - i/6  - (100 + 2 * buttonHeight)) - heightOfKeyPad);
  }
  
  buttonSubmit.frame = CGRectMake(25 + i, textViewMessage.frame.origin.y + textViewMessage.frame.size.height + 30, kScreenWidth - 50 - 2 *i, buttonHeight);
 }

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}



- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
  
  if (textView.tag == 2) {
  if ([[textView text]length] > 0 ) {
    if (textView.tag == 0) {
      labelEmail.hidden = YES;
    }else if (textView.tag == 1){
      labelPhoneNo.hidden = YES;
    }else{
      labelMessage.hidden = YES;
    }
  }else{
    if ([textViewMessage.text length] == 1) {
      if (textView.tag == 0) {
        labelEmail.hidden = NO;
      }else if (textView.tag == 1){
        labelPhoneNo.hidden = NO;
      }else{
        labelMessage.hidden = NO;
      }
    }
    
  }
 
   textViewMessage.frame = CGRectMake(10 + i, 100 + 2 * buttonHeight, kScreenWidth - 2*(10 + i), kScreenHeight - buttonHeight - 60 - i/6 - (100 + 2 * buttonHeight) );
  buttonSubmit.frame = CGRectMake(25 + i, textViewMessage.frame.origin.y + textViewMessage.frame.size.height + 20, kScreenWidth - 50 - 2*i, buttonHeight);
  }
  return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
  if (textView.tag == 0) {
    [textPhoneNumber becomeFirstResponder];
    
  }else  if (textView.tag == 1) {
    
    [textViewMessage becomeFirstResponder];
    
  }

}


#pragma mark - Accessing Data From Server

-(void)sendDataToServerWithTag:(int)tag
{
  requestManager.delegate=self;
  switch (tag) {
    case TASK_FEEDBACK:
    {
      NSString *deviceTocken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DevToken"];
      NSString* countryId = [[NSUserDefaults standardUserDefaults]objectForKey:kCountrySelectionId];
       [self remove_Loader];
      [self show_Loader];
      NSMutableDictionary *dictRoot=[[NSMutableDictionary alloc] init];
      [dictRoot setObject:deviceTocken forKey:@"deviceId"];
      [dictRoot setObject:@"" forKey:@"gcmId"];
      [dictRoot setObject:textEmail.text forKey:@"email"];
      [dictRoot setObject:countryId forKey:@"countryId"];
      [dictRoot setObject:textPhoneNumber.text forKey:@"phone"];
      [dictRoot setObject:textViewMessage.text forKey:@"message"];
     
      [requestManager sendRequestToserver:dictRoot withCurrentTask:TASK_FEEDBACK withURL:kUrlForFeedback with:@"POST"];
      break;
    }
      
    default:
      break;
  }
}

#pragma mark - connection manager delegates

-(void)requestFinished:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  NSString *response = [request responseString];
  NSDictionary* recievedDictionary = [NSJSONSerialization objectWithString:response];
  if (recievedDictionary)
  {
    switch (request.tag) {
      case TASK_FEEDBACK:
      {
        
        if ([[recievedDictionary objectForKey:kResponseCode]integerValue] == 200) {
          ALERT(@"", [recievedDictionary objectForKey:kResponseMessage]);
          textEmail.text = @"";
          textPhoneNumber.text = @"";
          textViewMessage.text = @"";
          
          labelEmail.hidden = NO;
          labelPhoneNo.hidden = NO;
          labelMessage.hidden = NO;
          [textEmail becomeFirstResponder];

        }
        

        
                break;
      }
        
      default:
        break;
    }
  }
  else
  {
    ALERT(nil, @"");
  }
}

-(void) requestFailed:(ASIHTTPRequest *)request
{
  [self remove_Loader];
  
}


@end
