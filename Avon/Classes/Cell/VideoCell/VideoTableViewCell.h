//
//  VideoTableViewCell.h
//  Avon
//
//  Created by Shipra Singh on 24/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *viewForMainBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgvVideoThumb;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoName;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoDuration;
@property (weak, nonatomic) IBOutlet UIButton *btnPlay;
@property (weak, nonatomic) IBOutlet UIView *viewBG;
@end
