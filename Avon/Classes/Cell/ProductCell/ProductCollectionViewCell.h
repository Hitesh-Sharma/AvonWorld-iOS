//
//  ProductCollectionViewCell.h
//  Avon
//
//  Created by Shipra Singh on 31/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *viewBackGround;
//@property (strong, nonatomic) IBOutlet UIImageView *imageProduct;
@property (strong, nonatomic) IBOutlet UILabel *labelProductName;
@property (strong, nonatomic) IBOutlet UIImageView *ImageArrow;
@property (strong, nonatomic) IBOutlet UILabel *labelSelectedProductCount;
@property (strong, nonatomic) IBOutlet UIView *labelLineForCount;
@property (strong, nonatomic) IBOutlet UIView *labelLineForImage;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuantityClick;
@property (strong, nonatomic) IBOutlet UIView *viewForImage;

@end
