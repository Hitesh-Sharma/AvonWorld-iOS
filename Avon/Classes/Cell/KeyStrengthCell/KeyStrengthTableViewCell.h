//
//  KeyStrengthTableViewCell.h
//  Avon
//
//  Created by Shipra Singh on 31/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyStrengthTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelNo;
@property (strong, nonatomic) IBOutlet UILabel *labelKey;
@property (strong, nonatomic) IBOutlet UIImageView *imageArrow;
@property (strong, nonatomic) IBOutlet UILabel *lableLine;
@end
