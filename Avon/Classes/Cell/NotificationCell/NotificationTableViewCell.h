//
//  NotificationTableViewCell.h
//  Avon
//
//  Created by Shipra Singh on 24/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *clockImage;
@property (strong, nonatomic) IBOutlet UIView *baseViewOfScroll;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelNotification;
@property (strong, nonatomic) IBOutlet UILabel *labelTime;
@property (strong, nonatomic) IBOutlet UILabel *lableLine;
@property (strong, nonatomic) IBOutlet UILabel *labelHeding;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
