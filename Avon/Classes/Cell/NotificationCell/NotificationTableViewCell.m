//
//  NotificationTableViewCell.m
//  Avon
//
//  Created by Shipra Singh on 24/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NotificationTableViewCell.h"

@interface NotificationTableViewCell () <UIScrollViewDelegate>
{
   BOOL scrollOpen;
  CGFloat lastContentOffset;
  int i;
 }
@property (nonatomic) CGFloat offsetRadio;

@end


@implementation NotificationTableViewCell

- (void)awakeFromNib {
  [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  
  return self;
}

//-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
//   [super setEditing:editing animated:animated];
//  self.scrollView.scrollEnabled = !self.editing;
//  // Corrects effect of showing the button labels while selected on editing mode (comment line, build, run, add new items to table, enter edit mode and select an entry)
//  self.scrollView.hidden = editing;
//  
//}
//
//
//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
//  
//  self.offsetRadio = scrollView.contentOffset.x/CGRectGetWidth(scrollView.frame) - 1;
//}
//
//- (void)setOffsetRadio:(CGFloat)offsetRadio
//{
//  if (_offsetRadio != offsetRadio)
//  {
//    _offsetRadio = offsetRadio;
//    
//    [self.scrollView setContentOffset:CGPointMake(CGRectGetWidth(self.bounds) + CGRectGetWidth(self.bounds) * offsetRadio, 0)];
//  }
//}


@end
