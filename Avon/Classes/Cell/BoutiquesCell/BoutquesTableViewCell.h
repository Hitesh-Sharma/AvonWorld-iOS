//
//  BoutquesTableViewCell.h
//  Avon
//
//  Created by Shipra Singh on 20/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoutquesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imageCall;
@property (strong, nonatomic) IBOutlet UIImageView *imageLocationLogo;
@property (strong, nonatomic) IBOutlet UILabel *labelStoreName;
@property (strong, nonatomic) IBOutlet UILabel *labelLocationOfStore;
@property (strong, nonatomic) IBOutlet UILabel *labelSepraterLine;
@property (strong, nonatomic) IBOutlet UILabel *labelDistance;

@end
