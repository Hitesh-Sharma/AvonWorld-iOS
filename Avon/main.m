//
//  main.m
//  Avon
//
//  Created by Shipra Singh on 27/07/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
