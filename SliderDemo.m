//
//  SliderDemo.m
//  DemoSlider
//
//  Created by http://Sugartin.info on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SliderDemo.h"
#import "HomeViewController.h"
#import "AppDelegate.h"

@interface SliderDemo (PrivateMethods)

-(float)xForValue:(float)value;
-(float)valueForX:(float)x;
-(void)updateTrackHighlight;

@end

@implementation SliderDemo
@synthesize minimumValue, maximumValue, minimumRange, selectedMinimumValue, selectedMaximumValue;

- (id)initWithFrame:(CGRect)frame
{
 app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    self = [super initWithFrame:frame];
    if (self) {
        _minThumbOn = false;
        _maxThumbOn = false;
      if (IS_IPAD) {
        _padding = 40 ;
      }else{
        _padding = 30 ;
      }
      
      for (UIView *v  in self.subviews) {
        if ([v isKindOfClass:[UIImage class]]) {
          [v removeFromSuperview];
        }
                }

        
        _trackBackground = [[UIImageView alloc] init] ;
      _trackBackground.frame=CGRectMake(self.frame.origin.x,self.frame.origin.y , self.frame.size.width   ,self.frame.size.height - 50 );
      _trackBackground.clipsToBounds = YES;
      _trackBackground.layer.cornerRadius = (self.frame.size.height - 50)/2;
      _trackBackground.layer.borderWidth = 2.0;
      _trackBackground.layer.borderColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f].CGColor;
      _trackBackground.backgroundColor = [UIColor whiteColor];
        _trackBackground.center = self.center;
        [self addSubview:_trackBackground];
        
       // _track = [[UIImageView alloc] init] ;
//      _track.layer.masksToBounds = YES;
//      _track.layer.cornerRadius = 4.0;
//      _track.layer.borderWidth = 1.0;
//      _track.layer.borderColor = [UIColor blackColor].CGColor;
  //    _track.backgroundColor = [UIColor yellowColor];
   // _track.frame=CGRectMake(self.frame.origin.x,self.frame.origin.y, self.frame.size.height ,self.frame.size.height + 100);
        //       _track.frame=CGRectMake(self.frame.origin.x-40,20, self.frame.size.width,11);
     //   _track.center = self.center;
     //   [self addSubview:_track];
      
      _minThumb = [[UIImageView alloc] init];
      UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
      [_minThumb addGestureRecognizer:gestureRecognizer];
//        _minThumb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"handle.png"] highlightedImage:[UIImage imageNamed:@"handle-hover.png"]] ;
             _minThumb.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y, self.frame.size.height + 20 ,self.frame.size.height + 20 );
     // _minThumb.layer.cornerRadius = _minThumb.frame.size.width / 2;
        _minThumb.contentMode = UIViewContentModeCenter;
      //_minThumb.backgroundColor = [UIColor grayColor];
      
      _maxThumb = [[UIImageView alloc] init];
      
//        _maxThumb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"handle.png"] highlightedImage:[UIImage imageNamed:@"handle-hover.png"]] ;
               _maxThumb.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y, self.frame.size.height  ,self.frame.size.height  );
       // _maxThumb.layer.cornerRadius = _maxThumb.frame.size.width / 2;
        _maxThumb.contentMode = UIViewContentModeCenter;
      //_maxThumb.backgroundColor = [UIColor grayColor];
      if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsEastMalaysia] == YES) {
        [self addSubview:_minThumb];
        [self addSubview:_maxThumb];
//        _minThumb.userInteractionEnabled = YES;
//        _maxThumb.userInteractionEnabled = NO;
        if (IS_IPAD) {
         
          _minThumb.image = [UIImage imageNamed:@"choose_icon@3x.png"] ;
          _maxThumb.image = [UIImage imageNamed:@"choose_icon_click@3x.png"];
        }else{
         _minThumb.image = [UIImage imageNamed:@"choose_icon.png"] ;
        _maxThumb.image = [UIImage imageNamed:@"choose_icon_click.png"] ;
        }
//        _minThumb.backgroundColor = [UIColor grayColor];
//        _maxThumb.backgroundColor = [UIColor purpleColor];

      }else{
        [self addSubview:_maxThumb];
        [self addSubview:_minThumb];
//        _minThumb.userInteractionEnabled = NO;
//        _maxThumb.userInteractionEnabled = YES;
        
        if (IS_IPAD) {
          _minThumb.image =[UIImage imageNamed:@"choose_icon_click@3x.png"]  ;
          _maxThumb.image = [UIImage imageNamed:@"choose_icon@3x.png"] ;
        }else{
         _minThumb.image =[UIImage imageNamed:@"choose_icon_click.png"]  ;
        _maxThumb.image = [UIImage imageNamed:@"choose_icon.png"] ;
        }

//        _minThumb.backgroundColor = [UIColor purpleColor];
//        _maxThumb.backgroundColor = [UIColor grayColor];

      }

    }
    
    return self;
}

-(void)hideKeyboard{
  
}
-(void)layoutSubviews
{
    // Set the initial state
    _minThumb.center = CGPointMake([self xForValue:selectedMinimumValue], self.center.y);
    
    _maxThumb.center = CGPointMake([self xForValue:selectedMaximumValue], self.center.y);
    
    
    NSLog(@"Tapable size %f", _minThumb.bounds.size.width); 
    [self updateTrackHighlight];
    
    
}




-(float)xForValue:(float)value{
    return (self.frame.size.width-(_padding*2))*((value - minimumValue) / (maximumValue - minimumValue))+_padding;
}

-(float) valueForX:(float)x{
    return minimumValue + (x-_padding) / (self.frame.size.width-(_padding*2)) * (maximumValue - minimumValue);
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    if(!_minThumbOn && !_maxThumbOn){
        return YES;
    }
    
    CGPoint touchPoint = [touch locationInView:self];
    if(_minThumbOn){
        _minThumb.center = CGPointMake(MAX([self xForValue:minimumValue],MIN(touchPoint.x - distanceFromCenter, [self xForValue:selectedMaximumValue - minimumRange])), _minThumb.center.y);
        selectedMinimumValue = [self valueForX:_minThumb.center.x];
        
    }
    if(_maxThumbOn){
        _maxThumb.center = CGPointMake(MIN([self xForValue:maximumValue], MAX(touchPoint.x - distanceFromCenter, [self xForValue:selectedMinimumValue + minimumRange])), _maxThumb.center.y);
        selectedMaximumValue = [self valueForX:_maxThumb.center.x];
    }
    [self updateTrackHighlight];
    [self setNeedsLayout];
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    return YES;
}

-(BOOL) beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
  
  CGPoint touchPoint = [touch locationInView:self];
  if (CGRectContainsPoint(_minThumb.frame, touchPoint) && CGRectContainsPoint(_maxThumb.frame, touchPoint) && touchPoint.x < kScreenWidth/2)  {
    _maxThumbOn = true;
    distanceFromCenter = touchPoint.x - _maxThumb.center.x;
  }else if(CGRectContainsPoint(_minThumb.frame, touchPoint) && CGRectContainsPoint(_maxThumb.frame, touchPoint) && touchPoint.x > kScreenWidth/2){
    _minThumbOn = true;
    distanceFromCenter = touchPoint.x - _minThumb.center.x;

    
  }
   else if(CGRectContainsPoint(_minThumb.frame, touchPoint)){
        _minThumbOn = true;
        distanceFromCenter = touchPoint.x - _minThumb.center.x;
//      NSLog(@"%f", _minThumb.center.x);
//       NSLog(@"%f", touchPoint.x);
    }
    else if(CGRectContainsPoint(_maxThumb.frame, touchPoint)){
        _maxThumbOn = true;
        distanceFromCenter = touchPoint.x - _maxThumb.center.x;
        
    }
    return YES;
}

-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
  
    _minThumbOn = false;
    _maxThumbOn = false;
  CGPoint touchPoint = [touch locationInView:self];
  
  if ((CGRectContainsRect(_minThumb.frame, _maxThumb.frame) && touchPoint.x < _trackBackground.frame.size.width/2 ) || ([[NSUserDefaults standardUserDefaults]boolForKey:kIsEastMalaysia] == YES && touchPoint.x < _trackBackground.frame.size.width/2) || ([[NSUserDefaults standardUserDefaults]boolForKey:kIsEastMalaysia] == NO && touchPoint.x < _trackBackground.frame.size.width/2)){
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ChangeColorWest"
     object:@"West Malaysia"];
   
      }else if ((CGRectContainsRect(_minThumb.frame, _maxThumb.frame) && touchPoint.x > _trackBackground.frame.size.width/2) || ([[NSUserDefaults standardUserDefaults]boolForKey:kIsEastMalaysia] == NO && touchPoint.x > _trackBackground.frame.size.width/2) || ([[NSUserDefaults standardUserDefaults]boolForKey:kIsEastMalaysia] == YES && touchPoint.x > _trackBackground.frame.size.width/2)){
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ChangeColorEast"
     object:@"East Malaysia"];
   

    
  }
  
  
  
}

-(void)updateTrackHighlight{
    
	_track.frame = CGRectMake(
                              _minThumb.center.x,
                              _track.center.y - (_track.frame.size.height/2),
                              _maxThumb.center.x - _minThumb.center.x,
                              _track.frame.size.height
                              );
    
}


@end
