//
//  NotificationBO.m
//  Avon
//
//  Created by Shipra Singh on 25/08/15.
//  Copyright (c) 2015 Shipra Singh. All rights reserved.
//

#import "NotificationBO.h"

@implementation NotificationBO

-(NotificationBO *)init{
  
  self = [super init];
  if (self) {
    self.notificationId = @"";
    self.notificationTime = @"";
    self.notificationTittle = @"";
    self.notificationMessage = @"";
    }
  return self; 
}

-(NotificationBO *)initWithResponse:(id)response{
  
  self = [super init];
  if (self && [response isKindOfClass:[NSMutableDictionary class]]) {
    self.notificationId = [self checkNil:[NSString stringWithFormat:@"%@",[response objectForKey:kID]]];
    self.notificationTime = [self checkNil:[response objectForKey:kNotificationTime]];
    self.notificationTittle = [self checkNil:[response objectForKey:kTittle]];
    self.notificationMessage = [self checkNil:[response objectForKey:kNotificationMessage]];
    }
    return self;
}

@end
